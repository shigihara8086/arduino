/*
 ADT7410
*/

#if defined(ENABLE_ADT7410)

#include <Wire.h>

#define I2C_SDA 5                                     // SDA Pin No.5 yellow
#define I2C_SCL 4                                     // SCL Pin No.4 green

#define ADT7410_ADD 0x48

char *node_config_adt7410_temp =
  "graph_title 温度(%s)\n"
  "graph_args --base 1000 --alt-autoscale\n"
  "graph_vlabel ℃\n"
  "graph_scale no\n"
  "graph_order temp inter trend\n"
  "graph_category measure_temperature\n"
  "graph_info adt7410センサーで測定した温度\n"
  "temp.label 温度　　\n"
  "temp.info ADT7410センサーの温度\n"
  "inter.cdef temp,UN,PREV,temp,IF\n"
  "inter.update no\n"
  "inter.graph no\n"
  "trend.label 移動平均\n"
  "trend.line 25.0:880000\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info 温度の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

float meas_temp = 0.0;

int adt7410I2CAddress = 0x48;

int adt7410_init() {
  Wire.begin(I2C_SDA, I2C_SCL);
  return 0;
}

unsigned int adt7410_readRegister(void) {
  unsigned int val;
  Wire.requestFrom(ADT7410_ADD, 2);
  val  = Wire.read() << 8;
  val |= Wire.read();
  return val;
}

float adt7410_getvalues() {
uint16_t uiVal;                         //2バイト(16ビット)の領域
int iVal;
  uiVal = adt7410_readRegister();
  uiVal >>= 3;                          // シフトで13bit化
  if (uiVal & 0x1000) {                 // 13ビットで符号判定
    iVal = uiVal - 0x2000;              // マイナスの時 (10進数で8192)
  } else {
    iVal = uiVal;                       //プラスの時
  }
  return ((float)iVal / 16.0);          // 温度換算(摂氏)
}

//
// config
//

void do_config_adt7410_temp(WiFiClient &client) {
  while(true) {
    meas_temp = adt7410_getvalues();
    if(meas_temp > -50 && meas_temp < 100) break;
    delay(100);
  }
  client.printf(node_config_adt7410_temp,node_name);
  Serial.printf(node_config_adt7410_temp,node_name);
}

//
// fetch
//

void do_fetch_adt7410_temp(WiFiClient &client) {
  client.printf("temp.value %f\n",meas_temp);
  Serial.printf("meas_temp: %5.1f\n", meas_temp);
}

//
// loop
//

void adt7410_loop() {
}

#endif
