/*
  timer wakeup measerement base
*/

// #define NODE_BLANK                                 // 室外設置(Wi-Fi) OTA済み     // COM??
// #define NODE_LTE                                   // 室内設置(LTE)
// #define NODE_OUTER3                                // 室外設置(Wi-Fi) OTA済み     // COM??
// #define NODE_OUTER4                                // 室外設置(Wi-Fi) OTA済み     // COM??
#define NODE_DHT22                                 // 室外設置(Wi-Fi) OTA済み     // COM??
// #define NODE_STORE                                 // 室外設置(Wi-Fi) OTA済み     // COM??
// #define NODE_SHT3X                                 // 室外設置(Wi-Fi) OTA済み     // COM??

#include "TimerSampler.h"

#include <Wire.h>
#include <esp_deep_sleep.h>
#include <WiFi.h>
#include <WiFiClientSecure.h>
#include <WiFiMulti.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <HTTPClient.h>

//
// define macros
//

#define uS_TO_S_FACTOR      1000000UL              // Conversion factor for micro seconds to seconds
#define HTTP_TIMEOUT        (60)                   // HTTP timeout
#define HTTP_RETRY          (5)                    // HTTP retry count
#define HTTP_RETRY_DELAY    (10)                   // HTTP retry delay

//
// global objects
//

const char *http_server     = "https://sgmail.jp/cgi/esp32-node.cgi";       // data logging server name
const char *node_name       = NODE_NAME;

esp_sleep_wakeup_cause_t wakeup_reason;            // Wakeup reason

RTC_DATA_ATTR int boot_count = 0;                  // sleep times counter
RTC_DATA_ATTR bool ota_mode = false;               // ota mode false:normal mode,true:OTA hunt mode

struct tm timeInfo, timeMeas, timeFine;            // times
int epoch_left;                                    // epoch seconds

//
// measere data
//

double meas_temp  = 0.0;
double meas_humi  = 0.0;
double meas_dewp  = 0.0;
double meas_press = 0.0;
double core_temp  = 0.0;

//
// show wakeup reason
//

void get_wakeup_reason() {
  wakeup_reason = esp_sleep_get_wakeup_cause();
  switch (wakeup_reason) {
    case ESP_DEEP_SLEEP_WAKEUP_EXT0     : Serial.print("Wakeup caused by external signal using RTC_IO\n"); break;
    case ESP_DEEP_SLEEP_WAKEUP_EXT1     : Serial.print("Wakeup caused by external signal using RTC_CNTL\n"); break;
    case ESP_DEEP_SLEEP_WAKEUP_TIMER    : Serial.print("Wakeup caused by timer\n"); break;
    case ESP_DEEP_SLEEP_WAKEUP_TOUCHPAD : Serial.print("Wakeup caused by touchpad\n"); break;
    case ESP_DEEP_SLEEP_WAKEUP_ULP      : Serial.print("Wakeup caused by ULP program\n"); break;
    default : {
        Serial.print("Wakeup was not caused by deep sleep\n");
        break;
      }
  }
}

//
// setup hardware
//

void hw_init() {
  // wdt_init();
  Serial.begin(115200);
  get_wakeup_reason();
  if(LTE_POWERLED != -1) pinMode(LTE_POWERLED, OUTPUT);
  if(LTE_POWER != -1) pinMode(LTE_POWER, OUTPUT);
  delay(1 * 1000);
  if(LTE_POWERLED != -1) digitalWrite(LTE_POWERLED, HIGH);
  if(LTE_POWER != -1) digitalWrite(LTE_POWER, HIGH);
#if defined(ENABLE_BME280)
  bme280_init();
  bme280_getValues();
#endif
#if defined(ENABLE_SHT3X)
  sht3x_init();
  sht3x_getValues();
#endif
#if defined(ENABLE_DHT22)
  dht22_init();
  dht22_getValues();
#endif
}

//
// wait LTE dongle is up
//

void wait_lte_up() {
  Serial.printf("wait for Modem up %d sec.\n", TIME_TO_ON);
  delay((TIME_TO_ON - 1) * 1000);
}

//
// idle for measure epoch time
//

int wait_for_measure() {
  time_t now, left;
  now = time(NULL);
  left = TIME_TO_SLEEP - (now % TIME_TO_SLEEP);
  Serial.printf("Wait for epoch : %d - (%ld %% %d) = %d sec.\n", TIME_TO_SLEEP, now, TIME_TO_SLEEP, left);
  if(left > ((TIME_TO_ON + WAKE_OFFSET) * 2)) {
    left -= TIME_TO_ON + WAKE_OFFSET;
    Serial.printf("Deep sleep for %ld sec.\n", left);
    esp_sleep_enable_timer_wakeup(left * uS_TO_S_FACTOR);
    esp_deep_sleep_start();
    Serial.print("This will never be printed\n");
  } else {
    Serial.printf("Wait by delay : %d sec.\n", left);
    epoch_left = left;
    delay(left * 1000);
    for (;;) {
      getLocalTime(&timeInfo);
      if(timeInfo.tm_sec == 0) break;
      delay(10);
    }
  }
  getLocalTime(&timeInfo);
  Serial.printf("Epoch time : %04d-%02d-%02d %02d:%02d:%02d\n",
                timeInfo.tm_year + 1900, timeInfo.tm_mon + 1, timeInfo.tm_mday,
                timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec);
}

//
// measure data
//

void measure(void) {
  getLocalTime(&timeMeas);
  Serial.printf("Measure start : %04d-%02d-%02d %02d:%02d:%02d\n",
                timeMeas.tm_year + 1900, timeMeas.tm_mon + 1, timeMeas.tm_mday,
                timeMeas.tm_hour, timeMeas.tm_min, timeMeas.tm_sec);
  //
  core_temp = temperatureRead();
  Serial.printf("コア = % 5.1f ℃\n", core_temp);

#if defined(ENABLE_BME280)
  bme280_getValues();
#endif
#if defined(ENABLE_SHT3X)
  sht3x_getValues();
#endif
#if defined(ENABLE_DHT22)
  dht22_getValues();
#endif
}

//
// send measure data via http
//

void http_send() {
  HTTPClient http;
  int httpCode;
  char buffer[512];
  Serial.print("http send start\n");
  sprintf(
    buffer,
    "%s?"
    "node=%s&"
    "date=%04d-%02d-%02dT%02d:%02d:%02d&"
    "wifi-ssid=%s&"
    "wifi-rssi=%d&"
    "boot=%d&"
    "latency=%d&"
    "core_temp=%f&"
    "temp=%f&"
    "pressure=%f&"
    "humidity=%f&"
    "dewpoint=%f",
    http_server,
    node_name,
    timeMeas.tm_year + 1900, timeMeas.tm_mon + 1, timeMeas.tm_mday, timeMeas.tm_hour, timeMeas.tm_min, timeMeas.tm_sec,
    WiFi.SSID().c_str(),
    WiFi.RSSI(),
    boot_count,
    epoch_left,
    core_temp,
    meas_temp,
    meas_press,
    meas_humi,
    meas_dewp
  );
  Serial.printf("%s\n", buffer);
  for (int i = 0; i < HTTP_RETRY; i ++) {
    http.begin(buffer);
    http.setTimeout(HTTP_TIMEOUT * 1000);
    httpCode = http.GET();
    if(httpCode > 0) {
      Serial.printf("[HTTP] GET... code: %d\n", httpCode);
      if(httpCode == HTTP_CODE_OK) {
        String payload = http.getString();
        Serial.println("Payload : " + payload);
        http.end();
        if(payload.equals(String("OTA\r\n"))) {
          Serial.print("Next is OTA mode\n");
          ota_mode = true;
          esp_sleep_enable_timer_wakeup(1 * uS_TO_S_FACTOR);
          esp_deep_sleep_start();
          Serial.print("start deep sleep\n");
        } else {
          Serial.print("Next is Normal mode\n");
          ota_mode = false;
        }
        return;
      }
    } else {
      Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
      if(httpCode == HTTPC_ERROR_READ_TIMEOUT) {
        http.end();
        return;
      }
    }
    http.end();
    Serial.printf("http send retry after %d sec.\n",HTTP_RETRY_DELAY);
    delay(HTTP_RETRY_DELAY * 1000);
  }
}

//
// deep sleep for next time
//

void deep_sleep_next() {
  time_t now, left;
  now = time(NULL);
  left = (TIME_TO_SLEEP - (now % TIME_TO_SLEEP)) - (TIME_TO_ON + WAKE_OFFSET);
  if(left < 0) {
    delay(-left * 1000);
    now = time(NULL);
    left = (TIME_TO_SLEEP - (now % TIME_TO_SLEEP)) - (TIME_TO_ON + WAKE_OFFSET);
  }
  boot_count ++;
  Serial.printf("Boot count : %d\n", boot_count);
  if(boot_count > (86400 / TIME_TO_SLEEP) || boot_count > 100) {
    Serial.printf("hungup guard reboot\n");
    boot_count = 0;
    // ESP.restart();
    esp_sleep_enable_timer_wakeup((unsigned long)left * uS_TO_S_FACTOR);
    esp_deep_sleep_start();
    Serial.print("This will never be printed\n");
  } else {
    Serial.printf("Deep sleep for %ld sec.\n", left);
    Serial.print("Going to sleep now\n");
    esp_sleep_enable_timer_wakeup((unsigned long)left * uS_TO_S_FACTOR);
    esp_deep_sleep_start();
    Serial.print("This will never be printed\n");
  }
}

//
// setup main
//

void setup() {
  hw_init();
  wifi_init();
  if(ota_mode) {
    Serial.print("start OTA mode\n");
    ota_mode = false;
  } else {
    Serial.print("start Normal mode\n");
    ntp_init();
    wait_for_measure();
    measure();
    http_send();
    wifi_term();
    deep_sleep_next();
  }
}

//
// main loop, send data via http
//

void loop() {
  ArduinoOTA.handle();
}
