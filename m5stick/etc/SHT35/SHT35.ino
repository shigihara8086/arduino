/*
*/

#include "Seeed_SHT35.h"

/*
#define SDAPIN  32
#define SCLPIN  33
*/
#define SDAPIN  22
#define SCLPIN  21
#define I2CADR  0x44

SHT35 sensor(SCLPIN);

void setup() {
  /*
  M5.begin();
  M5.Axp.ScreenBreath(8);
  M5.Lcd.begin();
  M5.Lcd.setRotation(1);
  M5.Lcd.fillScreen(BLACK);
  M5.Lcd.setTextColor(WHITE, BLACK);
  M5.Lcd.setTextSize(2);
  */
  //
  Serial.begin(115200);
  Serial.println("Serial start!!");
  if (sensor.init()) {
    Serial.println("sensor init failed!!!");
  }
}


void loop() {
  float tmp, hum;
  if (NO_ERROR != sensor.read_meas_data_single_shot(HIGH_REP_WITH_STRCH, &tmp, &hum)) {
    Serial.println("read temp failed!!");
  } else {
    /*
    M5.Lcd.setCursor(16, 10);
    M5.Lcd.printf("TEMP %5.1fC", tmp);
    M5.Lcd.setCursor(16, 10 + 18);
    M5.Lcd.printf("HUMI %5.1f%%", hum);
    M5.Lcd.setCursor(16, 10 + 18 * 2);
    M5.Lcd.printf("VBAT %5.1fV", bat);
    */
#if 1
    Serial.print("温度 = ");
    Serial.print(tmp);
    Serial.print("℃ ");
    Serial.print("湿度 = ");
    Serial.print(hum);
    Serial.print("％\n");
#endif
  }
  delay(3000);
}
