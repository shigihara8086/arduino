/*
 Munin-node
*/

#if defined(NODE_STICKC)
#define ENABLE_BLANK
// #define ENABLE_PIR
#define ENABLE_SHT35
#define LED_BUILTIN (10)
char *node_name = "stickc";
#endif

#if defined(NODE_ATOM)
#define ENABLE_BLANK
// #define ENABLE_PIR
// #define ENABLE_SHT35
#define LED_BUILTIN (12)
char *node_name = "atom";
#endif

#if defined(NODE_ATOM_LITE)
#define ENABLE_BLANK
// #define ENABLE_PIR
// #define ENABLE_SHT35
#define LED_BUILTIN (12)
char *node_name = "atom-lite";
#endif
