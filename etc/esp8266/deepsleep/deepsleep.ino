#include <TimeLib.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <ESP8266HTTPClient.h>
#include <Ambient.h>

// serial port baud rate
#define SER_BAUD 115200

// LED port
#define LED_PIN D8
#define RST_PIN D0

// pre-wakeup time
#define LTE_WAKEUP 0
#define PRE_WAKEUP 30

// HTTP
const char* httpServerIP = "192.168.254.7";
// Wi-Fi
const char* wifi_ssid   = "106F3FDBCFA9";
const char* wifi_passwd = "amhh8khbexssm";
// NTP
unsigned int localPort = 2390;       // local port to listen for UDP packets
const char* ntpServerName = "ntp.nict.jp";
IPAddress timeServerIP;            // time.nist.gov NTP server address
const int NTP_PACKET_SIZE = 48;      // NTP time stamp is in the first 48 bytes of the message
byte packetBuffer[ NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets
WiFiUDP udp;                         // A UDP instance to let us send and receive packets over UDP

void WiFiconnect(void) {
int count = 0;
  Serial.print("Connecting Wi-Fi ");
  WiFi.mode(WIFI_STA);
  WiFi.begin(wifi_ssid,wifi_passwd);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    count ++;
    if(count > 20) {
      Serial.println("Force reset ...");
      pinMode(RST_PIN,OUTPUT);
      digitalWrite(RST_PIN,LOW);
    }
  }
  Serial.println();
  Serial.print("Connected to ");
  Serial.println(wifi_ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

unsigned long sendNTPpacket(IPAddress& address) {
  Serial.println("Sending NTP packet ...");
  memset(packetBuffer,0,NTP_PACKET_SIZE); // set all bytes in the buffer to 0
  // Initialize values needed to form NTP request
  packetBuffer[ 0] = 0b11100011;           // LI, Version, Mode
  packetBuffer[ 1] = 0;                    // Stratum, or type of clock
  packetBuffer[ 2] = 6;                    // Polling Interval
  packetBuffer[ 3] = 0xEC;                 // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12] = 49;
  packetBuffer[13] = 0x4E;
  packetBuffer[14] = 49;
  packetBuffer[15] = 52;
  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  udp.beginPacket(address,123);           // NTP requests are to port 123
  udp.write(packetBuffer,NTP_PACKET_SIZE);
  udp.endPacket();
}

time_t getNTPtime() {
  udp.begin(localPort);
//  Serial.print("UDP local port: ");
//  Serial.println(udp.localPort());
  WiFi.hostByName(ntpServerName,timeServerIP); 
  sendNTPpacket(timeServerIP);   // send an NTP packet to a time server
  // wait to see if a reply is available
  delay(1000);
  int cb = udp.parsePacket();
  if (!cb) {
//    Serial.println("no packet yet");
    return(0);
  } else {
//    Serial.print("Packet received, length=");
//    Serial.println(cb);
    // We've received a packet, read the data from it
    udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer
    //the timestamp starts at byte 40 of the received packet and is four bytes,
    // or two words, long. First, esxtract the two words:
    unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
    unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
    // combine the four bytes (two words) into a long integer
    // this is NTP time (seconds since Jan 1 1900):
    unsigned long secsSince1900 = highWord << 16 | lowWord;
//    Serial.print("Seconds since Jan 1 1900 = " );
//    Serial.println(secsSince1900);
    // now convert NTP time into everyday time:
//    Serial.print("Unix time = ");
    // Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
    const unsigned long seventyYears = 2208988800UL;
    // subtract seventy years:
    unsigned long epoch = secsSince1900 - seventyYears;
    // print Unix time:
    Serial.println(epoch);
    epoch += 3600 * 9; // Add JST offset
    return(epoch);
  }
}

time_t getNTP() {
unsigned long epoch = 0;
int count = 0;
  while(epoch == 0) {
    epoch = getNTPtime();
    if(epoch != 0) break;
    count ++;
    Serial.print(".");
    delay(1000);
    if(count > 10) {
      Serial.println("Force reset ...");
      pinMode(RST_PIN,OUTPUT);
      digitalWrite(RST_PIN,LOW);
    }
  }
  return(epoch);
}

char* mysPrintf(char* buf,int len,char *fmt, ...) {
  va_list args;
  va_start(args,fmt);
  vsnprintf(buf,len,fmt,args);
  va_end(args);
  return(buf);
}

void myPrintf(char *fmt, ...) {
  char buf[128];
  va_list args;
  va_start(args,fmt);
  vsnprintf(buf,128,fmt,args);
  va_end(args);
  Serial.print(buf);
}

void measure(time_t left) {
HTTPClient http;
char buffer[256];
int httpCode;
  mysPrintf(buffer,256,"http://%s/cgi/esp8266.cgi?t=%04d/%02d/%02dT%02d:%02d:%02d&l=%d",httpServerIP,year(),month(),day(),hour(),minute(),second(),left);
  http.begin(buffer);
  Serial.printf("HTTP GET request: %s\n",buffer);
  httpCode = http.GET();
  if(httpCode > 0) {
    // HTTP header has been send and Server response header has been handled
    Serial.printf("HTTP GET response: %d\n",httpCode);
    if(httpCode == HTTP_CODE_OK) {
      String payload = http.getString();
      Serial.println(payload);
    }
  } else {
    Serial.printf("HTTP GET failed, error: %s\n",http.errorToString(httpCode).c_str());
  }
  http.end();
}

void setup() {
unsigned long start,wait,measure_time;
time_t left;
int count;
  // wakeup indicator
  pinMode(LED_PIN,OUTPUT);
  digitalWrite(LED_PIN,HIGH);
  //デバッグ用にシリアルを開く
  Serial.begin(SER_BAUD);
  Serial.println();
  Serial.print("Reboot reason: ");
  Serial.println(ESP.getResetReason());
  // Wait for LTE dongle starting
  Serial.println("Wait LTE dongle starting ...");
  delay(LTE_WAKEUP * 1000);
  // Start
  WiFiconnect();
  setSyncProvider(getNTP);
  setSyncInterval(3600);
  myPrintf("Wakeup: %04d/%02d/%02d %02d:%02d:%02d\n",year(),month(),day(),hour(),minute(),second());
  // xx:00:00まで待機
  left = (hour() + 1) * 60 * 60 - ((hour() * 60 * 60) + (minute() * 60) + second());
  if(left > (PRE_WAKEUP + LTE_WAKEUP) * 3) {
    wait = left - (PRE_WAKEUP + LTE_WAKEUP);
    myPrintf("Short sleep %02d:%02d (%d sec) ...\n",wait / 60,wait % 60,wait);
    ESP.deepSleep(wait * 1000 * 1000,WAKE_RF_DEFAULT);
    delay(1000); // deepsleepモード移行までのダミー命令
  }
  myPrintf("Wait %02d:%02d (%d) sec ...\n",left / 60,left % 60,left);
  count = 0;
  while(minute() != 0 || second() != 0) {
    delay(500);
    Serial.print(".");
    count ++;
    if(count > 120) {
      count = 0;
      Serial.println();
    }
  }
  Serial.println();
  start = millis();
  myPrintf("Measure start: %04d/%02d/%02d %02d:%02d:%02d\n",year(),month(),day(),hour(),minute(),second());
  measure(left - (PRE_WAKEUP + LTE_WAKEUP));
  measure_time = (millis() - start) / 1000;
  myPrintf("Measure time: %d sec ...\n",measure_time);
  digitalWrite(LED_PIN,LOW);
  myPrintf("Measure end: %04d/%02d/%02d %02d:%02d:%02d\n",year(),month(),day(),hour(),minute(),second());
  wait = 60 * 60 - (measure_time + PRE_WAKEUP + LTE_WAKEUP);
  myPrintf("Long sleep %02d:%02d (%d sec) ...\n",wait / 60,wait % 60,wait);
  ESP.deepSleep(wait * 1000 * 1000,WAKE_RF_DEFAULT);
  delay(1000); // deepsleepモード移行までのダミー命令
}

void loop() {}

