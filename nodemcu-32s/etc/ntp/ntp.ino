#include <WiFi.h>
#include <WiFiMulti.h>

//
// macros
//

#define JST                 (9 * 3600L)         // JST offset

//
// ssid and password list
//

struct WifiMulti_aps {                          // ssid,password pare
  char *ssid;
  char *passwd;
};

struct WifiMulti_aps wifi_aps[] = {             // ssid,password table
  { "aterm-20cd3f-g",     "65676ae9e9f9b" },
  { NULL, NULL }
};

//
// ntp server list
//

const char *ntp_hosts[] = {                     // NTP servers
  "192.168.254.7",
  "192.168.254.7",
  "192.168.254.7",
  NULL
};

WiFiMulti wifiMulti;                            // Multi AP
struct tm timeInfo, timeMeas, timeFine;         // times

//
// setup Wi-Fi
//

int wifimulti_init() {
int status;
int i;

  i = 0;
  while (wifi_aps[i].ssid) {
    wifiMulti.addAP(wifi_aps[i].ssid, wifi_aps[i].passwd);
    i ++;
  }
  Serial.print("WiFi connect start\n");
  // 以下4行の何処かでハングアップすることがある
  WiFi.mode(WIFI_STA);
  // WiFi.disconnect();
  i = 0;
  do {
    status = wifiMulti.run();
    Serial.printf("status : %d\n", status);
    if (i > 20) {
      Serial.print("Can not connect Wi-Fi. restart\n");
      ESP.restart();
    }
    i ++;
    delay(1000);
  } while(status != WL_CONNECTED);
  Serial.print("Connect Wi-Fi.\n");
  return(status);
}

//
// setup NTP
//

int ntp_init() {
int status;
int i;

  i = 0;
  do {
    configTime(JST, 0, ntp_hosts[0], ntp_hosts[1], ntp_hosts[2]);
    getLocalTime(&timeInfo);
    Serial.printf("NTP date : %04d-%02d-%02d %02d:%02d:%02d\n",
                  timeInfo.tm_year + 1900, timeInfo.tm_mon + 1, timeInfo.tm_mday,
                  timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec);
    status = (timeInfo.tm_year + 1900) < 1971;
    if(status) {
      Serial.print("Can not NTP sync\n");
      if(i > 20) {
        Serial.print("restart\n");
        ESP.restart();
      }
    }
    i ++;
    delay(1000);
  } while(status);
  return(status);
}

void setup() {
  Serial.begin(115200);
  wifimulti_init();
  ntp_init();
}

void loop() {
}
