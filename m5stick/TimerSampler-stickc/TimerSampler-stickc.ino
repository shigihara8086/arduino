/*
  timer wakeup measerement base
*/

#include <M5StickC.h>
#include <Wire.h>
#include <esp_deep_sleep.h>
#include <WiFiMulti.h>
#include <HTTPClient.h>

#define NODE_STICKC                             // トイレ設置 COM8

#include "TimerSampler.h"

//
// define macros
//

#define uS_TO_S_FACTOR      1000000UL           // Conversion factor for micro seconds to seconds
#define HTTP_TIMEOUT        (30)                // HTTP timeout
#define HTTP_RETRY          (5)                 // HTTP retry count
#define HTTP_RETRY_DELAY    (30)                // HTTP retry delay

//
// global objects
//

const char *http_server     = "https://sgmail.jp/cgi/esp32-node.cgi";       // data logging server name
const char *node_name       = NODE_NAME;

esp_sleep_wakeup_cause_t wakeup_reason;         // Wakeup reason
RTC_DATA_ATTR int bootCount = 0;                // sleep times counter

struct tm timeInfo, timeMeas, timeFine;         // times
int epoch_left;                                 // epoch seconds
int wifi_connect_count      = 0;                //

const int wdtTimeout = (5 * 60 * 1000);
hw_timer_t *timer = NULL;

//
// measurement data
//

float  core_temp  = 0.0;
float  meas_temp  = 0.0;
float  meas_humi  = 0.0;
double meas_batt  = 0.0;

//
// show wakeup reason
//

void get_wakeup_reason() {
  wakeup_reason = esp_sleep_get_wakeup_cause();
  switch (wakeup_reason) {
    case ESP_DEEP_SLEEP_WAKEUP_EXT0     : Serial.print("Wakeup caused by external signal using RTC_IO\n"); break;
    case ESP_DEEP_SLEEP_WAKEUP_EXT1     : Serial.print("Wakeup caused by external signal using RTC_CNTL\n"); break;
    case ESP_DEEP_SLEEP_WAKEUP_TIMER    : Serial.print("Wakeup caused by timer\n"); break;
    case ESP_DEEP_SLEEP_WAKEUP_TOUCHPAD : Serial.print("Wakeup caused by touchpad\n"); break;
    case ESP_DEEP_SLEEP_WAKEUP_ULP      : Serial.print("Wakeup caused by ULP program\n"); break;
    default : {
        Serial.print("Wakeup was not caused by deep sleep\n");
        break;
      }
  }
}

//
// watch dog timer
//

void IRAM_ATTR wdt_callback() {
  ets_printf("reboot by watchdog\n");
  esp_restart();
}

void wdt_init() {
  // watchdog
  timer = timerBegin(0, 80, true);                   // timer 0, div 80
  timerAttachInterrupt(timer, &wdt_callback, true);  // attach callback
  timerAlarmWrite(timer, wdtTimeout * 1000, false);  // set time in us
  timerAlarmEnable(timer);                           // enable interrupt
  timerWrite(timer, 0);                              // reset timer (feed watchdog)
}

//
// setup hardware
//

void m5_lcd_on() {
  Wire1.beginTransmission(0x34);
  Wire1.write(0x12);
  Wire1.write(0x4d); // Enable LDO2, aka OLED_VDD
  Wire1.endTransmission();
}

void m5_lcd_off() {
  Wire1.beginTransmission(0x34);
  Wire1.write(0x12);
  Wire1.write(0b01001011);  // LDO2, aka OLED_VDD, off
  Wire1.endTransmission();  
}

void m5_init() {
  M5.begin();
  m5_lcd_off();
/*
  m5_lcd_on();
  M5.Axp.ScreenBreath(8);
  M5.Lcd.begin();
  M5.Lcd.setRotation(1);
  M5.Lcd.fillScreen(BLACK);
  M5.Lcd.setTextColor(WHITE, BLACK);
  M5.Lcd.setTextSize(2);
*/
}

void serial_init() {
  Serial.begin(115200);
  Serial.println("Serial start!!");
}

void hw_init() {
  m5_init();
  // wdt_init();
  serial_init();
  get_wakeup_reason();
  delay(1 * 1000);
  pinMode(LTE_POWERLED, OUTPUT);
  digitalWrite(LTE_POWERLED, LOW);
  sht35_init();
}

//
// read battery volts and halt then low battery
//

void read_power_volts() {
  meas_batt = M5.Axp.GetVbatData() * 1.1 / 1000;
  Serial.printf("battery voltage : %5.2fV\n", meas_batt);
}

//
// wait LTE dongle is up
//

void wait_lte_up() {
  Serial.printf("wait for MT100 LTE up %d sec.\n", TIME_TO_ON);
  delay((TIME_TO_ON - 1) * 1000); //Take some time to open up the Serial Monitor
}

//
// idle for measure epoch time
//

int wait_for_measure() {
  time_t now, left;
  now = time(NULL);
  left = TIME_TO_SLEEP - (now % TIME_TO_SLEEP);
  Serial.printf("Wait for epoch : %d - (%ld %% %d) = %d sec.\n", TIME_TO_SLEEP, now, TIME_TO_SLEEP, left);
  if (left > ((TIME_TO_ON + WAKE_OFFSET) * 2)) {
    left -= TIME_TO_ON + WAKE_OFFSET;
    Serial.printf("Deep sleep for %ld sec.\n", left);
    esp_sleep_enable_timer_wakeup(left * uS_TO_S_FACTOR);
    esp_deep_sleep_start();
    Serial.print("This will never be printed\n");
  } else {
    Serial.printf("Wait by delay : %d sec.\n", left);
    epoch_left = left;
    delay(left * 1000);
    for (;;) {
      getLocalTime(&timeInfo);
      if (timeInfo.tm_sec == 0) break;
      delay(10);
    }
  }
  getLocalTime(&timeInfo);
  Serial.printf("Epoch time : %04d-%02d-%02d %02d:%02d:%02d\n",
                timeInfo.tm_year + 1900, timeInfo.tm_mon + 1, timeInfo.tm_mday,
                timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec);
}

//
// measure data
//

void measure(void) {
  getLocalTime(&timeMeas);
  Serial.printf("Measure start : %04d-%02d-%02d %02d:%02d:%02d\n",
                timeMeas.tm_year + 1900, timeMeas.tm_mon + 1, timeMeas.tm_mday,
                timeMeas.tm_hour, timeMeas.tm_min, timeMeas.tm_sec);
  //
  sht35_getValues();
  core_temp = temperatureRead();
}

//
// send measure data via http
//

void http_send() {
  HTTPClient http;
  int httpCode;
  char buffer[256];
  Serial.print("http send start\n");
  sprintf(
    buffer,
    "%s?node=%s&"
    "date=%04d-%02d-%02dT"
    "%02d:%02d:%02d&"
    "wifi-ssid=%s&"
    "wifi-rssi=%d&"
    "boot=%d&"
    "battery=%f&"
    "latency=%d&"
    "core_temp=%f&"
    "temp=%f&"
    "humidity=%f",
    http_server, node_name,
    timeMeas.tm_year + 1900, timeMeas.tm_mon + 1, timeMeas.tm_mday,
    timeMeas.tm_hour, timeMeas.tm_min, timeMeas.tm_sec,
    WiFi.SSID().c_str(),
    WiFi.RSSI(),
    bootCount,
    meas_batt,
    epoch_left,
    core_temp,
    meas_temp,
    meas_humi
  );
  Serial.printf("%s\n", buffer);
  for (int i = 0; i < HTTP_RETRY; i ++) {
    http.begin(buffer);
    http.setTimeout(HTTP_TIMEOUT * 1000);
    httpCode = http.GET();
    if (httpCode > 0) {
      Serial.printf("[HTTP] GET... code: %d\n", httpCode);
      if (httpCode == HTTP_CODE_OK) {
        String payload = http.getString();
        Serial.println("Payload : " + payload);
        http.end();
        return;
      }
    } else {
      Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
      if (httpCode == HTTPC_ERROR_READ_TIMEOUT) {
        http.end();
        return;
      }
    }
    http.end();
    Serial.printf("http send retry after %d sec.\n",HTTP_RETRY_DELAY);
    delay(HTTP_RETRY_DELAY * 1000);
  }
}

//
// deep sleep for next time
//

void deep_sleep_next() {
  time_t now, left;
  now = time(NULL);
  left = (TIME_TO_SLEEP - (now % TIME_TO_SLEEP)) - (TIME_TO_ON + WAKE_OFFSET);
  if (left < 0) {
    delay(-left * 1000);
    now = time(NULL);
    left = (TIME_TO_SLEEP - (now % TIME_TO_SLEEP)) - (TIME_TO_ON + WAKE_OFFSET);
  }
  bootCount ++;
  Serial.printf("Boot count : %d\n", bootCount);
  if (bootCount > (86400 / TIME_TO_SLEEP)) {
    Serial.printf("hungup guard reboot\n");
    bootCount = 0;
    ESP.restart();
  } else {
    Serial.printf("Deep sleep for %ld sec.\n", left);
    Serial.print("Going to sleep now\n");
    esp_sleep_enable_timer_wakeup((unsigned long)left * uS_TO_S_FACTOR);
    esp_deep_sleep_start();
    Serial.print("This will never be printed\n");
  }
}

//
// setup main
//

void setup() {
  hw_init();
  read_power_volts();
  wifi_init();
  ntp_init();
  wait_for_measure();
  measure();
  http_send();
  wifi_term();
  deep_sleep_next();
}

//
// main loop, send data via http
//

void loop() {}
