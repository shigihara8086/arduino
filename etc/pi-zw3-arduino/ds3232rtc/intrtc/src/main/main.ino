//Reference :
//https://github.com/JChristensen/DS3232RTC
//http://forum.arduino.cc/index.php?topic=109062.0
//http://donalmorrissey.blogspot.jp/2010/04/sleeping-arduino-part-5-wake-up-via.html
//http://easylabo.com/2015/04/arduino/8357/

#include <DS3232RTC.h>    //http://github.com/JChristensen/DS3232RTC
#include <Time.h>         //http://www.arduino.cc/playground/Code/Time
#include <Wire.h>         //http://arduino.cc/en/Reference/Wire (included with Arduino IDE)
#include <avr/sleep.h>
#include <avr/power.h>

bool rtcint = false;

void setup(void) {
  //clear all
  RTC.alarmInterrupt(1, false);
  RTC.alarmInterrupt(2, false);
  RTC.oscStopped(true);

  //sync time with RTC module
  Serial.begin(9600);
  setSyncProvider(RTC.get);   // the function to get the time from the RTC
  if(timeStatus() != timeSet) {
    Serial.println("Unable to sync with the RTC");
  } else {
    Serial.println("RTC has set the system time");
  }
  Serial.flush();

  //show current time, sync with 0 second
  digitalClockDisplay();
  synctozero();

  //set alarm to fire every minute
  RTC.alarm(2);
  attachInterrupt(1, alcall, FALLING);
  // RTC.setAlarm(ALM2_EVERY_MINUTE , 0, 0, 0);
  RTC.setAlarm(ALM2_MATCH_MINUTES, 0, 0, 0);
  RTC.alarmInterrupt(2, true);
  digitalClockDisplay();
}

void loop(void) {
  //process clock display and clear interrupt flag as needed
  if (rtcint) {
    rtcint = false;
    digitalClockDisplay();
    RTC.alarm(2);
  }

  //go to power save mode
  enterSleep();
}

void synctozero() {
  //wait until second reaches 0
  while (second() != 0) {
    delay(100);
  }
}

void alcall() {
  //per minute interrupt call
  rtcint = true;
}

void digitalClockDisplay(void) {
  // digital clock display of the time
  setSyncProvider(RTC.get); //sync time with RTC
  Serial.print(year());  Serial.print('/');
  printDigits(month());  Serial.print('/');
  printDigits(day());    Serial.print(' ');
  printDigits(hour());   Serial.print(':');
  printDigits(minute()); Serial.print(':');
  printDigits(second());
  Serial.println();
  Serial.flush();
}

void printDigits(int digits) {
  // utility function for digital clock display: prints preceding colon and leading 0
  if (digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

void enterSleep(void) {
  //enter sleep mode to save power
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_enable();
  sleep_mode();
  sleep_disable();
  power_all_enable();
}

