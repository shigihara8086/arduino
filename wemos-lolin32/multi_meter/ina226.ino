/*
  INA226
*/

#include <Wire.h>
#include "multi_meter.h"

#define INA226_ADD (0x40)

// I2c通信送信
void ina226_writeRegister(byte reg, word value) {
  Wire.beginTransmission(INA226_ADD);
  Wire.write(reg);
  Wire.write((value >> 8) & 0xFF);
  Wire.write(value & 0xFF);
  Wire.endTransmission();
}

// I2c通信受信
word ina226_readRegister(byte reg) {
  word res = 0x0000;

  Wire.beginTransmission(INA226_ADD);
  Wire.write(reg);
  if (Wire.endTransmission() == 0) {
    if (Wire.requestFrom(INA226_ADD, 2) >= 2) {
      res  = Wire.read() * 256;
      res += Wire.read();
    }
  }
  return res;
}

int ina226_init() {
  Wire.begin(I2C_SDA, I2C_SCL);
  ina226_writeRegister(0x00, 0x4127);
  ina226_writeRegister(0x05, 0x0A00);
  return 0;
}

void ina226_display(int times, int wait) {
  char strbuf[64];
  int head_pos =  0;
  int data_pos = 94 + 3;
  int unit_pos = 128;
  int volt_pos = (16 - LINE_OFFSET);
  int crnt_pos = (32 - LINE_OFFSET);
  int pwer_pos = (48 - LINE_OFFSET);
  bool done = false;

  for (int i = 0; i < times; i ++) {
    // 電圧、電流、電力読込み
    long voltage  = (long)((short)ina226_readRegister(0x02)) * 1250L; // LSB=1.25mV
    short current = (short)ina226_readRegister(0x04);
    long power    = (long)ina226_readRegister(0x03) * 25000L;         // LSB=25mW

    display.clear();
    ntp_ticker();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(0, 0, "INA226");

    display.setFont(ArialMT_Plain_16);
    // display.setFont(Monospaced_plain_16);

    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(head_pos, volt_pos, "Volt");
    display.drawString(head_pos, crnt_pos, "Current");
    display.drawString(head_pos, pwer_pos, "Power");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(unit_pos, volt_pos, "mV");
    display.drawString(unit_pos, crnt_pos, "mA");
    display.drawString(unit_pos, pwer_pos, "mW");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    sprintf(strbuf, "%5ld", (voltage + (1000 / 2)) / 1000);
    display.drawString(data_pos, volt_pos, strbuf);
    sprintf(strbuf, "%5ld", current);
    display.drawString(data_pos, crnt_pos, strbuf);
    sprintf(strbuf, "%5ld", (power + (1000 / 2)) / 1000);
    display.drawString(data_pos, pwer_pos, strbuf);

    if (not done) {
      Serial.printf("V:%5ldmV,I:%5ldmA,P:%5ldmW ", (voltage + (1000 / 2)) / 1000, current, (power + (1000 / 2)) / 1000);
      done = true;
    }
    display.display();
    delay(wait);
  }
}

