/*
  ESP8266 mDNS responder sample

  This is an example of an HTTP server that is accessible
  via http://esp8266.local URL thanks to mDNS responder.

  Instructions:
  - Update WiFi SSID and password as necessary.
  - Flash the sketch to the ESP8266 board
  - Install host software:
    - For Linux, install Avahi (http://avahi.org/).
    - For Windows, install Bonjour (http://www.apple.com/support/bonjour/).
    - For Mac OSX and iOS support is built in through Bonjour already.
  - Point your browser to http://esp8266.local, you should see a response.

 */

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiClient.h>
#include <Wire.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <RtcDS3231.h>
#include "ina226.h"

#define ONE_WIRE_BUS 2
#define LED 16
#define countof(a) (sizeof(a) / sizeof(a[0]))

// OneWire
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
RtcDS3231<TwoWire> Rtc(Wire);

const char* ssid = "106F3FDBCFA9";
const char* password = "amhh8khbexssm";
// TCP server at port 80 will respond to HTTP requests
WiFiServer server(80);

void setup(void) {
  pinMode(LED, OUTPUT);
  digitalWrite(LED, HIGH);
  Wire.begin();    // I2C
  setupRegister(); // ina226
  sensors.begin(); // OneWire
  Rtc.Begin();     // DS3231
  //
  Serial.begin(74880);
  // Connect to WiFi network
  WiFi.begin(ssid,password);
  Serial.println("");  
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  // Set up mDNS responder:
  // - first argument is the domain name, in this example
  //   the fully-qualified domain name is "esp8266.local"
  // - second argument is the IP address to advertise
  //   we send our IP address on the WiFi network
  if(!MDNS.begin("esp8266")) {
    Serial.println("Error setting up MDNS responder!");
    while(1) { 
      delay(1000);
    }
  }
  Serial.println("mDNS responder started");
  // Start TCP (HTTP) server
  server.begin();
  Serial.println("TCP server started");
  // Add service to MDNS-SD
  MDNS.addService("http","tcp",80);
}

void loop(void) {
  float voltage;       // Bus Voltage (mV)
  float current;       // Current (mA)
  float power;         // Power (mW)
  float temp;          // Temperature
  char datestring[20]; // DateTime string buffer

  // Check if a client has connected
  WiFiClient client = server.available();
  if(!client) {
    return;
  }
  Serial.println("");
  Serial.println("New client");
  // Wait for data from client to become available
  while(client.connected() && !client.available()) {
    delay(1);
  }
  // Read the first line of HTTP request
  String req = client.readStringUntil('\r');
  // First line of HTTP request looks like "GET /path HTTP/1.1"
  // Retrieve the "/path" part by finding the spaces
  int addr_start = req.indexOf(' ');
  int addr_end = req.indexOf(' ', addr_start + 1);
  if (addr_start == -1 || addr_end == -1) {
    Serial.print("Invalid request: ");
    Serial.println(req);
    return;
  }
  digitalWrite(LED, LOW);
  req = req.substring(addr_start + 1, addr_end);
  Serial.print("Request: ");
  Serial.println(req);
  client.flush();
  String s;
  if(req == "/") {
    // RTC
    RtcDateTime now = Rtc.GetDateTime();
    snprintf_P(datestring,countof(datestring),PSTR("%04u/%02u/%02u %02u:%02u:%02u"),
      now.Year(),now.Month(),now.Day(),now.Hour(),now.Minute(),now.Second());
    RtcTemperature rtctemp = Rtc.GetTemperature();
    // ina226
    voltage  = ((float)readRegister(INA226_REG_BUS_VOLTAGE)) * 1.25; // LSB=1.25mV
    current  = ((float)readRegister(INA226_REG_CURRENT));            // mA
    power    = ((float)readRegister(INA226_REG_POWER)) * 25.0;       // LSB=25mW
    // ds18b20
    sensors.requestTemperatures(); // Send the command to get temperatures
    temp = sensors.getTempCByIndex(0);
    //
    IPAddress ip = WiFi.localIP();
      String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
    s  = "HTTP/1.1 200 OK\r\n";
    s += "Content-Type: text/html; charset=UTF-8\r\n";
    s += "Cache-Control: no-cache\r\n";
    s += "\r\n";
    s += "<!DOCTYPE HTML>\r\n";
    s += "<html>\r\n";
    s += "<head>\r\n";
    // s += "<meta http-equiv=\"refresh\" content=\"30\">\r\n";
    s += "</head>\r\n";
    s += "Web-meter by ESP8266<br>\r\n";
    s += "MyIP: "; s += ipStr; s += "<br>\r\n";
    s += "日時: "; s += String(datestring); s += "<br>\r\n";
    s += "気温: DS18B20: "; s += String(temp);    s += "℃,RTC: "; s += String(rtctemp.AsFloat()); s += "℃<br>\r\n";
    s += "電圧: "; s += String(voltage); s += "mV<br>\r\n";
    s += "電流: "; s += String(current); s += "mA<br>\r\n";
    s += "電力: "; s += String(power);   s += "mW<br>\r\n";
    s += "</html>\r\n";
    s += "\r\n";
    Serial.println("Sending 200");
  } else {
    s = "HTTP/1.1 404 Not Found\r\n\r\n";
    Serial.println("Sending 404");
  }
  client.print(s);
  digitalWrite(LED, HIGH);
  Serial.println("Done with client");
}

