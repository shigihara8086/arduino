/*
 PIR(人感センサー)
*/

#if defined(ENABLE_PIR)

int pir_pin = 13;
int led_pin = 15;
int pir_status;
int pir_count = 0;

//
// config text
//

char *node_config_pir =
  "graph_title 人感センサー(%s)\n"
  "graph_args --base 1000 -r -l 0 --alt-autoscale\n"
  "graph_vlabel 回\n"
  "graph_scale no\n"
  "graph_category measure_pir\n"
  "graph_info 過去5分間に人感センサーの前を通過した回数\n"
  "pir.draw AREA\n"
  "pir.label 回数\n"
  "pir.info 回数\n";

//
// initialize
//

void pir_init() {
  pinMode(pir_pin, INPUT);
  pinMode(led_pin, OUTPUT);
  pir_count = 0;
  pir_status = digitalRead(pir_pin);
}

//
// sampling loop
//

void pir_loop() {
int st;
  st = digitalRead(pir_pin);
  if(st != pir_status) {
    // getLocalTime(&timeInfo);
    if(st) {
      pir_count ++;
      digitalWrite(led_pin,HIGH);
      Serial.print("O");
      // Serial.println("ON");
      // Serial.printf("ON  : %04d-%02d-%02d %02d:%02d:%02d\n",
      //               timeInfo.tm_year + 1900, timeInfo.tm_mon + 1, timeInfo.tm_mday,
      //               timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec);
    } else {
      digitalWrite(led_pin,LOW);
      Serial.print(".");
      // Serial.println("OFF");
      // Serial.printf("OFF : %04d-%02d-%02d %02d:%02d:%02d\n",
      //               timeInfo.tm_year + 1900, timeInfo.tm_mon + 1, timeInfo.tm_mday,
      //               timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec);
    }
    pir_status = st;
  }
}

//
// config
//

void do_config_pir(WiFiClient &client) {
  client.printf(node_config_pir,node_name);
  Serial.printf(node_config_pir,node_name);
}

//
// fetch
//

void do_fetch_pir(WiFiClient &client) {
  client.print("pir.value ");
  if(isnan(pir_count)) {
    client.print("U");
    Serial.println("Failed to read temperature");
  } else {
    client.print(pir_count);
    Serial.printf("pir: %d\n", pir_count);
  }
  client.print("\n");
  pir_count = 0;
}
#endif
