/*
 * pir test
*/

int pir = 13;
int led = 15;
int pir_status;

struct tm timeInfo;

void setup() {
  pinMode(pir, INPUT);
  pinMode(led, OUTPUT);
  Serial.begin(115200);
  Serial.println("人感センサースタート");
  pir_status = digitalRead(pir);
}

void loop() {
int st;
  st = digitalRead(pir);
  if(st != pir_status) {
    getLocalTime(&timeInfo);
    if(st) {
      digitalWrite(led,HIGH);
      Serial.printf("ON  : %04d-%02d-%02d %02d:%02d:%02d\n",
                    timeInfo.tm_year + 1900, timeInfo.tm_mon + 1, timeInfo.tm_mday,
                    timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec);
    } else {
      digitalWrite(led,LOW);
      Serial.printf("OFF : %04d-%02d-%02d %02d:%02d:%02d\n",
                    timeInfo.tm_year + 1900, timeInfo.tm_mon + 1, timeInfo.tm_mday,
                    timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec);
    }
    pir_status = st;
  }
  delay(1000); 
}
