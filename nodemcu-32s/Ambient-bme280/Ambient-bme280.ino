#include <WiFi.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include "Ambient.h"

#define HOSTNAME "ambient-bme280"

unsigned int channelId = 2575;                  // AmbientのチャネルID
const char* writeKey = "0bde5a9e38390754";      // ライトキー

WiFiClient client;
Ambient ambient;

double meas_temp  = 0.0;
double meas_humi  = 0.0;
double meas_dewp  = 0.0;
double meas_press = 0.0;

long loop_count = 0;

void setup() {
  Serial.begin(115200);
  wifi_init();
  bme280_init();
  ambient.begin(channelId, writeKey, &client);  // チャネルIDとライトキーを指定してAmbientの初期化
  bme280_getValues();
  ambient.set(1, meas_temp);
  ambient.set(2, meas_humi);
  ambient.set(3, meas_press);
  ambient.set(4, meas_dewp);
  ambient.send();
}

void loop() {
  ArduinoOTA.handle();
  loop_count ++;
  if(loop_count > 1000 * 60 * 5) {
    loop_count = 0;
    bme280_getValues();
    ambient.set(1, meas_temp);
    ambient.set(2, meas_humi);
    ambient.set(3, meas_press);
    ambient.set(4, meas_dewp);
    ambient.send();
  }
  delay(1);
}
