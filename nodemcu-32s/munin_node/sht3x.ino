/*
 SHT3x
*/

#if defined(ENABLE_SHT3X)

char *node_config_sht3x_temp =
  "graph_title 温度(%s)\n"
  "graph_args --base 1000 --alt-autoscale\n"
  "graph_vlabel ℃\n"
  "graph_scale no\n"
  "graph_order temp inter trend\n"
  "graph_category measure_temperature\n"
  "graph_info SHT3Xセンサーで測定した温度\n"
  "temp.label 温度　　\n"
  "temp.info SHT3Xセンサーの温度\n"
  "inter.cdef temp,UN,PREV,temp,IF\n"
  "inter.graph no\n"
  "inter.update no\n"
  "trend.label 移動平均\n"
  "trend.line 25.0:880000\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info 温度の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

char *node_config_sht3x_humi =
  "graph_title 湿度(%s)\n"
  "graph_args --base 1000 --alt-autoscale\n"
  "graph_vlabel %%\n"
  "graph_scale no\n"
  "graph_order humi inter trend\n"
  "graph_category measure_humidity\n"
  "graph_info SHT3Xセンサーで測定した湿度\n"
  "humi.label 湿度　　\n"
  "humi.info SHT3Xセンサーの湿度\n"
  "inter.cdef humi,UN,PREV,humi,IF\n"
  "inter.graph no\n"
  "inter.update no\n"
  "trend.label 移動平均\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info 湿度の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

char *node_config_sht3x_di =
  "graph_title 不快指数(%s)\n"
  "graph_args --base 1000 -l 55.0 -u 85.0 --alt-y-grid --alt-autoscale\n"
  "graph_vlabel DI\n"
  "graph_scale no\n"
  "graph_order di inter trend\n"
  "graph_category measure_di\n"
  "graph_info SHT3Xセンサーの温度・湿度から計算した不快指数\n"
  "di.label 不快指数\n"
  "di.info SHT3Xセンサーの温度・湿度から計算\n"
  "di.warning 60:80\n"
  "di.critical 55:85\n"
  "inter.cdef di,UN,PREV,di,IF\n"
  "inter.graph no\n"
  "inter.update no\n"
  "trend.warning 55:85\n"
  "trend.critical 55:85\n"
  "trend.label 移動平均\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info 不快指数の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

char *node_config_sht3x_wbgt =
  "graph_title 暑さ指数(%s)\n"
  "graph_args --base 1000 --alt-y-grid --alt-autoscale\n"
  "graph_vlabel ℃\n"
  "graph_scale no\n"
  "graph_order wbgt inter trend\n"
  "graph_category measure_wbgt\n"
  "graph_info SHT3Xセンサーの温度・湿度から計算した暑さ指数\n"
  "wbgt.label 暑さ指数\n"
  "wbgt.info SHT3Xセンサーの温度・湿度から計算\n"
  "wbgt.warning  :28.0\n"
  "wbgt.critical :31.0\n"
  "inter.cdef wbgt,UN,PREV,wbgt,IF\n"
  "inter.graph no\n"
  "inter.update no\n"
  "trend.label 移動平均\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info 暑さ指数の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

char *node_config_sht3x_dewp =
  "graph_title 露点温度(%s)\n"
  "graph_args --base 1000 --alt-y-grid --alt-autoscale\n"
  "graph_vlabel ℃\n"
  "graph_scale no\n"
  "graph_order dewp inter trend\n"
  "graph_category measure_dewpoint\n"
  "graph_info SHT3Xセンサーの温度・湿度から計算した露点温度\n"
  "dewp.label 露点温度\n"
  "dewp.info SHT3Xセンサーの温度・湿度から計算\n"
  "inter.cdef dewp,UN,PREV,dewp,IF\n"
  "inter.graph no\n"
  "inter.update no\n"
  "trend.label 移動平均\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info 露点温度の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

#include <Wire.h>

#define I2C_SDA  (22)
#define I2C_SCL  (21)
#define I2C_ADDR (0x44)

float meas_temp = 0.0;
float meas_humi = 0.0;
float meas_di   = 0.0;
float meas_wbgt = 0.0;
float meas_dewp = 0.0;

// 露点温度計算用のダミー
float meas_press = 1013.25;

int sht3x_init() {
  Wire.begin(I2C_SCL, I2C_SDA);
  delay(300);
  sht3x_getValues();
  return(0);
}

void sht3x_getValues(void) {
unsigned int data[6];
float sr = 0.0;
float ws = 0.0;
  while(true) {
    // Start I2C Transmission
    Wire.beginTransmission(I2C_ADDR);
    // Send measurement command
    Wire.write(0x2C);
    Wire.write(0x06);
    // Stop I2C transmission
    Wire.endTransmission();
    delay(500);
    // Request 6 bytes of data
    Wire.requestFrom(I2C_ADDR, 6);
  
    // Read 6 bytes of data
    // cTemp msb, cTemp lsb, cTemp crc, humidity msb, humidity lsb, humidity crc
    if (Wire.available() == 6) {
      data[0] = Wire.read();
      data[1] = Wire.read();
      data[2] = Wire.read();
      data[3] = Wire.read();
      data[4] = Wire.read();
      data[5] = Wire.read();
    }
    // Convert the data
    meas_temp = ((((data[0] * 256.0) + data[1]) * 175) / 65535.0) - 45;
    meas_humi = ((((data[3] * 256.0) + data[4]) * 100) / 65535.0);
    if(meas_temp > -50 && meas_temp < 100 && meas_humi >= 0 && meas_humi <= 100) {
      meas_di   = 0.81 * meas_temp + (0.01 * meas_humi * (0.99 * meas_temp - 14.3)) + 46.3;
      meas_wbgt = 0.735 * meas_temp + 0.0374 * meas_humi + 0.00292 * meas_temp * meas_humi + 7.619 * sr - 4.557 * sr * sr - 0.0572 * ws - 4.064;
      meas_dewp = computeByRH(meas_temp, meas_humi);
      // Output data to serial monitor
      Serial.printf("気温     : %5.1fºC\n", meas_temp);
      Serial.printf("湿度     : %5.1f%%\n", meas_humi);
      Serial.printf("不快指数 :   %5.1f\n", meas_di);
      Serial.printf("暑さ指数 : %5.1fºC\n", meas_wbgt);
      Serial.printf("露点温度 : %5.1fºC\n", meas_dewp);
      break;
    }
  }
}

//
// config
//

void do_config_sht3x_temp(WiFiClient &client) {
  sht3x_getValues();
  client.printf(node_config_sht3x_temp,node_name);
  Serial.printf(node_config_sht3x_temp,node_name);
}

void do_config_sht3x_humi(WiFiClient &client) {
  client.printf(node_config_sht3x_humi,node_name);
  Serial.printf(node_config_sht3x_humi,node_name);
}

void do_config_sht3x_di(WiFiClient &client) {
  client.printf(node_config_sht3x_di,node_name);
  Serial.printf(node_config_sht3x_di,node_name);
}

void do_config_sht3x_wbgt(WiFiClient &client) {
  client.printf(node_config_sht3x_wbgt,node_name);
  Serial.printf(node_config_sht3x_wbgt,node_name);
}

void do_config_sht3x_dewp(WiFiClient &client) {
  client.printf(node_config_sht3x_dewp,node_name);
  Serial.printf(node_config_sht3x_dewp,node_name);
}

//
// fetch
//

void do_fetch_sht3x_temp(WiFiClient &client) {
  client.printf("temp.value %f\n", meas_temp);
  Serial.printf("meas_temp: %5.1f\n", meas_temp);
}

void do_fetch_sht3x_humi(WiFiClient &client) {
  client.printf("humi.value %f\n", meas_humi);
  Serial.printf("meas_humi: %5.1f\n", meas_humi);
}

void do_fetch_sht3x_di(WiFiClient &client) {
  client.printf("di.value %f\n",meas_di);
  Serial.printf("meas_di: %5.1f\n", meas_di);
}

void do_fetch_sht3x_wbgt(WiFiClient &client) {
  client.printf("wbgt.value %f\n", meas_wbgt);
  Serial.printf("meas_wbgt: %5.1f\n", meas_wbgt);
}

void do_fetch_sht3x_dewp(WiFiClient &client) {
  client.printf("dewp.value %f\n", meas_dewp);
  Serial.printf("meas_dewp: %5.1f\n", meas_dewp);
}

//
// loop
//

void sht3x_loop() {
}

#endif
