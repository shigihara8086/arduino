#include <Time.h>
#include <Wire.h>
#include "RTClib.h"
RTC_DS1307 RTC;

#define OFFSET = 10

void setup () {
    Serial.begin(115200);
    Wire.begin();
    RTC.begin();
  if (! RTC.isrunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    RTC.adjust(DateTime(__DATE__, __TIME__));
  }
  Serial.println(__DATE__);
  Serial.println(__TIME__);
  // RTC.adjust(DateTime(__DATE__, __TIME__));
}
void loop () {
    DateTime now = RTC.now();
    Serial.print(now.year(), DEC);
    Serial.print('/');
    if(now.month() < 10) { Serial.print("0"); }
    Serial.print(now.month(), DEC);
    Serial.print('/');
    if(now.day() < 10) { Serial.print("0"); }
    Serial.print(now.day(), DEC);
    Serial.print(' ');
    if(now.hour() < 10) { Serial.print("0"); }
    Serial.print(now.hour(), DEC);
    Serial.print(':');
    if(now.minute() < 10) { Serial.print("0"); }
    Serial.print(now.minute(), DEC);
    Serial.print(':');
    if(now.second() < 10) { Serial.print("0"); }
    Serial.print(now.second(), DEC);
    Serial.println(); 
    delay(1000);
}

