/*
 Munin-node
*/

#if defined(NODE_BLANK)
#define ENABLE_BLANK
// #define ENABLE_PIR                               // 人感センサー
#define ENABLE_HALL                              // ホールセンサー
// #define ENABLE_BME280                            // BME280,SHT3X,DHT11,ADT7410は排他
// #define ENABLE_SHT3X
// #define ENABLE_DHT11
// #define ENABLE_DHT22
// #define ENABLE_ADT7410
char *node_name = "blank";
#endif

#if defined(NODE_WROVER)
#define ENABLE_BLANK
// #define ENABLE_PIR                               // 人感センサー
#define ENABLE_HALL                              // ホールセンサー
// #define ENABLE_BME280                            // BME280,SHT3X,DHT11,ADT7410は排他
// #define ENABLE_SHT3X
// #define ENABLE_DHT11
// #define ENABLE_DHT22
// #define ENABLE_ADT7410
char *node_name = "wrover";
#endif

#if defined(NODE_PIR)
#define ENABLE_BLANK
#define ENABLE_PIR
#define ENABLE_HALL                              // ホールセンサー
// #define ENABLE_BME280                            // BME280,SHT3X,DHT11,ADT7410は排他
// #define ENABLE_SHT3X
// #define ENABLE_DHT11
// #define ENABLE_DHT22
// #define ENABLE_ADT7410
char *node_name = "pir";
#endif

#if defined(NODE_PIR2)
#define ENABLE_BLANK
// #define ENABLE_PIR
#define ENABLE_HALL                              // ホールセンサー
// #define ENABLE_BME280                            // BME280,SHT3X,DHT11,ADT7410は排他
// #define ENABLE_SHT3X
// #define ENABLE_DHT11
// #define ENABLE_DHT22
// #define ENABLE_ADT7410
char *node_name = "pir2";
#endif

#if defined(NODE_STOREROOM)
#define ENABLE_BLANK
// #define ENABLE_PIR
#define ENABLE_HALL
#define ENABLE_BME280
#define I2C_SDA (21)
#define I2C_SCL (22)
// #define ENABLE_SHT3X
// #define ENABLE_DHT11
// #define ENABLE_DHT22
// #define ENABLE_ADT7410
char *node_name = "store";
#endif

#if defined(NODE_COOLERBOX)
#define ENABLE_BLANK
// #define ENABLE_PIR
// #define ENABLE_HALL
// #define ENABLE_BME280
// #define ENABLE_SHT3X
#define ENABLE_DHT11
// #define ENABLE_DHT22
// #define ENABLE_ADT7410
char *node_name = "coolerbox";
#endif

#if defined(NODE_FLOOR)
#define ENABLE_BLANK
// #define ENABLE_PIR
// #define ENABLE_HALL
// #define ENABLE_BME280
// #define ENABLE_SHT3X
// #define ENABLE_DHT11
// #define ENABLE_DHT22
#define ENABLE_ADT7410
char *node_name = "floor";
#endif

#if defined(NODE_LIVING)
#define ENABLE_BLANK
// #define ENABLE_PIR
// #define ENABLE_HALL
#define ENABLE_BME280
#define I2C_SDA (22)
#define I2C_SCL (21)
// #define ENABLE_SHT3X
// #define ENABLE_SHT35
// #define ENABLE_DHT11
// #define ENABLE_DHT22
// #define ENABLE_ADT7410
char *node_name = "living";
#endif

#if defined(NODE_DS18B20)
// #define ENABLE_BLANK
// #define ENABLE_PIR
// #define ENABLE_HALL
// #define ENABLE_BME280
// #define ENABLE_SHT3X
// #define ENABLE_SHT35
// #define ENABLE_DHT11
// #define ENABLE_DHT22
// #define ENABLE_ADT7410
// #define ENABLE_DS18B20
char *node_name = "ds18b20";
#endif

#if defined(NODE_OUTER2)
#define ENABLE_BLANK
// #define ENABLE_PIR
// #define ENABLE_HALL
// #define ENABLE_BME280
// #define ENABLE_SHT3X
// #define ENABLE_DHT11
#define ENABLE_DHT22
// #define ENABLE_ADT7410
char *node_name = "outer2";
#endif

#if defined(NODE_LOCAL2)
// #define ENABLE_BLANK
// #define ENABLE_PIR
// #define ENABLE_HALL
#define ENABLE_BME280
#define I2C_SDA (25)
#define I2C_SCL (21)
// #define ENABLE_SHT3X
// #define ENABLE_SHT35
// #define ENABLE_DHT11
// #define ENABLE_DHT22
// #define ENABLE_ADT7410
char *node_name = "local2";
#endif

#if defined(NODE_CO2)
// #define ENABLE_BLANK
// #define ENABLE_PIR
// #define ENABLE_HALL
// #define ENABLE_BME280
// #define ENABLE_SHT3X
// #define ENABLE_SHT35
// #define ENABLE_DHT11
// #define ENABLE_DHT22
// #define ENABLE_ADT7410
// #define ENABLE_CCS811
#define ENABLE_MHZ19
char *node_name = "co2";
#endif

#if defined(NODE_CO22)
// #define ENABLE_BLANK
// #define ENABLE_PIR
// #define ENABLE_HALL
// #define ENABLE_BME280
// #define ENABLE_SHT3X
// #define ENABLE_SHT35
// #define ENABLE_DHT11
// #define ENABLE_DHT22
// #define ENABLE_ADT7410
// #define ENABLE_CCS811
#define ENABLE_MHZ19
char *node_name = "co22";
#endif
