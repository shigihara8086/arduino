/*
sht35
*/

#include "Seeed_SHT35.h"

#define SDAPIN  32
#define SCLPIN  33
#define RSTPIN  2
#define I2CADR  0x45

SHT35 sensor(SCLPIN,I2CADR);

int sht35_init() {
  if(sensor.init()) {
    Serial.println("sensor init failed!!!");
    return(-1);
  }
  return(0);
}

void sht35_getValues() {
  u16 value = 0;
  u8 data[6] = {0};
  if (NO_ERROR != sensor.read_meas_data_single_shot(HIGH_REP_WITH_STRCH, &meas_temp, &meas_humi)) {
    Serial.println("read temp failed!!");
  } else {
    /*
    M5.Lcd.setCursor(16, 10);          M5.Lcd.printf("TEMP %5.1fC", meas_temp);
    M5.Lcd.setCursor(16, 10 + 18);     M5.Lcd.printf("HUMI %5.1f%%", meas_humi);
    M5.Lcd.setCursor(16, 10 + 18 * 2); M5.Lcd.printf("VBAT %5.1fV", meas_batt);
    */
#if 1
    Serial.print("温度 = "); Serial.print(meas_temp); Serial.print("℃ ");
    Serial.print("湿度 = "); Serial.print(meas_humi); Serial.print("％ ");
    Serial.print("電圧 = "); Serial.print(meas_batt); Serial.println("V");
#endif
  }
}
