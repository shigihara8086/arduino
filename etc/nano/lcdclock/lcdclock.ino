/* Demonstration sketch for PCF8574T I2C LCD Backpack 
Uses library from https://bitbucket.org/fmalpartida/new-liquidcrystal/downloads GNU General Public License, version 3 (GPL-3.0) */
#include <Time.h>
#include <Wire.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>
#include "RTClib.h"
#include <OneWire.h>
#include <DallasTemperature.h>

#include "ina226.h"

#define ONE_WIRE_BUS 10 // データ(黄)で使用するポート番号
#define SENSER_BIT    9 // 精度の設定bit

#if 0
#define OFFSET 9
#endif

RTC_DS1307 RTC;
LiquidCrystal_I2C lcd(0x27,2,1,0,4,5,6,7); // 0x27 is the I2C bus address for an unmodified backpack
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

void myPrintf(char *fmt, ...) {
  char buf[128];
  va_list args;
  va_start(args,fmt);
  vsnprintf(buf,128,fmt,args);
  va_end(args);
  lcd.print(buf);
}

void setup() {
  Serial.begin(115200);
  Wire.begin();
  RTC.begin();
  if (! RTC.isrunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    #ifdef OFFSET
    DateTime compile_time = DateTime(__DATE__,__TIME__);
    DateTime now (compile_time.unixtime() + OFFSET);
    RTC.adjust(now);
    #endif
  }
  #ifdef OFFSET
  DateTime pc_time = DateTime(__DATE__,__TIME__);
  DateTime now (pc_time.unixtime() + OFFSET);
  RTC.adjust(now);
  #endif
  // activate LCD module
  lcd.begin(20,4); // for 20 x 4 LCD module
  lcd.setBacklightPin(3,POSITIVE);
  lcd.setBacklight(HIGH);
  // lcd.setCursor(0,0);        // go to start of 1st line
  // lcd.print("====Simple clock====");
  sensors.setResolution(SENSER_BIT);
  setupRegister(); // ina226 setup
}

void loop() {
  DateTime now = RTC.now();
  float voltage;   // Bus Voltage (mV)
  float current;   // Current (mA)
  float  power;    // Power (mW)

  lcd.setCursor(0,0);
  lcd.print("Date ");
  lcd.print(now.year(),DEC); lcd.print('/');
  if(now.month() < 10) { lcd.print("0"); } lcd.print(now.month(),DEC); lcd.print('/');
  if(now.day()   < 10) { lcd.print("0"); } lcd.print(now.day(),  DEC);
  lcd.print('(');
  switch(now.dayOfWeek()) {
    case 0: lcd.print("Sun"); break;
    case 1: lcd.print("Mon"); break;
    case 2: lcd.print("Tue"); break;
    case 3: lcd.print("Wed"); break;
    case 4: lcd.print("Thu"); break;
    case 5: lcd.print("Fri"); break;
    case 6: lcd.print("Sat"); break;
    default:
      lcd.print("***");
  }
  lcd.print(')');
  //
  lcd.setCursor(0,1);
  lcd.print("Time ");
  if(now.hour()   < 10) { lcd.print("0"); } lcd.print(now.hour(),  DEC); lcd.print(':');
  if(now.minute() < 10) { lcd.print("0"); } lcd.print(now.minute(),DEC); lcd.print(':');
  if(now.second() < 10) { lcd.print("0"); } lcd.print(now.second(),DEC);
  //
  // lcd.setCursor(0,2);
  // lcd.print("Temp ");
  sensors.requestTemperatures();
  lcd.setCursor(14,1);
  lcd.print(sensors.getTempCByIndex(0),1);
  lcd.print("\xdf"); lcd.print("C");
  //
  // voltage  = (long)((short)readRegister(INA226_REG_BUS_VOLTAGE)) * 1250.0; // LSB=1.25mV
  voltage  = ((float)readRegister(INA226_REG_BUS_VOLTAGE)) * 1.25; // LSB=1.25mV
  current  = ((float)readRegister(INA226_REG_CURRENT));            // mA
  power    = ((float)readRegister(INA226_REG_POWER)) * 25.0;       // LSB=25mW
  lcd.setCursor( 0,2); lcd.print("V/A");
  lcd.setCursor( 5,2); myPrintf("%05dmV,%05dmA",(int)voltage,(int)current);
  lcd.setCursor( 0,3); lcd.print("W");
  lcd.setCursor( 5,3); myPrintf("%05dmW",(int)power);
  //
  delay(50);
}

