/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Blink
*/

int lte_power               = 2;               // GPIO23 lte_power on digital port

// the setup function runs once when you press reset or power the board
void setup() {
  Serial.begin(115200);
  pinMode(lte_power, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  delay(2000);
  digitalWrite(lte_power, HIGH);      // turn the LED on (HIGH is the voltage level)
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
}

// the loop function runs over and over again forever
void loop() {
//  digitalWrite(lte_power, HIGH);      // turn the LED on (HIGH is the voltage level)
//  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
//  delay(60 * 1000);                       // wait for a second
//  digitalWrite(lte_power, LOW);     // turn the LED on (HIGH is the voltage level)
//  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
//  delay(10 * 1000);                       // wait for a second
}
