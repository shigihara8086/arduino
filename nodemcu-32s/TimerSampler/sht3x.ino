/*
SHT3x
*/

#if defined(ENABLE_SHT3X)

#include <Wire.h>

#define SHT3x_Addr (0x44)

int sht3x_init() {
  Wire.begin(SCLPIN, SDAPIN);
  delay(1000);
  return(0);
}

void sht3x_getValues(void) {
   unsigned int data[6];
  // Start I2C Transmission
  Wire.beginTransmission(SHT3x_Addr);
  // Send measurement command
  Wire.write(0x2C);
  Wire.write(0x06);
  // Stop I2C transmission
  Wire.endTransmission();
  delay(500);
  // Request 6 bytes of data
  Wire.requestFrom(SHT3x_Addr, 6);

  // Read 6 bytes of data
  // cTemp msb, cTemp lsb, cTemp crc, humidity msb, humidity lsb, humidity crc
  if (Wire.available() == 6) {
    data[0] = Wire.read();
    data[1] = Wire.read();
    data[2] = Wire.read();
    data[3] = Wire.read();
    data[4] = Wire.read();
    data[5] = Wire.read();
  }
  // Convert the data
  meas_temp = ((((data[0] * 256.0) + data[1]) * 175) / 65535.0) - 45;
  meas_humi = ((((data[3] * 256.0) + data[4]) * 100) / 65535.0);
  meas_dewp = computeByRH(meas_temp, meas_humi);
  // Output data to serial monitor
  Serial.printf("気温     : % 6.1f ºC\n", meas_temp);
  Serial.printf("湿度     : % 6.1f %%\n", meas_humi);
  Serial.printf("露点温度 : % 6.1f ºC\n", meas_dewp);
}
#endif
