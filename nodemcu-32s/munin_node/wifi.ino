/*
 Wi-Fi
*/

#include <WiFi.h>
#include <WiFiMulti.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>

//
// config text
//

char *node_config_rssi =
  "graph_title 信号強度(%s)\n"
  "graph_args --base 1000 -r -l -90 -u -20 --alt-autoscale\n"
  "graph_vlabel dBm\n"
  "graph_scale no\n"
  "graph_category measure_rssi\n"
  "graph_info ESP32のWi-Fi信号強度。-70dBmが通信が困難になる信号強度の目安。\n"
  "graph_order rssi inter trend\n"
  "rssi.label 信号強度\n"
  "rssi.info ESP32のWi-Fi信号強度\n"
  "rssi.warning  -70:\n"
  "rssi.critical -80:\n"
  "inter.cdef rssi,UN,PREV,rssi,IF\n"
  "inter.graph no\n"
  "inter.update no\n"
  "trend.label 移動平均\n"
  "trend.line -80:880000\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info 信号強度の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

char *node_config_channel =
  "graph_title Wi-Fi Channel(%s)\n"
  "graph_args --base 1000 -r -l 0 -u 15 --alt-autoscale\n"
  "graph_vlabel Channel\n"
  "graph_scale no\n"
  "graph_category measure_channel\n"
  "graph_info ESP32のWi-Fi Channel。どのAPを使っているかの目安。\n"
  "graph_order channel inter trend\n"
  "channel.label Channel\n"
  "channel.info ESP32のWi-Fi Channel\n"
  "inter.cdef channel,UN,PREV,channel,IF\n"
  "inter.graph no\n"
  "inter.update no\n";

//
// wifi init
//

struct WifiMulti_aps {                          // ssid,password pare
  char *ssid;
  char *passwd;
};

struct WifiMulti_aps wifi_aps[] = {             // ssid,password table
  { "aterm-20cd3f-g",  "65676ae9e9f9b" },
  { "106F3FDBCFA9_EXT","amhh8khbexssm" },
  { NULL, NULL }
};

WiFiMulti wifiMulti;                            // Multi AP

//
// OTA
//

void ota_init() {
  Serial.println("OTA initialize start");
  ArduinoOTA.setHostname(node_name);
  // ArduinoOTA.setPassword("admin");
  ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    })
    .onEnd([]() {
      Serial.println("\nEnd");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });
  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void wifi_init() {
int status;
int i;
  i = 0;
  while (wifi_aps[i].ssid) {
    wifiMulti.addAP(wifi_aps[i].ssid, wifi_aps[i].passwd);
    i ++;
  }
  Serial.print("WiFi connect start\n");
  WiFi.mode(WIFI_STA);
  i = 0;
  while(true) {
    status = wifiMulti.run();
    switch(status) {
      case WL_CONNECTED       : Serial.print("status : WL_CONNECTED\n"); break;
      case WL_NO_SHIELD       : Serial.print("status : WL_NO_SHIELD\n"); break;
      case WL_IDLE_STATUS     : Serial.print("status : WL_IDLE_STATUS\n"); break;
      case WL_NO_SSID_AVAIL   : Serial.print("status : WL_NO_SSID_AVAIL\n"); break;
      case WL_SCAN_COMPLETED  : Serial.print("status : WL_SCAN_COMPLETED\n"); break;
      case WL_CONNECT_FAILED  : Serial.print("status : WL_CONNECT_FAILED\n"); break;
      case WL_CONNECTION_LOST : Serial.print("status : WL_CONNECTION_LOST\n"); break;
      case WL_DISCONNECTED    : Serial.print("status : WL_DISCONNECTED\n"); break;
      default : {
        Serial.printf("status : Unknown : %d\n", status);
      }
    }
    if(status == WL_CONNECTED && WiFi.RSSI() != 0) break;
    i ++;
    if (i > 5) {
      Serial.print("Can not connect Wi-Fi. restart\n");
      ESP.restart();
    }
    while(WiFi.status() == WL_CONNECTED) {
      WiFi.disconnect();
      delay(100);
    }
    delay(1000);
  }
  Serial.printf("Connect Wi-Fi ssid:%s rssi:%d\n", WiFi.SSID().c_str(), WiFi.RSSI());
  // OTA
  ota_init();
  delay(1000);
/*
  // mDNS
  i = 0;
  while(!MDNS.begin(node_name)) {
    i ++;
    if(i > 5) {
      Serial.println("Error setting up MDNS responder!");
      ESP.restart();
    }
    delay(1000);
  }
  Serial.printf("mDNS responder %s.local is started\n",node_name);
  delay(1000);
*/
}

void wifi_term() {
  while(WiFi.status() == WL_CONNECTED) {
    WiFi.disconnect();
    delay(100);
  }
}

//
// config
//

void do_config_rssi(WiFiClient &client) {
  client.printf(node_config_rssi,node_name);
  Serial.printf(node_config_rssi,node_name);
}

void do_config_channel(WiFiClient &client) {
  client.printf(node_config_channel,node_name);
  Serial.printf(node_config_channel,node_name);
}

//
// fetch
//

void do_fetch_rssi(WiFiClient &client) {
int rssi;
  rssi = WiFi.RSSI();
  client.print("rssi.value ");
  if(isnan(rssi)) {
    client.print("U");
    Serial.println("Failed to read temperature");
  } else {
    client.print(rssi);
    Serial.printf("rssi: %d\n", rssi);
  }
  client.printf("\n");
}

void do_fetch_channel(WiFiClient &client) {
int channel;
  channel = WiFi.channel();
  client.print("channel.value ");
  if(isnan(channel)) {
    client.print("U");
    Serial.println("Failed to read temperature");
  } else {
    client.print(channel);
    Serial.printf("channel: %d\n", channel);
  }
  client.printf("\n");
}
