/*
  LIS3DH
*/

#include <Wire.h>
#include "multi_meter.h"

#define LIS3DH_ADD 0x19

#define DEFAULT_XA (0.0)
#define DEFAULT_YA (0.0)

unsigned int lis3dh_readRegister(byte reg) {
  Wire.beginTransmission(LIS3DH_ADD);
  Wire.write(reg);
  Wire.endTransmission();
  Wire.requestFrom(LIS3DH_ADD, 1);
  return Wire.read();
}

void lis3dh_writeRegister(byte reg, byte data) {
  Wire.beginTransmission(LIS3DH_ADD);
  Wire.write(reg);
  Wire.write(data);
  Wire.endTransmission();
}

int lis3dh_init() {
  lis3dh_writeRegister(0x20, 0x27);
  int res = lis3dh_readRegister(0x0f);
  return 0;
}

int s18(unsigned int v) {
  return -(v & 0b100000000000) | (v & 0b011111111111);
}

void lis3dh_display(int times, int wait) {
  char strbuf[100];
  unsigned int x, y, z, h, l;
  float xa, ya, za;
  int head_pos =  0;
  int data_pos = 94 + 4;
  int unit_pos = 128;
  int galx_pos = (16 - LINE_OFFSET);
  int galy_pos = (32 - LINE_OFFSET);
  int galz_pos = (48 - LINE_OFFSET);
  bool done = false;

  for (int i = 0; i < times; i ++) {
    // LIS3DH
    lis3dh_writeRegister(0x20, 0x27);
    // X
    l = lis3dh_readRegister(0x28);
    h = lis3dh_readRegister(0x29);
    x = (h << 8 | l) >> 4;
    xa = s18(x) / 1024.0 * 980.0 - DEFAULT_XA;
    // Y
    l = lis3dh_readRegister(0x2a);
    h = lis3dh_readRegister(0x2b);
    y = (h << 8 | l) >> 4;
    ya = s18(y) / 1024.0 * 980.0 - DEFAULT_YA;
    // Z
    l = lis3dh_readRegister(0x2c);
    h = lis3dh_readRegister(0x2d);
    z = (h << 8 | l) >> 4;
    za = s18(z) / 1024.0 * 980.0;
    display.clear();
    ntp_ticker();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(0, 0, "LIS3DH");

    display.setFont(ArialMT_Plain_16);
    // display.setFont(Monospaced_plain_16);

    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(head_pos, galx_pos, "Xaxis");
    display.drawString(head_pos, galy_pos, "Yaxis");
    display.drawString(head_pos, galz_pos, "Zaxis");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    sprintf(strbuf, "%5.1f", xa);
    display.drawString(data_pos, galx_pos, strbuf);
    sprintf(strbuf, "%5.1f", ya);
    display.drawString(data_pos, galy_pos, strbuf);
    sprintf(strbuf, "%5.1f", za);
    display.drawString(data_pos, galz_pos, strbuf);

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(unit_pos, galx_pos, "Gal");
    display.drawString(unit_pos, galy_pos, "Gal");
    display.drawString(unit_pos, galz_pos, "Gal");

    if (not done) {
      Serial.printf("X:%5.1fGal,Y:%5.1fGal,Z:%5.1fGal ", xa, ya, za);
      done = true;
    }
    display.display();
    delay(wait);
  }
}

