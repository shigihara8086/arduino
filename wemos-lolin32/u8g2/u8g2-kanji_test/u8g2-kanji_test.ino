#include <U8g2lib.h>

U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, /* clock=*/ 4, /* data=*/ 5, /* reset=*/ U8X8_PIN_NONE);

void setup() {
  u8g2.begin();
  u8g2.enableUTF8Print();
}

void loop() {
  u8g2.setFont(u8g2_font_f12_t_japanese1);
  u8g2.setFontDirection(0);
  u8g2.setCursor(0, 20);
  u8g2.print("あいう");
}

