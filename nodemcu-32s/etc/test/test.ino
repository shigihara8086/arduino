void setup() {
  Serial.begin(115200);
  Serial.printf("port:%d\n",LED_BUILTIN);
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  digitalWrite(LED_BUILTIN, HIGH);
  delay(100);
  digitalWrite(LED_BUILTIN, LOW);
  delay(100);
}
