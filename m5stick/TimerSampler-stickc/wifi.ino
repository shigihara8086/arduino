/*
Wi-Fi
*/

#include <WiFi.h>
#include <WiFiMulti.h>

WiFiMulti wifiMulti;                            // Multi AP

//
// setup Wi-Fi
//

void wifi_init() {
int status;
int i;
  i = 0;
  while (wifi_aps[i].ssid) {
    wifiMulti.addAP(wifi_aps[i].ssid, wifi_aps[i].passwd);
    i ++;
  }
  Serial.print("WiFi connect start\n");
  wait_lte_up();
  WiFi.mode(WIFI_STA);
  i = 0;
  while(true) {
    status = wifiMulti.run();
    Serial.printf("status : %d\n", status);
    if(status == WL_CONNECTED && WiFi.RSSI() != 0) break;
    if(i > 5) {
      Serial.print("Can not connect Wi-Fi. restart\n");
      digitalWrite(LTE_POWERLED, HIGH);
      ESP.restart();
    }
    i ++;
    delay(1000);
  }
  Serial.printf("Connect Wi-Fi ssid:%s rssi: %d\n",WiFi.SSID().c_str(),WiFi.RSSI());
}

void wifi_term() {
  while(WiFi.status() == WL_CONNECTED) {
    WiFi.disconnect();
    delay(2000);
  }
}
