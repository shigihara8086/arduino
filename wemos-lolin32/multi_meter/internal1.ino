/*
  internal sensors
*/

#include "multi_meter.h"

#define ADC_ADJUSTMENT (1.06)

int int1_init() {
  return 0;
}

void int1_display(int times, int wait) {
  char strbuf[64];
  int head_pos  =  0;
  int data_pos  = 94 + 6;
  int unit_pos  = 128;
  int adc_pos   = (16 - LINE_OFFSET);
  int hall_pos  = (32 - LINE_OFFSET);
  int touch_pos = (48 - LINE_OFFSET);
  bool done = false;

  for (int i = 0; i < times; i ++) {
    int adcv  = (int)(3.3 / 4096 * analogRead(A3) * 3 * 1000 * ADC_ADJUSTMENT);  // ADC(ADC3,SVN pin)
    int hall  = hallRead();                                                      // HALL effect sensor
    int touch = touchRead(T3);                                                   // touch sensor

    display.clear();
    ntp_ticker();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(0, 0, "INT");

    display.setFont(ArialMT_Plain_16);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(head_pos, adc_pos,   "Battery");
    display.drawString(head_pos, hall_pos,  "Hall");
    display.drawString(head_pos, touch_pos, "Touch");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(unit_pos, adc_pos,   "mV");
    display.drawString(unit_pos, hall_pos,  "dig");
    display.drawString(unit_pos, touch_pos, "dig");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    sprintf(strbuf, "%5ld", adcv);  display.drawString(data_pos, adc_pos,   strbuf);
    sprintf(strbuf, "%5ld", hall);  display.drawString(data_pos, hall_pos,  strbuf);
    sprintf(strbuf, "%5ld", touch); display.drawString(data_pos, touch_pos, strbuf);

    if (not done) {
      Serial.printf("V:%5ldmV,HL:%5ld,TCH:%5ld ", adcv,hall,touch);
      done = true;
    }
    display.display();
    delay(wait);
  }
}

