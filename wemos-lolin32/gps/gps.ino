// for Wemos Lolin-EPS32
static const uint32_t GPSBaud = 9600;
 
// The serial connection to the GPS device
 HardwareSerial GPSRaw(2);  // ESP32 UART2 GPIO-16 ( RXD2 ) --> GPS TXD
 
void setup() {
  Serial.begin(115200);
  GPSRaw.begin(GPSBaud, SERIAL_8N1, 16, 17);
}
 
void loop() {
  if (GPSRaw.available())
  {
    String Buffer = "";
    while (GPSRaw.available())
    {
       char GPSRX = GPSRaw.read();
       Buffer += GPSRX;
    }
    Serial.print(Buffer);
  }
}