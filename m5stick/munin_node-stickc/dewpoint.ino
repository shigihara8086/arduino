//
// 露点温度の計算
//

double computeByWB(double db, double wb) {
  double vp, xs, hw, ah, c, e, h, v, vpd, ahd, ph, rh, dp;

  vp = computeES(wb);
  xs = 0.622 * vp / (760 - vp);

  if (db <= 0) {
    hw = -334 + 2.09 * wb;
  } else {
    hw = 4.19 * wb ;
  }
  ah = (xs * (2500 + 1.85 * wb - hw) - 1.005 * (db - wb)) / (2500 + 1.85 * db - hw);
  e = 760 * ah / (ah + 0.622);
  h = 1.005 * db + (2500 + 1.85 * db ) * ah;
  v = 0.455 * (ah + 0.622) * (273.15 + db) / 100;
  vpd = computeES(db);
  ahd = 0.622 * vpd / (760 - vpd);
  ph = (ah / ahd) * 100;
  rh = (e / vpd) * 100;

  if (db == wb) {
    dp = db;
  } else {
    c = log(101325 * ah / (ah + 0.622));
    if (db > 0) {
      dp = -42.92 - 0.0514 * c + 1.052 * c * c;
    } else {
      dp = -60.02 + 6.803 * c + 0.3966 * c * c;
    }
  }
  // dp = round(dp);
  return (dp);
}

double computeByRH(double db, double rh) {
  double wb;

  wb = computeTempByRH(rh, db, -50);
  return (computeByWB(db, wb));
}

double computeES(double temp) {
  double a1 = 10.79574;
  double a2 = -5.028;
  double a3 = 1.50475e-4;
  double a4 = -8.2969;
  double a5 = 0.42873e-3;
  double a6 = 4.76955;
  double b1 = -9.09685;
  double b2 = -3.56654;
  double b3 = 0.87682;
  double atc = 273.15;
  double base, atemp, ps, es;

  base = log(10.0);

  atemp = temp + atc;

  if (temp > 0) {
    ps = a1 * (1 - atc / atemp) + a2 * log(atemp / atc)
         / base + a3 * (1 - pow(10, a4 * (atemp / atc - 1)))
         + a5 * (pow(10, a6 * (1 - atc / atemp)) - 1)
         + log(4.581) / base;
  } else {
    ps = b1 * (atc / atemp - 1) + b2 * log(atc / atemp)
         / base + b3 * (1 - atc / atemp) + log(4.581) / base;
  }
  es = pow(10, ps);
  return (es);
}

double computeTempByRH(double goalRH, double startTemp, double endTemp) {
  double temp, rh, dt;

  for (temp = startTemp; temp >= endTemp; temp --) {
    rh = computeRH(startTemp, temp);
    if (rh <= goalRH) break;
  }

  for (dt = 0.0; dt < 1.0; dt += 0.01) {
    rh = computeRH(startTemp, temp + dt);
    if (rh >= goalRH) break;
  }
  return (temp + dt);
}

double computeRH(double db, double wb) {
  double vp, xs, hw, ah, vpd, e, rh;

  vp = computeES(wb);
  xs = 0.622 * vp / (760 - vp);

  if (db <= 0) {
    hw = -334 + 2.09 * wb;
  } else {
    hw = 4.19 * wb;
  }
  ah = (xs * (2500 + 1.85 * wb - hw) - 1.005 * (db - wb)) / (2500 + 1.85 * db - hw);
  vpd = computeES(db);
  e = 760 * ah / (ah + 0.622);
  rh = (e / vpd) * 100;
  return (rh);
}
