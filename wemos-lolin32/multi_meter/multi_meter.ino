/*
  multi-meter
*/


#include "multi_meter.h"

#include <WiFi.h>
#if defined ENABLE_DTA
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#endif

#include <Wire.h>
#include "SSD1306.h"
#include "Adafruit_BME280.h"
#include "Adafruit_Sensor.h"

const char* ssid     = "106F3FDBCFA9";
const char* password = "amhh8khbexssm";

SSD1306 display(0x3c, I2C_SDA, I2C_SCL);

int bme280_disable;
int adt7410_disable;
int ds18x20_disable;
int ina226_disable;
int lis3dh_disable;
int ntp_disable;
int int1_disable;

int ssd1306_init() {
  display.init();
  display.flipScreenVertically();
  display.setBrightness(64);
  display.clear();
  display.setFont(ArialMT_Plain_16);
  // display.setFont(Monospaced_plain_16);
  display.setTextAlignment(TEXT_ALIGN_CENTER_BOTH);
  display.drawString(64, 32, "Multi-Meter");
  display.display();
  return 0;
}

void setup() {
  Serial.begin(115200);
#if defined ENABLE_OTA
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }
  ArduinoOTA
  .onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH)
      type = "sketch";
    else // U_SPIFFS
      type = "filesystem";

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    Serial.println("Start updating " + type);
  })
  .onEnd([]() {
    Serial.println("\nEnd");
  })
  .onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  })
  .onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });

  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
#endif
  ssd1306_init();
  bme280_disable  = bme280_init();
  adt7410_disable = adt7410_init();
  // ds18x20_disable  = ds18x20_init();
  ina226_disable  = ina226_init();
  lis3dh_disable  = lis3dh_init();
  ntp_disable     = ntp_init();
  int1_disable    = int1_init();
  delay(1000);
}

void loop() {
  Wire.reset();
  if (bme280_disable  == 0) {
    bme280_display (DISPLAY_TIMES, DISPLAY_WAIT);
  }
  if (adt7410_disable == 0) {
    adt7410_display(DISPLAY_TIMES, DISPLAY_WAIT);
  }
  // if (ds18x20_disable == 0) {
  //   ds18x20_display(DISPLAY_TIMES, DISPLAY_WAIT);
  // }
  if (ina226_disable  == 0) {
    ina226_display (DISPLAY_TIMES, DISPLAY_WAIT);
  }
  if (lis3dh_disable  == 0) {
    lis3dh_display (DISPLAY_TIMES, DISPLAY_WAIT);
  }
  if (true) {
    ntp_display    (DISPLAY_TIMES, DISPLAY_WAIT);
  }
  if (true) {
    int1_display   (DISPLAY_TIMES, DISPLAY_WAIT);
  }
  Serial.println();
}

