#include <Wire.h>

#define I2C_SDA 5
#define I2C_SCL 4
#define INA226_ADD (0x40)              // アドレス(0b1000000x)

// I2c通信送信
static void writeRegister(byte reg, word value) {
  Wire.beginTransmission(INA226_ADD);
  Wire.write(reg);
  Wire.write((value >> 8) & 0xFF);
  Wire.write(value & 0xFF);
  Wire.endTransmission();
}

// I2c通信受信
static word readRegister(byte reg) {
word res = 0x0000;
  Wire.beginTransmission(INA226_ADD);
  Wire.write(reg);
  if (Wire.endTransmission() == 0) {
    if (Wire.requestFrom(INA226_ADD, 2) >= 2) {
      res  = Wire.read() * 256;
      res += Wire.read();
    }
  }
  return res;
}

void setup() {
  Wire.begin(I2C_SDA,I2C_SCL);
  Serial.begin(115200);
  // INA226初期化
  writeRegister(0x00, 0x4127);
  writeRegister(0x05, 0x0A00);
}

void loop() {
  // 電圧、電流、電力読込み
  long voltage  = (long)((short)readRegister(0x02)) * 1250L;    // LSB=1.25mV
  short current = (short)readRegister(0x04);
  long power    = (long)readRegister(0x03) * 25000L;            // LSB=25mW
  // シリアル出力
  char buff[64];
  snprintf(buff,63,"V:%5ldmV, I:%5dmA, P:%5ldmW"
           ,(voltage + (1000 / 2)) / 1000, current, (power + (1000 / 2)) / 1000);
  Serial.println(buff);
  // ウェイト
  delay(1000);
}

