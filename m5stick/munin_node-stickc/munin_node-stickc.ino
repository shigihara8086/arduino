/*
 Munin-node
*/

#include <M5StickC.h>
#include <WiFi.h>
#include <WiFiClient.h>
#include <ArduinoOTA.h>

#define NODE_STICKC                                 // COM8
// #define NODE_ATOM                                   // COM7
// #define NODE_ATOM_LITE                              // COM18

#include "munin_node.h"

WiFiServer server(4949);

#if defined(ENABLE_BLANK)
char *node_config_blank =
  "graph_title _\n"
  "graph_args --base 1000 -r -l -1 -u 1\n"
  "graph_vlabel _\n"
  "graph_scale no\n"
  "graph_category measure_blank\n"
  "graph_info _\n"
  "blank.graph no\n";
#endif

char *node_config_boot =
  "graph_title 死活(%s)\n"
  "graph_args --base 1000 -r -l 0 --alt-autoscale\n"
  "graph_vlabel 回\n"
  "graph_scale no\n"
  "graph_order boot inter\n"
  "graph_category measure_alive\n"
  "graph_info 死活状態の監視用。Muninがデータ取得を行うとカウントアップ。100でリセットする。\n"
  "boot.graph no\n"
  "inter.label 死活\n"
  "inter.info 死活\n"
  "inter.draw LINE1\n"
  "inter.cdef boot,UN,PREV,boot,IF\n"
  "inter.update no\n";

char *node_config_coretemp =
  "graph_title コア温度(%s)\n"
  "graph_args --base 1000\n"
  "graph_vlabel ℃\n"
  "graph_scale no\n"
  "graph_category measure_coretemp\n"
  "graph_order temp inter trend\n"
  "graph_info ESP32のコア温度\n"
  "temp.label コア温度\n"
  "temp.info ESP32のコア温度\n"
  "inter.cdef temp,UN,PREV,temp,IF\n"
  "inter.graph no\n"
  "inter.update no\n"
  "trend.label 移動平均\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info 温度の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

char *node_config_battery =
  "graph_title 電池電圧(%s)\n"
  "graph_args --base 1000 -l 4.0 -u 4.0 --alt-y-grid --alt-autoscale\n"
  "graph_vlabel V\n"
  "graph_scale no\n"
  "graph_order battery inter\n"
  "graph_category measure_battery\n"
  "graph_info 内蔵バッテリーの電圧。\n"
  "batt.graph no\n"
  "inter.label 電圧\n"
  "inter.info 内蔵バッテリーの電圧\n"
  "inter.draw LINE1\n"
  "inter.warning 3.0:\n"
  "inter.cdef batt,UN,PREV,batt,IF\n"
  "inter.update no\n";

RTC_DATA_ATTR int measure_count = 0;
boolean cmd_done;
int loop_count = 0;
long idle_count = 0;
boolean led_state = false;

double meas_batt  = 0.0;

bool do_list(WiFiClient &client) {
#if defined(ENABLE_BLANK)
  client.print("blank boot");
#else
  client.print("boot");
#endif
  client.print(" coretemp rssi channel battery");
#if defined(ENABLE_PIR)
  client.print(" pir");
#endif
#if defined(ENABLE_SHT35)
  client.print(" temp humidity di wbgt dewpoint");
#endif
  client.printf("\n");
  return true;
}

bool do_version(WiFiClient &client) {
  client.print("munin node at arduino version: 0.1\n");
  return true;
}

#if defined(ENABLE_BLANK)
void do_config_blank(WiFiClient &client) {
  client.print(node_config_blank);
  Serial.print(node_config_blank);
}
#endif

void do_config_boot(WiFiClient &client) {
  client.printf(node_config_boot,node_name);
  Serial.printf(node_config_boot,node_name);
}

void do_config_coretemp(WiFiClient &client) {
  client.printf(node_config_coretemp,node_name);
  Serial.printf(node_config_coretemp,node_name);
}

void do_config_battery(WiFiClient &client) {
  client.printf(node_config_battery,node_name);
  Serial.printf(node_config_battery,node_name);
}

bool do_config(WiFiClient &client, String arg) {
#if defined(ENABLE_BLANK)
  if     (arg.equals(String("blank")))    { do_config_blank(client); }
  else if(arg.equals(String("boot")))     { do_config_boot(client); }
#else
  if     (arg.equals(String("boot")))     { do_config_boot(client); }
#endif
  else if(arg.equals(String("coretemp"))) { do_config_coretemp(client); }
  else if(arg.equals(String("rssi")))     { do_config_rssi(client); }
  else if(arg.equals(String("channel")))  { do_config_channel(client); }
  else if(arg.equals(String("battery")))  { do_config_battery(client); }
#if defined(ENABLE_PIR)
  else if(arg.equals(String("pir")))      { do_config_pir(client); }
#endif
#if defined(ENABLE_SHT35)
  else if(arg.equals(String("temp")))     { do_config_sht35_temp(client); }
  else if(arg.equals(String("humidity"))) { do_config_sht35_humi(client); }
  else if(arg.equals(String("di")))       { do_config_sht35_di(client); }
  else if(arg.equals(String("wbgt")))     { do_config_sht35_wbgt(client); }
  else if(arg.equals(String("dewpoint"))) { do_config_sht35_dewp(client); }
#endif
  else {
    client.print("# Unknown service\n");
  }
  client.printf(".\n");
  return true;
}

void do_fetch_blank(WiFiClient &client) {
  client.print("blank.value NaN\n");
  Serial.print("blank: NaN\n");
}

void do_fetch_boot(WiFiClient &client) {
  client.printf("boot.value %d\n",measure_count);
  Serial.printf("boot: %d\n", measure_count);
}

void do_fetch_coretemp(WiFiClient &client) {
float temp = temperatureRead();
  client.printf("temp.value %f\n",temp);
  Serial.printf("coretemp: %f℃\n", temp);
}

void do_fetch_battery(WiFiClient &client) {
  meas_batt = M5.Axp.GetVbatData() * 1.1 / 1000;
  client.printf("batt.value %f\n",meas_batt);
  Serial.printf("batt: %f\n", meas_batt);
}

bool do_fetch(WiFiClient &client, String arg) {
#if defined(ENABLE_BLANK)
  if     (arg.equals(String("blank")))    { do_fetch_blank(client); }
  else if(arg.equals(String("boot")))     { do_fetch_boot(client); }
#else
  if     (arg.equals(String("boot")))     { do_fetch_boot(client); }
#endif
  else if(arg.equals(String("coretemp"))) { do_fetch_coretemp(client); }
  else if(arg.equals(String("rssi")))     { do_fetch_rssi(client); }
  else if(arg.equals(String("channel")))  { do_fetch_channel(client); }
  else if(arg.equals(String("battery")))  { do_fetch_battery(client); }
#if defined(ENABLE_PIR)
  else if(arg.equals(String("pir")))      { do_fetch_pir(client); }
#endif
#if defined(ENABLE_SHT35)
  else if(arg.equals(String("temp")))     { do_fetch_sht35_temp(client); }
  else if(arg.equals(String("humidity"))) { do_fetch_sht35_humi(client); }
  else if(arg.equals(String("di")))       { do_fetch_sht35_di(client); }
  else if(arg.equals(String("wbgt")))     { do_fetch_sht35_wbgt(client); }
  else if(arg.equals(String("dewpoint"))) { do_fetch_sht35_dewp(client); }
#endif
  else {
    client.print("# Unknown service\n");
  }
  client.printf(".\n");
  return true;
}

int do_client(WiFiClient &client) {
char buffer[128];
String cmd, arg;
char c;
byte pos = 0;
  while (client.connected()) {
    if (client.available()) {
      c = client.read();
      if (c != '\n') {
        if (c == '\r') c = '\0';
        buffer[pos] = c;
        pos += 1;
      } else {
        // reset flags on new command
        cmd_done = false;
        buffer[pos] = '\0';
        pos = 0;
        String cmd(strtok(buffer, " "));
        String arg(strtok(NULL, " "));
        Serial.print("Command: ");
        Serial.print(cmd);
        Serial.print(" ");
        Serial.print(arg);
        Serial.print("\n");
        if (cmd.equals(String("quit"))) {
          break;
        }
        if (cmd.equals(String("list")))    cmd_done = do_list(client);
        if (cmd.equals(String("version"))) cmd_done = do_version(client);
        if (cmd.equals(String("config")))  cmd_done = do_config(client, arg);
        if (cmd.equals(String("fetch")))   cmd_done = do_fetch(client, arg);
        if (!cmd_done) {
          client.print("# Unknown command. Try list, config, fetch, version or quit\n");
        }
      }
    }
  }
}

//
// setup hardware
//

void m5_lcd_on() {
  Wire1.beginTransmission(0x34);
  Wire1.write(0x12);
  Wire1.write(0x4d); // Enable LDO2, aka OLED_VDD
  Wire1.endTransmission();
}

void m5_lcd_off() {
  Wire1.beginTransmission(0x34);
  Wire1.write(0x12);
  Wire1.write(0b01001011);  // LDO2, aka OLED_VDD, off
  Wire1.endTransmission();  
}

void m5_init() {
  M5.begin();
  m5_lcd_off();
/*
  m5_lcd_on();
  M5.Axp.ScreenBreath(8);
  M5.Lcd.begin();
  M5.Lcd.setRotation(1);
  M5.Lcd.fillScreen(BLACK);
  M5.Lcd.setTextColor(WHITE, BLACK);
  M5.Lcd.setTextSize(2);
*/
}

void setup() {
  Serial.begin(115200);
  m5_init();
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN,LOW);
  wifi_init();
  digitalWrite(LED_BUILTIN,HIGH);
  ntp_init();
#if defined(ENABLE_PIR)
  pir_init();
#endif
#if defined(ENABLE_SHT35)
  sht35_init();
#endif
  server.begin();
  Serial.println("TCP server started");
}

void loop() {
  WiFiClient client = server.available();
  if(client) {
    Serial.println("New connection");
    client.printf("# munin node at %s\n", node_name);
    do_client(client);
    client.stop();
    Serial.println("Close connection");
    measure_count ++;
    if(measure_count > 100) measure_count = 0;
    idle_count = 0;
  }
#if defined(ENABLE_PIR)
  pir_loop();
#endif
#if defined(ENABLE_SHT35)
  sht35_loop();
#endif
  loop_count ++;
  if(loop_count > 999) {
    if(led_state == false) {
      digitalWrite(LED_BUILTIN,LOW);
      led_state = true;
    }
  } else {
    if(led_state == true) {
      digitalWrite(LED_BUILTIN,HIGH);
      led_state = false;
    }
  }
  if(loop_count > 1000) loop_count = 0;
  // 15分アイドル状態ならリブート
  idle_count ++;
  if(idle_count > (15 * 60 * 1000L)) {
    ESP.restart();
  }
  ArduinoOTA.handle();
  delay(1);
}
