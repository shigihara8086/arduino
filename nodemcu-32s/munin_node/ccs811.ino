#if defined(ENABLE_CCS811)

char *node_config_ccs811_co2 =
  "graph_title CO2濃度(%s)\n"
  "graph_args --base 1000 -r --alt-autoscale\n"
  "graph_vlabel ppm\n"
  "graph_scale no\n"
  "graph_category measure_co2\n"
  "graph_order co2 inter trend\n"
  "graph_info CCS811センサーで測定したCO2濃度\n"
  "co2.label 濃度　　\n"
  "co2.info CCS811センサーのCO2濃度\n"
  "co2.warning  :1500\n"
  "co2.critical :2000\n"
  "inter.cdef co2,UN,PREV,co2,IF\n"
  "inter.graph no\n"
  "inter.update no\n"
  "trend.label 移動平均\n"
  "trend.line 2000:880000\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info CO2濃度の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

char *node_config_ccs811_tvoc =
  "graph_title TVOC(%s)\n"
  "graph_args --base 1000 --alt-autoscale\n"
  "graph_vlabel ppb\n"
  "graph_scale no\n"
  "graph_order tvoc inter trend\n"
  "graph_category measure_tvoc\n"
  "graph_info CCS811センサーで測定したTVOC\n"
  "tvoc.label ＴＶＯＣ\n"
  "tvoc.info CCS811センサーのTVOC\n"
  "inter.cdef tvoc,UN,PREV,tvoc,IF\n"
  "inter.graph no\n"
  "inter.update no\n"
  "trend.label 移動平均\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info TVOCの移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

float meas_co2;
float meas_tvoc;

#include <CCS811.h>

/*
* IIC address default 0x5A, the address becomes 0x5B if the ADDR_SEL is soldered.
*/

//CCS811 sensor(&Wire, /*IIC_ADDRESS=*/0x5A);
CCS811 sensor;

int ccs811_init(void) {
   /*Wait for the chip to be initialized completely, and then exit*/
   while(sensor.begin() != 0){
       Serial.println("failed to init chip, please check if the chip connection is fine");
       delay(1000);
   }
   /**
    * @brief Set measurement cycle
    * @param cycle:in typedef enum{
    *                  eClosed,      //Idle (Measurements are disabled in this mode)
    *                  eCycle_1s,    //Constant power mode, IAQ measurement every second
    *                  eCycle_10s,   //Pulse heating mode IAQ measurement every 10 seconds
    *                  eCycle_60s,   //Low power pulse heating mode IAQ measurement every 60 seconds
    *                  eCycle_250ms  //Constant power mode, sensor measurement every 250ms
    *                  }eCycle_t;
    */
   sensor.setMeasCycle(sensor.eCycle_250ms);
}

void ccs811_getValues() {
 delay(1000);
   if(sensor.checkDataReady() == true) {
      meas_co2 = sensor.getCO2PPM();
      meas_tvoc = sensor.getTVOCPPB();
       Serial.print("CO2: ");
       Serial.print(meas_co2);
       Serial.print("ppm, TVOC: ");
       Serial.print(meas_tvoc);
       Serial.println("ppb");
       
   } else {
       Serial.println("Data is not ready!");
   }
   /*!
    * @brief Set baseline
    * @param get from getBaseline.ino
    */
   sensor.writeBaseLine(0x847B);
   //delay cannot be less than measurement cycle
   //delay(1000);
}

void do_config_ccs811_co2(WiFiClient &client) {
  client.printf(node_config_ccs811_co2,node_name);
  Serial.printf(node_config_ccs811_co2,node_name);
}

void do_config_ccs811_tvoc(WiFiClient &client) {
  client.printf(node_config_ccs811_tvoc,node_name);
  Serial.printf(node_config_ccs811_tvoc,node_name);
}

void do_fetch_ccs811_co2(WiFiClient &client) {
  client.printf("co2.value %f\n",meas_co2);
  Serial.printf("meas_co2: %5.1f\n", meas_co2);
}

void do_fetch_ccs811_tvoc(WiFiClient &client) {
  client.printf("tvoc.value %f\n",meas_tvoc);
  Serial.printf("meas_tvoc: %5.1f\n", meas_tvoc);
}

//
// loop
//

void ccs811_loop() {
  ccs811_getValues();
}

#endif
