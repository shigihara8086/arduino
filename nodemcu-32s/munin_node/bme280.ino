/*
 BME280
*/

#if defined(ENABLE_BME280)

char *node_config_bme280_temp =
  "graph_title 温度(%s)\n"
  "graph_args --base 1000 --alt-autoscale\n"
  "graph_vlabel ℃\n"
  "graph_scale no\n"
  "graph_category measure_temperature\n"
  "graph_order temp inter trend\n"
  "graph_info BME280センサーで測定した温度\n"
  "temp.label 温度　　\n"
  "temp.info BME280センサーの温度\n"
  "inter.cdef temp,UN,PREV,temp,IF\n"
  "inter.graph no\n"
  "inter.update no\n"
  "trend.label 移動平均\n"
  "trend.line 25.0:880000\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info 温度の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

char *node_config_bme280_humi =
  "graph_title 湿度(%s)\n"
  "graph_args --base 1000 --alt-autoscale\n"
  "graph_vlabel %%\n"
  "graph_scale no\n"
  "graph_order humi inter trend\n"
  "graph_category measure_humidity\n"
  "graph_info BME280センサーで測定した湿度\n"
  "humi.label 湿度　　\n"
  "humi.info BME280センサーの湿度\n"
  "inter.cdef humi,UN,PREV,humi,IF\n"
  "inter.graph no\n"
  "inter.update no\n"
  "trend.label 移動平均\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info 湿度の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

char *node_config_bme280_di =
  "graph_title 不快指数(%s)\n"
  "graph_args --base 1000 -l 55.0 -u 85.0 --alt-y-grid --alt-autoscale\n"
  "graph_vlabel DI\n"
  "graph_scale no\n"
  "graph_order di inter trend\n"
  "graph_category measure_di\n"
  "graph_info BME280センサーの温度・湿度から計算した不快指数\n"
  "di.label 不快指数\n"
  "di.info BME280センサーの温度・湿度から計算\n"
  "di.warning 60:80\n"
  "di.critical 55:85\n"
  "inter.cdef di,UN,PREV,di,IF\n"
  "inter.graph no\n"
  "inter.update no\n"
  "trend.warning 55:85\n"
  "trend.critical 55:85\n"
  "trend.label 移動平均\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info 不快指数の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

char *node_config_bme280_wbgt =
  "graph_title 暑さ指数(%s)\n"
  "graph_args --base 1000 --alt-y-grid --alt-autoscale\n"
  "graph_vlabel ℃\n"
  "graph_scale no\n"
  "graph_order wbgt inter trend\n"
  "graph_category measure_wbgt\n"
  "graph_info BME280センサーの温度・湿度から計算した暑さ指数\n"
  "wbgt.label 暑さ指数\n"
  "wbgt.info BME280センサーの温度・湿度から計算\n"
  "wbgt.warning  :28.0\n"
  "wbgt.critical :31.0\n"
  "inter.cdef wbgt,UN,PREV,wbgt,IF\n"
  "inter.graph no\n"
  "inter.update no\n"
  "trend.label 移動平均\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info 暑さ指数の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

char *node_config_bme280_dewp =
  "graph_title 露点温度(%s)\n"
  "graph_args --base 1000 --alt-y-grid --alt-autoscale\n"
  "graph_vlabel ℃\n"
  "graph_scale no\n"
  "graph_order dewp inter trend\n"
  "graph_category measure_dewpoint\n"
  "graph_info BME280センサーの温度・湿度から計算した露点温度\n"
  "dewp.label 露点温度\n"
  "dewp.info BME280センサーの温度・湿度から計算\n"
  "inter.cdef dewp,UN,PREV,dewp,IF\n"
  "inter.graph no\n"
  "inter.update no\n"
  "trend.label 移動平均\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info 露点温度の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

char *node_config_bme280_press =
  "graph_title 気圧(%s)\n"
  "graph_args --base 1000 -l 1012.25 -u 1014.25 --alt-y-grid --alt-autoscale\n"
  "graph_vlabel local hPa\n"
  "graph_scale no\n"
  "graph_order press inter trend\n"
  "graph_category measure_pressure\n"
  "graph_info BME280センサーで測定した気圧。海面標準気圧に補正してある。\n"
  "press.label 気圧　　\n"
  "press.info BME280センサーの気圧\n"
  "inter.cdef press,UN,PREV,press,IF\n"
  "inter.graph no\n"
  "inter.update no\n"
  "trend.label 移動平均\n"
  "trend.line 1013.25:880000\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info 気圧の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

#include "Adafruit_BME280.h"
#include "Adafruit_Sensor.h"

#define BME280_ADD          (0x76)
#define SEALEVEL_ADJUSTMENT ((260.0 / 100.0) * 12.0)

float meas_temp =  0.0;
float meas_humi =  0.0;
float meas_di   =  0.0;
float meas_wbgt =  0.0;
float meas_dewp =  0.0;
float meas_press = 0.0;

Adafruit_BME280 bme(I2C_SDA, I2C_SCL);

int bme280_init() {
  bool status;
  // 内蔵PULLUP ON
  pinMode(I2C_SDA, INPUT_PULLUP);
  pinMode(I2C_SCL, INPUT_PULLUP);
  //
  status = bme.begin(BME280_ADD);
  if (!status) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    return -1;
  }
  delay(1000);
  bme280_getValues();
  return 0;
}

void bme280_getValues() {
float sr = 0.0;
float ws = 0.0;
  while(true) {
    meas_temp = bme.readTemperature();
    meas_press = bme.readPressure() / 100.0F + SEALEVEL_ADJUSTMENT;
    meas_humi = bme.readHumidity();
    if(meas_temp > -50 && meas_temp < 100 && meas_humi >= 0 && meas_humi <= 100) {
      meas_di   = 0.81 * meas_temp + (0.01 * meas_humi * (0.99 * meas_temp - 14.3)) + 46.3;
      meas_wbgt = 0.735 * meas_temp + 0.0374 * meas_humi + 0.00292 * meas_temp * meas_humi + 7.619 * sr - 4.557 * sr * sr - 0.0572 * ws - 4.064;
      meas_dewp = computeByRH(meas_temp, meas_humi);
      //
      Serial.printf("気温     : %6.1f ºC\n",  meas_temp);
      Serial.printf("湿度     : %6.1f %%\n",  meas_humi);
      Serial.printf("不快指数 : %6.1f\n",     meas_di);
      Serial.printf("暑さ指数 : %6.1f ºC\n",  meas_wbgt);
      Serial.printf("露点温度 : %6.1f ºC\n",  meas_dewp);
      Serial.printf("気圧     : %6.1f hPa\n", meas_press);
      break;
    }
  }
}

//
// config
//

void do_config_bme280_temp(WiFiClient &client) {
  bme280_getValues();
  client.printf(node_config_bme280_temp,node_name);
  Serial.printf(node_config_bme280_temp,node_name);
}

void do_config_bme280_humi(WiFiClient &client) {
  client.printf(node_config_bme280_humi,node_name);
  Serial.printf(node_config_bme280_humi,node_name);
}

void do_config_bme280_di(WiFiClient &client) {
  client.printf(node_config_bme280_di,node_name);
  Serial.printf(node_config_bme280_di,node_name);
}

void do_config_bme280_wbgt(WiFiClient &client) {
  client.printf(node_config_bme280_wbgt,node_name);
  Serial.printf(node_config_bme280_wbgt,node_name);
}

void do_config_bme280_dewp(WiFiClient &client) {
  client.printf(node_config_bme280_dewp,node_name);
  Serial.printf(node_config_bme280_dewp,node_name);
}

void do_config_bme280_press(WiFiClient &client) {
  client.printf(node_config_bme280_press,node_name);
  Serial.printf(node_config_bme280_press,node_name);
}

//
// fetch
//

void do_fetch_bme280_temp(WiFiClient &client) {
  client.printf("temp.value %f\n",meas_temp);
  Serial.printf("meas_temp: %5.1f\n", meas_temp);
}

void do_fetch_bme280_humi(WiFiClient &client) {
  client.printf("humi.value %f\n",meas_humi);
  Serial.printf("meas_humi: %5.1f\n", meas_humi);
}

void do_fetch_bme280_di(WiFiClient &client) {
  client.printf("di.value %f\n",meas_di);
  Serial.printf("meas_di: %5.1f\n", meas_di);
}

void do_fetch_bme280_wbgt(WiFiClient &client) {
  client.printf("wbgt.value %f\n",meas_wbgt);
  Serial.printf("meas_wbgt: %5.1f\n", meas_wbgt);
}

void do_fetch_bme280_dewp(WiFiClient &client) {
  client.printf("dewp.value %f\n",meas_dewp);
  Serial.printf("meas_dewp: %5.1f\n", meas_dewp);
}

void do_fetch_bme280_press(WiFiClient &client) {
  client.printf("press.value %f\n",meas_press);
  Serial.printf("meas_press: %5.1f\n", meas_press);
}

//
// loop
//

void bme280_loop() {
}

#endif
