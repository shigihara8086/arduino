/*
BME280
*/

#if defined(ENABLE_BME280)

#include "Adafruit_BME280.h"
#include "Adafruit_Sensor.h"

#define BME280_ADD (0x76)
#define SEALEVEL_ADJUSTMENT ((260.0 / 100.0) * 12.0)

Adafruit_BME280 bme(I2C_SDA, I2C_SCL);

int bme280_init() {
  bool status;
  // 内蔵PULLUP ON
  pinMode(I2C_SDA, INPUT_PULLUP);
  pinMode(I2C_SCL, INPUT_PULLUP);
  //
  status = bme.begin(BME280_ADD);
  if (!status) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    return -1;
  }
  delay(100);
  return 0;
}

void bme280_getValues() {
  meas_temp = bme.readTemperature();
  meas_press = bme.readPressure() / 100.0F + SEALEVEL_ADJUSTMENT;
  meas_humi = bme.readHumidity();
  meas_dewp = computeByRH(meas_temp, meas_humi);

  Serial.printf("気温     = % 6.1f ºC\n", meas_temp);
  Serial.printf("湿度     = % 6.1f %%\n", meas_humi);
  Serial.printf("露点温度 = % 6.1f ºC\n",  meas_dewp);
  Serial.printf("気圧     = % 6.1f hPa\n", meas_press);
}
#endif
