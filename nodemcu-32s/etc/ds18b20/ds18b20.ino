/*********
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com  
*********/

#include <OneWire.h>
#include <DallasTemperature.h>

// GPIO where the DS18B20 is connected to
const int oneWireBus = 4;     

// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(oneWireBus);

// Pass our oneWire reference to Dallas Temperature sensor 
DallasTemperature sensors(&oneWire);
DeviceAddress dev0, dev1;

// function to print a device address
void printAddress(DeviceAddress deviceAddress)
{
  for (uint8_t i = 0; i < 8; i++)
  {
    // zero pad the address if necessary
    if (deviceAddress[i] < 16) Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
  }
  Serial.println();
}

void setup() {
  // Start the Serial Monitor
  Serial.begin(115200);
  // Start the DS18B20 sensor
  sensors.begin();
  delay(500);
  if (!sensors.getAddress(dev0, 0)) Serial.println("Unable to find address for Device 0");
  if (!sensors.getAddress(dev1, 1)) Serial.println("Unable to find address for Device 1");
  printAddress(dev0);
  printAddress(dev1);
}

void loop() {
  sensors.requestTemperatures(); 
  float temperatureC1 = sensors.getTempC(dev0);
  float temperatureC2 = sensors.getTempC(dev1);
  Serial.print("Senser1:"); Serial.print(temperatureC1); Serial.print("ºC ");
  Serial.print("Senser2:"); Serial.print(temperatureC2); Serial.print("ºC ");
  Serial.println();
  delay(5000);
}
