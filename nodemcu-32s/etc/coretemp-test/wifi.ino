/*
Wi-Fi
*/

#include <WiFi.h>
#include <WiFiMulti.h>

WiFiMulti wifiMulti;                            // Multi AP

//
// setup Wi-Fi
//

void wifimulti_init() {
int status;
int i;
  i = 0;
  while (wifi_aps[i].ssid) {
    wifiMulti.addAP(wifi_aps[i].ssid, wifi_aps[i].passwd);
    i ++;
  }
  Serial.print("WiFi connect start\n");
  WiFi.mode(WIFI_STA);
  i = 0;
  while(true) {
    status = wifiMulti.run();
    Serial.printf("status : %d\n", status);
    if(status == WL_CONNECTED) break;
    if (i > 10) {
      Serial.print("Can not connect Wi-Fi. restart\n");
      ESP.restart();
    }
    i ++;
    delay(1000);
  }
  Serial.printf("Connect Wi-Fi ssid:%s rssi:%d\n", WiFi.SSID().c_str(), WiFi.RSSI());
}
