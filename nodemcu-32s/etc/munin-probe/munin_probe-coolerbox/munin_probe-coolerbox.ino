/*
  munin_probe
*/

#include <Wire.h>
#include <WiFi.h>
#include <HTTPClient.h>

#define uS_TO_S_FACTOR      1000000UL           // Conversion factor for micro seconds to seconds

//const char *config = "/sd21.txt";
const char *config = "/home.txt";
const char *http_server = "https://sgmail.jp/cgi/esp32-node.cgi";
const int wdtTimeout = (5 * 60 * 1000);
esp_sleep_wakeup_cause_t wakeup_reason;         // Wakeup reason
RTC_DATA_ATTR int bootCount = 0;                // sleep times counter
String ssid, passwd, myname;
hw_timer_t *timer = NULL;

// measure data
int measure_count = 0;
float core_temp;
float dht11_temp;
float dht11_humidity;

void IRAM_ATTR resetModule() {
  ets_printf("reboot by watchdog\n");
  esp_restart();
}

void wdt_init() {
  // watchdog
  timer = timerBegin(0, 80, true);                  // timer 0, div 80
  timerAttachInterrupt(timer, &resetModule, true);  // attach callback
  timerAlarmWrite(timer, wdtTimeout * 1000, false); // set time in us
  timerAlarmEnable(timer);                          // enable interrupt
  timerWrite(timer, 0);                             // reset timer (feed watchdog)
}

void get_wakeup_reason() {
  wakeup_reason = esp_sleep_get_wakeup_cause();
  switch (wakeup_reason) {
    case 1  : Serial.print("Wakeup caused by external signal using RTC_IO\n"); break;
    case 2  : Serial.print("Wakeup caused by external signal using RTC_CNTL\n"); break;
    case 3  : Serial.print("Wakeup caused by timer\n"); break;
    case 4  : Serial.print("Wakeup caused by touchpad\n"); break;
    case 5  : Serial.print("Wakeup caused by ULP program\n"); break;
    default : {
        Serial.print("Wakeup was not caused by deep sleep\n");
        break;
      }
  }
}

void measure() {
  core_temp = temperatureRead();
  dht11_measure();
  measure_count ++;
}

void http_send() {
  HTTPClient http;
  int httpCode;
  char buffer[256];
  Serial.print("http send start\n");
  sprintf(buffer,
          "%s?node=%s&"
          "count=%d&"
          "core_temp=%f&"
          "dht11_temp=%f&"
          "dht11_humidity=%f",
          http_server,
          myname.c_str(),
          bootCount,
          core_temp,
          dht11_temp,
          dht11_humidity
         );
  Serial.printf("%s\n", buffer);
  for (int i = 0; i < 5; i ++) {
    http.begin(buffer);
    http.setTimeout(10 * 1000);
    httpCode = http.GET();
    if (httpCode > 0) {
      Serial.printf("[HTTP] GET... code: %d\n", httpCode);
      if (httpCode == HTTP_CODE_OK) {
        String payload = http.getString();
        Serial.println("Payload : " + payload);
        http.end();
        return;
      }
    } else {
      Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
      if (httpCode == HTTPC_ERROR_READ_TIMEOUT) {
        http.end();
        return;
      }
    }
    http.end();
    Serial.print("http send retry after 10 sec.\n");
    delay(10 * 1000);
  }
}

void setup() {
  Serial.begin(115200);
  get_wakeup_reason();
  bootCount ++;
  wifimulti_init();
  dht11_init();
}

void loop() {
  measure();
  http_send();
  Serial.println("sleep to next time");
  if(bootCount < 2) {
    esp_sleep_enable_timer_wakeup((unsigned long)(2 * 60) * uS_TO_S_FACTOR);
    esp_deep_sleep_start();
    delay(1);
  } else {
    WiFi.mode(WIFI_OFF);
    delay(2 * 60 * 1000);
    ESP.restart();
  }
}
