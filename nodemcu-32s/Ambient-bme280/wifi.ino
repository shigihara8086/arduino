/*
 Wi-Fi
*/

#include <WiFi.h>
#include <WiFiMulti.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>

//
// wifi init
//

struct WifiMulti_aps {                          // ssid,password pare
  char *ssid;
  char *passwd;
};

struct WifiMulti_aps wifi_aps[] = {             // ssid,password table
  { "aterm-20cd3f-g",  "65676ae9e9f9b" },
  { "106F3FDBCFA9_EXT","amhh8khbexssm" },
  { NULL, NULL }
};

WiFiMulti wifiMulti;                            // Multi AP

//
// OTA
//

void ota_init() {
  Serial.println("OTA initialize start");
  ArduinoOTA.setHostname(HOSTNAME);
  // ArduinoOTA.setPassword("admin");
  ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    })
    .onEnd([]() {
      Serial.println("\nEnd");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });
  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void wifi_init() {
int status;
int i;
  i = 0;
  while (wifi_aps[i].ssid) {
    wifiMulti.addAP(wifi_aps[i].ssid, wifi_aps[i].passwd);
    i ++;
  }
  Serial.print("WiFi connect start\n");
  WiFi.mode(WIFI_STA);
  i = 0;
  while(true) {
    status = wifiMulti.run();
    switch(status) {
      case WL_CONNECTED       : Serial.print("status : WL_CONNECTED\n"); break;
      case WL_NO_SHIELD       : Serial.print("status : WL_NO_SHIELD\n"); break;
      case WL_IDLE_STATUS     : Serial.print("status : WL_IDLE_STATUS\n"); break;
      case WL_NO_SSID_AVAIL   : Serial.print("status : WL_NO_SSID_AVAIL\n"); break;
      case WL_SCAN_COMPLETED  : Serial.print("status : WL_SCAN_COMPLETED\n"); break;
      case WL_CONNECT_FAILED  : Serial.print("status : WL_CONNECT_FAILED\n"); break;
      case WL_CONNECTION_LOST : Serial.print("status : WL_CONNECTION_LOST\n"); break;
      case WL_DISCONNECTED    : Serial.print("status : WL_DISCONNECTED\n"); break;
      default : {
        Serial.printf("status : Unknown : %d\n", status);
      }
    }
    if(status == WL_CONNECTED && WiFi.RSSI() != 0) break;
    i ++;
    if (i > 5) {
      Serial.print("Can not connect Wi-Fi. restart\n");
      ESP.restart();
    }
    while(WiFi.status() == WL_CONNECTED) {
      WiFi.disconnect();
      delay(100);
    }
    delay(1000);
  }
  Serial.printf("Connect Wi-Fi ssid:%s rssi:%d\n", WiFi.SSID().c_str(), WiFi.RSSI());
  // OTA
  ota_init();
  delay(1000);
}

void wifi_term() {
  while(WiFi.status() == WL_CONNECTED) {
    WiFi.disconnect();
    delay(100);
  }
}
