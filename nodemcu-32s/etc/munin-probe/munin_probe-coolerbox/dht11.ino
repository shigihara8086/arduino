#include "DHTesp.h"

DHTesp dht;

void dht11_init() {
  dht.setup(17,DHTesp::DHT11); // Connect DHT sensor to GPIO 17
}

void dht11_measure() {
  delay(dht.getMinimumSamplingPeriod());
  dht11_humidity = dht.getHumidity();
  dht11_temp = dht.getTemperature();
}
