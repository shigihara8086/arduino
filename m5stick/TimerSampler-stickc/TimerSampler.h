/*
 board base definition 
*/

//
// SSID and Password structure
//

struct WifiMulti_aps {                          // ssid,password pare
  char *ssid;
  char *passwd;
};

//
// difinitions for each boards
//

#if defined(NODE_STICKC)
#define LTE_POWERLED        (10)                // GPIO23 lte_power on digital port
#define TIME_TO_ON          (1)                 // MT100 startup wait time
#define WAKE_OFFSET         (29)                // wakeup time offset use other overhead
#define TIME_TO_SLEEP       (5 * 60)            // Time ESP32 will go to sleep (in seconds)
#define NODE_NAME           "esp32-stickc"      // node name
struct WifiMulti_aps wifi_aps[] = {             // ssid,password table
  { "aterm-20cd3f-g","65676ae9e9f9b" },
  { NULL, NULL }
};
#endif

#if defined(PICO_TEST)
#define LTE_POWERLED        (10)                // GPIO23 lte_power on digital port
#define TIME_TO_ON          (1)                 // MT100 startup wait time
#define WAKE_OFFSET         (29)                // wakeup time offset use other overhead
#define TIME_TO_SLEEP       (5 * 60)            // Time ESP32 will go to sleep (in seconds)
#define NODE_NAME           "esp32-stickc"      // node name
struct WifiMulti_aps wifi_aps[] = {             // ssid,password table
  { "aterm-20cd3f-g","65676ae9e9f9b" },
  { NULL, NULL }
};
#endif
