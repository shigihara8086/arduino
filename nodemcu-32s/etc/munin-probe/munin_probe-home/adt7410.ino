/*
  ADT7410
*/

#include <Wire.h>

#define I2C_SDA 5                                     // SDA Pin No.5 yellow
#define I2C_SCL 4                                     // SCL Pin No.4 green

#define ADT7410_ADD 0x48
#define DS18X20_OFFSET (0.0)

int adt7410I2CAddress = 0x48;

int adt7410_init() {
  Wire.begin(I2C_SDA, I2C_SCL);
  return 0;
}

unsigned int adt7410_readRegister(void) {
  unsigned int val;
  Wire.requestFrom(ADT7410_ADD, 2);
  val  = Wire.read() << 8;
  val |= Wire.read();
  return val;
}

float adt7410_value() {
  Wire.reset();
  delay(100);
  uint16_t uiVal; //2バイト(16ビット)の領域
  int iVal;
  uiVal = adt7410_readRegister();
  uiVal >>= 3;                          // シフトで13bit化
  if (uiVal & 0x1000) {                 // 13ビットで符号判定
    iVal = uiVal - 0x2000;              // マイナスの時 (10進数で8192)
  } else {
    iVal = uiVal;                       //プラスの時
  }
  return ((float)iVal / 16.0);          // 温度換算(摂氏)
}

