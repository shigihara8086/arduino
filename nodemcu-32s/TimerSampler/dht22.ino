#if defined(ENABLE_DHT22)

#include "DHTesp.h"

DHTesp dht;

void dht22_init() {
  dht.setup(GPIO_PORT,DHTesp::DHT22);
}

void dht22_getValues() {
  while(true) {
    delay(dht.getMinimumSamplingPeriod());
    meas_temp = dht.getTemperature();
    meas_humi = dht.getHumidity();
    if(meas_temp > -50 && meas_temp < 100) {
      meas_dewp = computeByRH(meas_temp, meas_humi);
      Serial.printf("気温     : %5.1fºC\n", meas_temp);
      Serial.printf("湿度     : %5.1f%%\n", meas_humi);
      Serial.printf("露点温度 : %5.1fºC\n", meas_dewp);
      break;
    }
    delay(100);
  }
}

#endif
