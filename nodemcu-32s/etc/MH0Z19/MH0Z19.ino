#include "MHZ19.h"     

#define RX_PIN 17                                          // Rx pin which the MHZ19 Tx pin is attached to
#define TX_PIN 16                                          // Tx pin which the MHZ19 Rx pin is attached to
#define BAUDRATE 9600                                      // Device to MH-Z19 Serial baudrate (should not be changed)
#define LED_BUILTIN 2                                      // Set the GPIO pin where you connected your test LED or comment this line out if your dev board has a built-in LED

//prototype
void co2SensorModule(void);

MHZ19 myMHZ19;                                             // Constructor for library
HardwareSerial mySerial(2);                              // (ESP32 Example) create device to MH-Z19 serial
unsigned long getDataTimer = 0;
int CO2; 
int8_t Temp;

void setup() {
  Serial.begin(115200);
  Serial.println();
  Serial.println("Configuring access point...");
  //CO2 sensor setup
  mySerial.begin(BAUDRATE, SERIAL_8N1, RX_PIN, TX_PIN); // (ESP32 Example) device to MH-Z19 serial start   
  myMHZ19.begin(mySerial);                                // *Serial(Stream) refence must be passed to library begin(). 
  myMHZ19.autoCalibration();                              // Turn auto calibration ON (OFF autoCalibration(false))    
}

void loop() {
  co2SensorModule();
  Serial.println(CO2);
  Serial.println(Temp);
}


void co2SensorModule(void) {
  if (millis() - getDataTimer >= 2000) {

      /* note: getCO2() default is command "CO2 Unlimited". This returns the correct CO2 reading even 
      if below background CO2 levels or above range (useful to validate sensor). You can use the 
      usual documented command with getCO2(false) */

      CO2 = myMHZ19.getCO2();                             // Request CO2 (as ppm)                                      
      Temp = myMHZ19.getTemperature();                     // Request Temperature (as Celsius)                          
      getDataTimer = millis();
  }
}
