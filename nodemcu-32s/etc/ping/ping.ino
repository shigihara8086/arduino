#include "ping/ping.h"
#include "esp_ping.h"

static const uint32_t PING_COUNT = 5;
static uint32_t timeout_count = 0;

static esp_err_t ping_handler(ping_target_id_t id, esp_ping_found * pf)
{
    if (pf->send_count == PING_COUNT) {
        timeout_count = pf->timeout_count;
    }

    return ESP_OK;
}

static void ping_gatway()
{
    tcpip_adapter_ip_info_t ip_info;
    uint32_t count = PING_COUNT;
    uint32_t timeout_msec = 100;
    uint32_t delay_msec = 500;

    if (tcpip_adapter_get_ip_info(TCPIP_ADAPTER_IF_STA, &ip_info) != ESP_OK) {
        timeout_count = PING_COUNT;
        return;
    }

    ping_deinit();
    esp_ping_set_target(PING_TARGET_IP_ADDRESS_COUNT, &count, sizeof(uint32_t));
    esp_ping_set_target(PING_TARGET_RCV_TIMEO, &timeout_msec, sizeof(uint32_t));
    esp_ping_set_target(PING_TARGET_DELAY_TIME, &delay_msec, sizeof(uint32_t));
    esp_ping_set_target(PING_TARGET_IP_ADDRESS, &(ip_info.gw.addr), sizeof(uint32_t));
    esp_ping_set_target(PING_TARGET_RES_FN, &ping_handler, sizeof(ping_handler));
    ping_init();
}
