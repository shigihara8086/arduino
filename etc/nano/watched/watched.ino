void setup() {
  // write all digital I/O LOW
  for(int i=0; i<20; i++){
    pinMode(i, OUTPUT);
  }
}

void loop() {
  // set power-down mode
  SMCR |= (1 << SM1);
  SMCR |= 1;

  // disable ADC
  ADCSRA &= ~(1 << ADEN);

  // set watchdog timer
  asm("wdr");
  WDTCSR |= (1 << WDCE)|(1 << WDE);
  // WDTCSR = (1 << WDIE)|(1 << WDP3)|(1 << WDP0);     // 8 sec
  WDTCSR  = (1 << WDIE) | (1 << WDP2) | (1 << WDP0);    // 0.5 sec

  // disable BOD
  MCUCR |= (1 << BODSE)|(1 << BODS);
  MCUCR = (MCUCR & ~(1 << BODSE))|(1 << BODS);
  asm("sleep");

  // stop watchdog timer
  asm("wdr");
  MCUSR &= ~(1 << WDRF);
  WDTCSR |= (1 << WDCE)|(1 << WDE);
  WDTCSR = 0;

  // enable ADC
  ADCSRA |= (1 << 7);
}

//watchdog interrupt
ISR(WDT_vect){
  digitalWrite(LED_BUILTIN, HIGH);
  delay(300);
  digitalWrite(LED_BUILTIN, LOW);
}

