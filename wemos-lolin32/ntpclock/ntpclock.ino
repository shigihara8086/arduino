/*
  u8g2-Clock.ino
*/

#include <WiFi.h>
#include <Arduino.h>
#include <U8g2lib.h>

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif

#define JST (3600L * 9)
#define TOUCH (T3)

U8G2_SSD1306_128X64_NONAME_F_HW_I2C u8g2(U8G2_R0, /* reset=*/ U8X8_PIN_NONE, /* clock=*/ 4, /* data=*/ 5);   // ESP32 Thing, HW I2C with pin remapping

const char *ssid       = "106F3FDBCFA9";
const char *password   = "amhh8khbexssm";
const char *ntp_servers[] { "sgmail.jp", "sgmail.jp", "sgmail.jp" };

struct tm timeInfo;
char *dayofweek_short[] = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };
volatile boolean touched = false;
hw_timer_t * timer = NULL;
volatile SemaphoreHandle_t timerSemaphore;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;
volatile uint32_t isrCounter = 0;
volatile uint32_t lastIsrAt = 0;

void gotTouch() {
  touched = true;
}

void IRAM_ATTR onTimer() {
  // Increment the counter and set the time of ISR
  portENTER_CRITICAL_ISR(&timerMux);
  isrCounter ++;
  lastIsrAt = millis();
  portEXIT_CRITICAL_ISR(&timerMux);
  // Give a semaphore that we can check in the loop
  xSemaphoreGiveFromISR(timerSemaphore, NULL);
  // It is safe to use digitalRead/Write here if you want to toggle an output
}

void show_logo() {
  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_logisoso26_tf);
  u8g2.drawStr( 0, 43, "NtpClock");
  u8g2.sendBuffer();
}

void show_clock() {
  char strbuf[100];
  getLocalTime(&timeInfo);
  u8g2.clearBuffer();
  sprintf(strbuf, "%04d/%02d/%02d (%s)", timeInfo.tm_year + 1900, timeInfo.tm_mon + 1, timeInfo.tm_mday, dayofweek_short[timeInfo.tm_wday]);
  u8g2.setFont(u8g2_font_unifont_t_latin);
  u8g2.drawStr(0, 16, strbuf);
  u8g2.setFont(u8g2_font_logisoso42_tn);
  sprintf(strbuf, "%02d", timeInfo.tm_hour);
  u8g2.drawStr( 0, 63, strbuf);
  if ((timeInfo.tm_sec % 2) == 0) {
    u8g2.drawStr(54, 63, ":");
  } else {
    u8g2.drawStr(54, 63, " ");
  }
  sprintf(strbuf, "%02d", timeInfo.tm_min);
  u8g2.drawStr(70, 63, strbuf);
  u8g2.sendBuffer();
}

void ntp_init() {
  int count;
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  if (WiFi.begin(ssid, password) != WL_DISCONNECTED) {
    ESP.restart();
  }
  count = 0;
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    count ++;
    if (count > 5) {
      ESP.restart();
    }
  }
  configTime(JST, 0, ntp_servers[0], ntp_servers[1], ntp_servers[2]);
  getLocalTime(&timeInfo);
  if ((timeInfo.tm_year + 1900) < 1971) {
    Serial.print("Can not NTP sync, force restart\n");
    delay(1000);
    ESP.restart();
  }
  Serial.print("WiFi connected\n");
}

void setup(void) {
  Serial.begin(115200);
  u8g2.begin();
  show_logo();
  ntp_init();
  for(int i = 0; i < 30; i ++) {
    show_clock();
    delay(100);
  }
  u8g2.setPowerSave(1);
  timerSemaphore = xSemaphoreCreateBinary();
  touchAttachInterrupt(TOUCH, gotTouch, 30);
}

void loop(void) {
  if (touched) {
    if (timer == NULL) {
      timer = timerBegin(0, 80, true);
      timerAttachInterrupt(timer, &onTimer, true);
      timerAlarmWrite(timer, 4 * 1000000L, true);
      timerAlarmEnable(timer);
    }
    touched = false;
    Serial.print("touched\n");
    u8g2.setPowerSave(0);
  }
  if (xSemaphoreTake(timerSemaphore, 0) == pdTRUE) {
    uint32_t isrCount = 0, isrTime = 0;
    // Read the interrupt count and time
    portENTER_CRITICAL(&timerMux);
    isrCount = isrCounter;
    isrTime = lastIsrAt;
    portEXIT_CRITICAL(&timerMux);
    timerEnd(timer);
    timer = NULL;
    u8g2.setPowerSave(1);
  }
  if (timer) {
    show_clock();
  }
  delay(100);
}

