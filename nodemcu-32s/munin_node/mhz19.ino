#if defined(ENABLE_MHZ19)

char *node_config_mhz19_co2 =
  "graph_title CO2濃度(%s)\n"
  "graph_args --base 1000 -r -l 0 -u 2500 --alt-autoscale\n"
  "graph_vlabel ppm\n"
  "graph_scale no\n"
  "graph_category measure_co2\n"
  "graph_order co2 inter trend\n"
  "graph_info MH-Z19センサーで測定したCO2濃度\n"
  "co2.label 濃度　　\n"
  "co2.info MH-Z19センサーのCO2濃度\n"
  "co2.warning  :1500\n"
  "co2.critical :2000\n"
  "inter.cdef co2,UN,PREV,co2,IF\n"
  "inter.graph no\n"
  "inter.update no\n"
  "trend.label 移動平均\n"
  "trend.line 2000:880000\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info CO2濃度の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

float meas_co2;

#include <SoftwareSerial.h>
#include "MHZ19.h"

// #define RX_PIN 17                                          // Rx pin which the MHZ19 Tx pin is attached to
// #define TX_PIN 16                                          // Tx pin which the MHZ19 Rx pin is attached to
#define RX_PIN 5                                           // Rx pin which the MHZ19 Tx pin is attached to
#define TX_PIN 4                                           // Tx pin which the MHZ19 Rx pin is attached to
#define BAUDRATE 9600                                      // Device to MH-Z19 Serial baudrate (should not be changed)

MHZ19 myMHZ19;                                             // Constructor for library
// HardwareSerial mySerial(2);                                // (ESP32 Example) create device to MH-Z19 serial
SoftwareSerial mySerial;

int mhz19_init(void) {
  // mySerial.begin(BAUDRATE, SERIAL_8N1, RX_PIN, TX_PIN);   // (ESP32 Example) device to MH-Z19 serial start   
  mySerial.begin(BAUDRATE, SWSERIAL_8N1, RX_PIN, TX_PIN, false);   // (ESP32 Example) device to MH-Z19 serial start   
  myMHZ19.begin(mySerial);                                // *Serial(Stream) refence must be passed to library begin(). 
  myMHZ19.autoCalibration();                              // Turn auto calibration ON (OFF autoCalibration(false))
}

void mhz19_getValues() {
  while(true) {
    meas_co2 = myMHZ19.getCO2();                          // Request CO2 (as ppm)
    if(meas_co2 > 300) break;
    sleep(100);
  }
}

void do_config_mhz19_co2(WiFiClient &client) {
  client.printf(node_config_mhz19_co2,node_name);
  Serial.printf(node_config_mhz19_co2,node_name);
}

void do_fetch_mhz19_co2(WiFiClient &client) {
  mhz19_getValues();
  client.printf("co2.value %f\n",meas_co2);
  Serial.printf("meas_co2: %5.1f\n", meas_co2);
}

//
// loop
//

void mhz19_loop() {
}

#endif
