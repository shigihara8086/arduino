/*
 Munin-node
*/

#include <WiFi.h>
#include <WiFiClient.h>
#include <ArduinoOTA.h>

// #define NODE_BLANK                               // esp32-pico-kit for blank node
// #define NODE_WROVER                              // esp32-wrover with 18650
// #define NODE_PIR                                 // esp32-dev module COM3
// #define NODE_COOLERBOX                           // nodemcu-32s COM16
// #define NODE_STOREROOM                           // nodemcu-32s COM20
// #define NODE_LIVING                              // nodemcu-32s COM?
// #define NODE_DS18B20                             // nodemcu-32s ds18b20 COM19
// #define NODE_OUTER2                              // nodemcu-32s DHT22 outside COM??
// #define NODE_LOCAL2                              // nodemcu-32s COM?
// #define NODE_CCS811                              // nodemcu-32s COM?
// #define NODE_CO2                                 // nodemcu-32s COM?
#define NODE_CO22                                // nodemcu-32s COM?

#include "munin_node.h"

WiFiServer server(4949);

#if defined(ENABLE_BLANK)
char *node_config_blank =
  "graph_title _\n"
  "graph_args --base 1000 -r -l -1 -u 1\n"
  "graph_vlabel _\n"
  "graph_scale no\n"
  "graph_category measure_blank\n"
  "graph_info _\n"
  "blank.graph no\n";
#endif

#if defined(ENABLE_HALL)
char *node_config_hall =
  "graph_title 磁気(%s)\n"
  "graph_args --base 1000 --alt-autoscale\n"
  "graph_vlabel 値\n"
  "graph_scale no\n"
  "graph_order hall inter\n"
  "graph_category measure_hall\n"
  "graph_info 内蔵磁気センサーの生データ。通常+200～-200程度の範囲で外部磁界の状態を示す。\n"
  "hall.graph no\n"
  "inter.label 値\n"
  "inter.info 内蔵ホールセンサー値\n"
  "inter.draw LINE1\n"
  "inter.cdef hall,UN,PREV,hall,IF\n"
  "inter.update no\n";
#endif

char *node_config_boot =
  "graph_title 死活(%s)\n"
  "graph_args --base 1000 -r -l 0 --alt-autoscale\n"
  "graph_vlabel 回\n"
  "graph_scale no\n"
  "graph_order boot inter\n"
  "graph_category measure_alive\n"
  "graph_info 死活状態の監視用。Muninがデータ取得を行うとカウントアップ。100でリセットする。\n"
  "boot.graph no\n"
  "inter.label 死活\n"
  "inter.info 死活\n"
  "inter.draw LINE1\n"
  "inter.cdef boot,UN,PREV,boot,IF\n"
  "inter.update no\n";

char *node_config_coretemp =
  "graph_title コア温度(%s)\n"
  "graph_args --base 1000\n"
  "graph_vlabel ℃\n"
  "graph_scale no\n"
  "graph_category measure_coretemp\n"
  "graph_order temp inter trend\n"
  "graph_info ESP32のコア温度\n"
  "temp.label コア温度\n"
  "temp.info ESP32のコア温度\n"
  "inter.cdef temp,UN,PREV,temp,IF\n"
  "inter.graph no\n"
  "inter.update no\n"
  "trend.label 移動平均\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info 温度の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

int measure_count = 0;
boolean cmd_done;
int loop_count = 0;
long idle_count = 0;
boolean led_state = false;

bool do_list(WiFiClient &client) {
#if defined(ENABLE_BLANK)
  client.print("blank boot coretemp rssi channel");
#else
  client.print("boot coretemp rssi channel");
#endif
#if defined(ENABLE_HALL)
  client.print(" hall");
#endif
#if defined(ENABLE_PIR)
  client.print(" pir");
#endif
#if defined(ENABLE_BME280)
  client.print(" temp humidity di wbgt dewpoint pressure");
#endif
#if defined(ENABLE_SHT3X)
  client.print(" temp humidity di wbgt dewpoint");
#endif
#if defined(ENABLE_DHT11)
  client.print(" temp humidity di wbgt dewpoint");
#endif
#if defined(ENABLE_DHT22)
  client.print(" temp humidity di wbgt dewpoint");
#endif
#if defined(ENABLE_ADT7410)
  client.print(" temp");
#endif
#if defined(ENABLE_DS18B20)
  client.print(" temp");
#endif
#if defined(ENABLE_CCS811)
  client.print(" co2 tvoc");
#endif
#if defined(ENABLE_MHZ19)
  client.print(" co2");
#endif
  client.printf("\n");
  return true;
}

bool do_version(WiFiClient &client) {
  client.print("munin node at arduino version: 0.1\n");
  return true;
}

#if defined(ENABLE_BLANK)
void do_config_blank(WiFiClient &client) {
  client.print(node_config_blank);
  Serial.print(node_config_blank);
}
#endif

void do_config_boot(WiFiClient &client) {
  client.printf(node_config_boot,node_name);
  Serial.printf(node_config_boot,node_name);
}

void do_config_coretemp(WiFiClient &client) {
  client.printf(node_config_coretemp,node_name);
  Serial.printf(node_config_coretemp,node_name);
}

#if defined(ENABLE_HALL)
void do_config_hall(WiFiClient &client) {
  client.printf(node_config_hall,node_name);
  Serial.printf(node_config_hall,node_name);
}
#endif

bool do_config(WiFiClient &client, String arg) {
#if defined(ENABLE_BLANK)
  if     (arg.equals(String("blank")))    { do_config_blank(client); }
  else if(arg.equals(String("boot")))     { do_config_boot(client); }
  else if(arg.equals(String("coretemp"))) { do_config_coretemp(client); }
  else if(arg.equals(String("rssi")))     { do_config_rssi(client); }
  else if(arg.equals(String("channel")))  { do_config_channel(client); }
#else
  if     (arg.equals(String("boot")))     { do_config_boot(client); }
  else if(arg.equals(String("coretemp"))) { do_config_coretemp(client); }
  else if(arg.equals(String("rssi")))     { do_config_rssi(client); }
  else if(arg.equals(String("channel")))  { do_config_channel(client); }
#endif
#if defined(ENABLE_HALL)
  else if(arg.equals(String("hall")))     { do_config_hall(client); }
#endif
#if defined(ENABLE_PIR)
  else if(arg.equals(String("pir")))      { do_config_pir(client); }
#endif
#if defined(ENABLE_BME280)
  else if(arg.equals(String("temp")))     { do_config_bme280_temp(client); }
  else if(arg.equals(String("humidity"))) { do_config_bme280_humi(client); }
  else if(arg.equals(String("di")))       { do_config_bme280_di(client); }
  else if(arg.equals(String("wbgt")))     { do_config_bme280_wbgt(client); }
  else if(arg.equals(String("dewpoint"))) { do_config_bme280_dewp(client); }
  else if(arg.equals(String("pressure"))) { do_config_bme280_press(client); }
#endif
#if defined(ENABLE_SHT3X)
  else if(arg.equals(String("temp")))     { do_config_sht3x_temp(client); }
  else if(arg.equals(String("humidity"))) { do_config_sht3x_humi(client); }
  else if(arg.equals(String("di")))       { do_config_sht3x_di(client); }
  else if(arg.equals(String("wbgt")))     { do_config_sht3x_wbgt(client); }
  else if(arg.equals(String("dewpoint"))) { do_config_sht3x_dewp(client); }
#endif
#if defined(ENABLE_DHT11)
  else if(arg.equals(String("temp")))     { do_config_dht11_temp(client); }
  else if(arg.equals(String("humidity"))) { do_config_dht11_humi(client); }
  else if(arg.equals(String("di")))       { do_config_dht11_di(client); }
  else if(arg.equals(String("wbgt")))     { do_config_dht11_wbgt(client); }
  else if(arg.equals(String("dewpoint"))) { do_config_dht11_dewp(client); }
#endif
#if defined(ENABLE_DHT22)
  else if(arg.equals(String("temp")))     { do_config_dht22_temp(client); }
  else if(arg.equals(String("humidity"))) { do_config_dht22_humi(client); }
  else if(arg.equals(String("di")))       { do_config_dht22_di(client); }
  else if(arg.equals(String("wbgt")))     { do_config_dht22_wbgt(client); }
  else if(arg.equals(String("dewpoint"))) { do_config_dht22_dewp(client); }
#endif
#if defined(ENABLE_ADT7410)
  else if(arg.equals(String("temp")))     { do_config_adt7410_temp(client); }
#endif
#if defined(ENABLE_DS18B20)
  else if(arg.equals(String("temp")))     { do_config_ds18b20_temp(client); }
#endif
#if defined(ENABLE_CCS811)
  else if(arg.equals(String("co2")))      { do_config_ccs811_co2(client); }
  else if(arg.equals(String("tvoc")))     { do_config_ccs811_tvoc(client); }
#endif
#if defined(ENABLE_MHZ19)
  else if(arg.equals(String("co2")))      { do_config_mhz19_co2(client); }
#endif
  else {
    client.print("# Unknown service\n");
  }
  client.printf(".\n");
  return true;
}

void do_fetch_blank(WiFiClient &client) {
  client.print("blank.value NaN\n");
  Serial.print("blank: NaN\n");
}

void do_fetch_boot(WiFiClient &client) {
  client.printf("boot.value %d\n",measure_count);
  Serial.printf("boot: %d\n", measure_count);
}

void do_fetch_coretemp(WiFiClient &client) {
float temp = temperatureRead();
  client.printf("temp.value %f\n",temp);
  Serial.printf("coretemp: %f℃\n", temp);
}

#if defined(ENABLE_HALL)
void do_fetch_hall(WiFiClient &client) {
int hall;
  hall = hallRead();
  client.printf("hall.value %d\n",hall);
  Serial.printf("hall: %d\n", hall);
}
#endif

bool do_fetch(WiFiClient &client, String arg) {
#if defined(ENABLE_BLANK)
  if     (arg.equals(String("blank")))    { do_fetch_blank(client); }
  else if(arg.equals(String("boot")))     { do_fetch_boot(client); }
  else if(arg.equals(String("coretemp"))) { do_fetch_coretemp(client); }
  else if(arg.equals(String("rssi")))     { do_fetch_rssi(client); }
  else if(arg.equals(String("channel")))  { do_fetch_channel(client); }
#else
  if     (arg.equals(String("boot")))     { do_fetch_boot(client); }
  else if(arg.equals(String("coretemp"))) { do_fetch_coretemp(client); }
  else if(arg.equals(String("rssi")))     { do_fetch_rssi(client); }
  else if(arg.equals(String("channel")))  { do_fetch_channel(client); }
#endif
#if defined(ENABLE_HALL)
  else if(arg.equals(String("hall")))     { do_fetch_hall(client); }
#endif
#if defined(ENABLE_PIR)
  else if(arg.equals(String("pir")))      { do_fetch_pir(client); }
#endif
#if defined(ENABLE_BME280)
  else if(arg.equals(String("temp")))     { do_fetch_bme280_temp(client); }
  else if(arg.equals(String("humidity"))) { do_fetch_bme280_humi(client); }
  else if(arg.equals(String("di")))       { do_fetch_bme280_di(client); }
  else if(arg.equals(String("wbgt")))     { do_fetch_bme280_wbgt(client); }
  else if(arg.equals(String("dewpoint"))) { do_fetch_bme280_dewp(client); }
  else if(arg.equals(String("pressure"))) { do_fetch_bme280_press(client); }
#endif
#if defined(ENABLE_SHT3X)
  else if(arg.equals(String("temp")))     { do_fetch_sht3x_temp(client); }
  else if(arg.equals(String("humidity"))) { do_fetch_sht3x_humi(client); }
  else if(arg.equals(String("di")))       { do_fetch_sht3x_di(client); }
  else if(arg.equals(String("wbgt")))     { do_fetch_sht3x_wbgt(client); }
  else if(arg.equals(String("dewpoint"))) { do_fetch_sht3x_dewp(client); }
#endif
#if defined(ENABLE_DHT11)
  else if(arg.equals(String("temp")))     { do_fetch_dht11_temp(client); }
  else if(arg.equals(String("humidity"))) { do_fetch_dht11_humi(client); }
  else if(arg.equals(String("di")))       { do_fetch_dht11_di(client); }
  else if(arg.equals(String("wbgt")))     { do_fetch_dht11_wbgt(client); }
  else if(arg.equals(String("dewpoint"))) { do_fetch_dht11_dewp(client); }
#endif
#if defined(ENABLE_DHT22)
  else if(arg.equals(String("temp")))     { do_fetch_dht22_temp(client); }
  else if(arg.equals(String("humidity"))) { do_fetch_dht22_humi(client); }
  else if(arg.equals(String("di")))       { do_fetch_dht22_di(client); }
  else if(arg.equals(String("wbgt")))     { do_fetch_dht22_wbgt(client); }
  else if(arg.equals(String("dewpoint"))) { do_fetch_dht22_dewp(client); }
#endif
#if defined(ENABLE_ADT7410)
  else if(arg.equals(String("temp")))     { do_fetch_adt7410_temp(client); }
#endif
#if defined(ENABLE_DS18B20)
  else if(arg.equals(String("temp")))     { do_fetch_ds18b20_temp(client); }
#endif
#if defined(ENABLE_CCS811)
  else if(arg.equals(String("co2")))      { do_fetch_ccs811_co2(client); }
  else if(arg.equals(String("tvoc")))     { do_fetch_ccs811_tvoc(client); }
#endif
#if defined(ENABLE_MHZ19)
  else if(arg.equals(String("co2")))      { do_fetch_mhz19_co2(client); }
#endif
  else {
    client.print("# Unknown service\n");
  }
  client.printf(".\n");
  return true;
}

int do_client(WiFiClient &client) {
char buffer[128];
String cmd, arg;
char c;
byte pos = 0;
  while (client.connected()) {
    if (client.available()) {
      c = client.read();
      if (c != '\n') {
        if (c == '\r') c = '\0';
        buffer[pos] = c;
        pos += 1;
      } else {
        // reset flags on new command
        cmd_done = false;
        buffer[pos] = '\0';
        pos = 0;
        String cmd(strtok(buffer, " "));
        String arg(strtok(NULL, " "));
        Serial.print("Command: ");
        Serial.print(cmd);
        Serial.print(" ");
        Serial.print(arg);
        Serial.print("\n");
        if (cmd.equals(String("quit"))) {
          break;
        }
        if (cmd.equals(String("list")))    cmd_done = do_list(client);
        if (cmd.equals(String("version"))) cmd_done = do_version(client);
        if (cmd.equals(String("config")))  cmd_done = do_config(client, arg);
        if (cmd.equals(String("fetch")))   cmd_done = do_fetch(client, arg);
        if (!cmd_done) {
          client.print("# Unknown command. Try list, config, fetch, version or quit\n");
        }
      }
    }
  }
}

void setup() {
  Serial.begin(115200);
#if defined(LED_BUILTIN)
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN,HIGH);
#endif
  wifi_init();
#if defined(LED_BUILTIN)
  digitalWrite(LED_BUILTIN,LOW);
#endif
  ntp_init();
#if defined(ENABLE_PIR)
  pir_init();
#endif
#if defined(ENABLE_BME280)
  bme280_init();
#endif
#if defined(ENABLE_SHT3X)
  sht3x_init();
#endif
#if defined(ENABLE_DHT11)
  dht11_init();
#endif
#if defined(ENABLE_DHT22)
  dht22_init();
#endif
#if defined(ENABLE_ADT7410)
  adt7410_init();
#endif
#if defined(ENABLE_DS18B20)
  ds18b20_init();
#endif
#if defined(ENABLE_CCS811)
  ccs811_init();
#endif
#if defined(ENABLE_MHZ19)
  mhz19_init();
#endif
  server.begin();
  Serial.println("TCP server started");
}

void loop() {
  WiFiClient client = server.available();
  if(client) {
    Serial.println("New connection");
    client.printf("# munin node at %s\n", node_name);
#if defined(LED_BUILTIN)
    digitalWrite(LED_BUILTIN,HIGH);
#endif
    do_client(client);
#if defined(LED_BUILTIN)
    digitalWrite(LED_BUILTIN,LOW);
#endif
    client.stop();
    Serial.println("Close connection");
    measure_count ++;
    if(measure_count > 100) measure_count = 0;
    // アイドルカウンターリセット
    idle_count = 0;
  }
#if defined(ENABLE_PIR)
  pir_loop();
#endif
#if defined(ENABLE_BME280)
  bme280_loop();
#endif
#if defined(ENABLE_SHT3X)
  sht3x_loop();
#endif
#if defined(ENABLE_DHT11)
  dht11_loop();
#endif
#if defined(ENABLE_DHT22)
  dht22_loop();
#endif
#if defined(ENABLE_ADT7410)
  adt7410_loop();
#endif
#if defined(ENABLE_DS18B20)
  ds18b20_loop();
#endif
#if defined(ENABLE_CCS811)
  ccs811_loop();
#endif
#if defined(ENABLE_MHZ19)
  mhz19_loop();
#endif
  // LEDをフラッシュ
  loop_count ++;
  if(loop_count > 990) {
#if defined(LED_BUILTIN)
    if(led_state == false) {
      digitalWrite(LED_BUILTIN,HIGH);
      led_state = true;
    }
  } else {
    if(led_state == true) {
      digitalWrite(LED_BUILTIN,LOW);
      led_state = false;
    }
#endif
  }
  if(loop_count > 1000) loop_count = 0;
  // 15分アイドル状態ならリブート
  idle_count ++;
  if(idle_count > (15 * 60 * 1000L)) {
    ESP.restart();
  }
  ArduinoOTA.handle();
  delay(1);
}
