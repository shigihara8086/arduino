#include <Wire.h>
#include "Adafruit_BME280.h"
#include "Adafruit_Sensor.h"

// #define I2C_SDA 32
// #define I2C_SCL 33
// #define I2C_SDA 25
#define I2C_SDA 22
#define I2C_SCL 21
#define SEALEVEL_ADJUSTMENT ((260.0 / 100.0) * 12.0)
#define BME280_ADD 0x76

void getValues(void);

Adafruit_BME280 bme(I2C_SDA, I2C_SCL);

void setup() {
  pinMode(I2C_SDA, OUTPUT);
  pinMode(I2C_SCL, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(115200);
  Serial.println("Program Start");

  bool status;

  status = bme.begin(BME280_ADD);
  if (!status) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    while (1);
  }
  delay(1000);
}

void loop() {
  digitalWrite(LED_BUILTIN, HIGH);
  getValues();
  delay(1000);
  digitalWrite(LED_BUILTIN, LOW);
  delay(2000);
}

void getValues() {
  Serial.print("気温 = ");
  Serial.print(bme.readTemperature());
  Serial.println(" ℃");

  Serial.print("気圧 = ");

  Serial.print(bme.readPressure() / 100.0F + SEALEVEL_ADJUSTMENT);
  Serial.println(" hPa");

  Serial.print("湿度 = ");
  Serial.print(bme.readHumidity());
  Serial.println(" %");

  Serial.println();
}
