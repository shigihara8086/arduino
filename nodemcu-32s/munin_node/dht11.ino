/*
 DHT11
*/

#if defined(ENABLE_DHT11)

char *node_config_dht11_temp =
  "graph_title 温度(%s)\n"
  "graph_args --base 1000 --alt-autoscale\n"
  "graph_vlabel ℃\n"
  "graph_scale no\n"
  "graph_order temp inter trend\n"
  "graph_category measure_temperature\n"
  "graph_info DHT11センサーで測定した温度\n"
  "temp.label 温度　　\n"
  "temp.info DHT11センサーの温度\n"
  "inter.cdef temp,UN,PREV,temp,IF\n"
  "inter.graph no\n"
  "inter.update no\n"
  "trend.label 移動平均\n"
  "trend.line 25.0:880000\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info 温度の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

char *node_config_dht11_humi =
  "graph_title 湿度(%s)\n"
  "graph_args --base 1000 --alt-autoscale\n"
  "graph_vlabel %%\n"
  "graph_scale no\n"
  "graph_order humi inter trend\n"
  "graph_category measure_humidity\n"
  "graph_info DHT11センサーで測定した湿度\n"
  "humi.label 湿度　　\n"
  "humi.info DHT11センサーの湿度\n"
  "inter.cdef humi,UN,PREV,humi,IF\n"
  "inter.graph no\n"
  "inter.update no\n"
  "trend.label 移動平均\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info 温度の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

char *node_config_dht11_di =
  "graph_title 不快指数(%s)\n"
  "graph_args --base 1000 -l 55.0 -u 85.0 --alt-y-grid --alt-autoscale\n"
  "graph_vlabel DI\n"
  "graph_scale no\n"
  "graph_order di inter trend\n"
  "graph_category measure_di\n"
  "graph_info DHT11センサーの温度・湿度から計算した不快指数\n"
  "di.label 不快指数\n"
  "di.info DHT11センサーの温度・湿度から計算\n"
  "di.warning 60:80\n"
  "di.critical 55:85\n"
  "inter.cdef di,UN,PREV,di,IF\n"
  "inter.graph no\n"
  "inter.update no\n"
  "trend.warning 55:85\n"
  "trend.critical 55:85\n"
  "trend.label 移動平均\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info 不快指数の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

char *node_config_dht11_wbgt =
  "graph_title 暑さ指数(%s)\n"
  "graph_args --base 1000 --alt-y-grid --alt-autoscale\n"
  "graph_vlabel ℃\n"
  "graph_scale no\n"
  "graph_order wbgt inter trend\n"
  "graph_category measure_wbgt\n"
  "graph_info DHT11センサーの温度・湿度から計算した暑さ指数\n"
  "wbgt.label 暑さ指数\n"
  "wbgt.info DHT11センサーの温度・湿度から計算\n"
  "wbgt.warning  :28.0\n"
  "wbgt.critical :31.0\n"
  "inter.cdef wbgt,UN,PREV,wbgt,IF\n"
  "inter.graph no\n"
  "inter.update no\n"
  "trend.label 移動平均\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info 暑さ指数の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

char *node_config_dht11_dewp =
  "graph_title 露点温度(%s)\n"
  "graph_args --base 1000 --alt-y-grid --alt-autoscale\n"
  "graph_vlabel ℃\n"
  "graph_scale no\n"
  "graph_order dewp inter trend\n"
  "graph_category measure_dewpoint\n"
  "graph_info DHT11センサーの温度・湿度から計算した露点温度\n"
  "dewp.label 露点温度\n"
  "dewp.info DHT11センサーの温度・湿度から計算\n"
  "inter.cdef dewp,UN,PREV,dewp,IF\n"
  "inter.graph no\n"
  "inter.update no\n"
  "trend.label 移動平均\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info 露点温度の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

#include "DHTesp.h"

float meas_temp = 0.0;
float meas_humi = 0.0;
float meas_di   = 0.0;
float meas_wbgt = 0.0;
float meas_dewp = 0.0;

// 露点温度計算用のダミー
float meas_press = 1013.25;

DHTesp dht;

void dht11_init() {
  dht.setup(17,DHTesp::DHT11); // Connect DHT sensor to GPIO 17
}

//
// config
//

void do_config_dht11_temp(WiFiClient &client) {
float sr = 0.0;
float ws = 0.0;
  while(true) {
    delay(dht.getMinimumSamplingPeriod());
    meas_temp = dht.getTemperature();
    meas_humi = dht.getHumidity();
    if(meas_temp > -50 && meas_temp < 100) {
      meas_di   = 0.81 * meas_temp + (0.01 * meas_humi * (0.99 * meas_temp - 14.3)) + 46.3;
      meas_wbgt = 0.735 * meas_temp + 0.0374 * meas_humi + 0.00292 * meas_temp * meas_humi + 7.619 * sr - 4.557 * sr * sr - 0.0572 * ws - 4.064;
      meas_dewp = computeByRH(meas_temp, meas_humi);
      Serial.printf("気温     : %5.1fºC\n", meas_temp);
      Serial.printf("湿度     : %5.1f%%\n", meas_humi);
      Serial.printf("不快指数 :   %5.1f\n", meas_di);
      Serial.printf("暑さ指数 : %5.1fºC\n", meas_wbgt);
      Serial.printf("露点温度 : %5.1fºC\n", meas_dewp);
      break;
    }
    delay(100);
  }
  client.printf(node_config_dht11_temp,node_name);
  Serial.printf(node_config_dht11_temp,node_name);
}

void do_config_dht11_humi(WiFiClient &client) {
  client.printf(node_config_dht11_humi,node_name);
  Serial.printf(node_config_dht11_humi,node_name);
}

void do_config_dht11_di(WiFiClient &client) {
  client.printf(node_config_dht11_di,node_name);
  Serial.printf(node_config_dht11_di,node_name);
}

void do_config_dht11_wbgt(WiFiClient &client) {
  client.printf(node_config_dht11_wbgt,node_name);
  Serial.printf(node_config_dht11_wbgt,node_name);
}

void do_config_dht11_dewp(WiFiClient &client) {
  client.printf(node_config_dht11_dewp,node_name);
  Serial.printf(node_config_dht11_dewp,node_name);
}

//
// fetch
//

void do_fetch_dht11_temp(WiFiClient &client) {
  client.printf("temp.value %f\n",meas_temp);
  Serial.printf("meas_temp: %5.1f\n", meas_temp);
}

void do_fetch_dht11_humi(WiFiClient &client) {
  client.printf("humi.value %f\n",meas_humi);
  Serial.printf("meas_humi: %5.1f\n", meas_humi);
}

void do_fetch_dht11_di(WiFiClient &client) {
  client.printf("di.value %f\n",meas_di);
  Serial.printf("meas_di: %5.1f\n", meas_di);
}

void do_fetch_dht11_wbgt(WiFiClient &client) {
  client.printf("wbgt.value %f\n",meas_wbgt);
  Serial.printf("meas_wbgt: %5.1f\n", meas_wbgt);
}

void do_fetch_dht11_dewp(WiFiClient &client) {
  client.printf("dewp.value %f\n",meas_dewp);
  Serial.printf("meas_dewp: %5.1f\n", meas_dewp);
}

//
// loop
//

void dht11_loop() {
}

#endif
