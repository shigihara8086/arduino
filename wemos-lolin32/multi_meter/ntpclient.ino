/*
  NTP client
*/

#include <WiFi.h>
#include <DS3231.h>

#include "multi_meter.h"

#define JST (3600L * 9)

/*
  const char *ssid = "106F3FDBCFA9";
  const char *password = "amhh8khbexssm";
*/

const char *ntp_servers[] { "192.168.254.7", "ntp.nict.jp", "time.google.com" };

struct tm timeInfo;
DS3231 rtc;
bool rtc_century = false;
bool rtc_h12, rtc_pm;

char *dayofweek[] = { "Sunday", "Monday", "Tuesday", "Wednsday", "Thursday", "Friday", "Saturday" };
char *dayofweek_short[] = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };

int ntp_init() {
  int count;

#if !defined ENABLE_OTA
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  if (WiFi.begin(ssid, password) != WL_DISCONNECTED) {
    ESP.restart();
  }
  count = 0;
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    count ++;
    if (count > 5) {
      return -1;
    }
  }
#endif
  configTime(JST, 0, ntp_servers[0], ntp_servers[1], ntp_servers[2]);
  // sprintf(datebuf,"%02d/%02d/%02d(%s)",timeInfo.tm_year + 1900 - 2000, timeInfo.tm_mon + 1, timeInfo.tm_mday,dayofweek_short[timeInfo.tm_wday]);
  // sprintf(timebuf,"%02d:%02d:%02d",timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec);
  getLocalTime(&timeInfo);
  rtc.setClockMode(false);  // set to 24h
  rtc.setYear(timeInfo.tm_year + 1900);
  rtc.setMonth(timeInfo.tm_mon + 1);
  rtc.setDate(timeInfo.tm_mday);
  rtc.setDoW(timeInfo.tm_wday);
  rtc.setHour(timeInfo.tm_hour);
  rtc.setMinute(timeInfo.tm_min);
  rtc.setSecond(timeInfo.tm_sec);
  return 0;
}

void ntp_ticker() {
  char strbuf[100];

#if defined ENABLE_OTA
  ArduinoOTA.handle();
#endif
  if (!ntp_disable) {
    getLocalTime(&timeInfo);
    sprintf(strbuf, "%02d/%02d %02d:%02d:%02d",
            timeInfo.tm_mon + 1, timeInfo.tm_mday, timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec);

    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(128, 0, strbuf);
    if ((timeInfo.tm_hour % 3) == 0 && timeInfo.tm_min == 0 && timeInfo.tm_sec == 0) {
      configTime(JST, 0, ntp_servers[0], ntp_servers[1], ntp_servers[2]);
      Serial.print("*ntp-sync*");
      getLocalTime(&timeInfo);
      rtc.setClockMode(false);  // set to 24h
      rtc.setYear(timeInfo.tm_year + 1900);
      rtc.setMonth(timeInfo.tm_mon + 1);
      rtc.setDate(timeInfo.tm_mday);
      rtc.setDoW(timeInfo.tm_wday);
      rtc.setHour(timeInfo.tm_hour);
      rtc.setMinute(timeInfo.tm_min);
      rtc.setSecond(timeInfo.tm_sec);
      delay(1000);
    }
  } else {
    int year  = rtc.getYear();
    int mon   = rtc.getMonth(rtc_century);
    int day   = rtc.getDate();
    int hour  = rtc.getHour(rtc_h12, rtc_pm);
    int min   = rtc.getMinute();
    int sec   = rtc.getSecond();
    char *dow = dayofweek_short[rtc.getDoW()];
    // sprintf(strbuf, "%02d/%02d(%s) %02d:%02d:%02d", mon, day, dow, hour, min, sec);
    sprintf(strbuf, "%02d/%02d %02d:%02d:%02d", mon, day, hour, min, sec);

    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(128, 0, strbuf);
  }
  display.drawLine(0, 11, 128, 11);
}

void ntp_display(int times, int wait) {
  char datebuf[20], timebuf[20];
  int head_pos =  0;
  int data_pos = 128;
  int date_pos = (16 - LINE_OFFSET);
  int time_pos = (32 - LINE_OFFSET);
  int week_pos = (48 - LINE_OFFSET);
  bool done = false;

  for (int i = 0; i < times; i ++) {
    if (ntp_disable == 0) {
      getLocalTime(&timeInfo);
      sprintf(datebuf, "%04d/%02d/%02d", timeInfo.tm_year + 1900, timeInfo.tm_mon + 1, timeInfo.tm_mday);
      sprintf(timebuf, "%02d:%02d:%02d", timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec);
    } else {
      int year  = rtc.getYear() + 1952;
      int mon   = rtc.getMonth(rtc_century);
      int day   = rtc.getDate();
      int hour  = rtc.getHour(rtc_h12, rtc_pm);
      int min   = rtc.getMinute();
      int sec   = rtc.getSecond();
      char *dow = dayofweek_short[rtc.getDoW()];
      sprintf(datebuf, "%04d/%02d/%02d", year, mon, day);
      sprintf(timebuf, "%02d:%02d:%02d", hour, min, sec);
    }
    display.clear();
    ntp_ticker();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    if (ntp_disable == 0) {
      display.drawString(0, 0, "NTP");
    } else {
      display.drawString(0, 0, "DS3231");
    }
    display.setFont(ArialMT_Plain_16);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(head_pos, date_pos, "Date");
    display.drawString(head_pos, time_pos, "Time");
    display.drawString(head_pos, week_pos, "Week");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(data_pos, date_pos, datebuf);
    display.drawString(data_pos, time_pos, timebuf);
    if (ntp_disable == 0) {
      display.drawString(data_pos, week_pos, dayofweek[timeInfo.tm_wday]);
    } else {
      display.drawString(data_pos, week_pos, dayofweek[rtc.getDoW()]);
    }

    if (not done) {
      Serial.printf("DATE:%s,TIME:%s ", datebuf, timebuf);
      done = true;
    }
    display.display();
    delay(wait);
  }
}

