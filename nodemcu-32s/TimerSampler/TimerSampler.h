/*
 board base definition 
*/

//
// SSID and Password structure
//

struct WifiMulti_aps {                          // ssid,password pare
  char *ssid;
  char *passwd;
};

//
// difinitions for each boards
//

#if defined(NODE_LTE)
#define LTE_POWER           (23)                // GPIO23 lte_power on digital port
#define LTE_POWERLED        (LED_BUILTIN)       // Use Bultin LED
#define TIME_TO_ON          (50)                // MT100 startup wait time
#define WAKE_OFFSET         (30)                // wakeup time offset use other overhead
#define TIME_TO_SLEEP       (60 * 60)           // Time ESP32 will go to sleep (in seconds)
#define ENABLE_BME280
#define BME280_ADD          (0x76)
#define I2C_SDA             (25)                // I2C SDA port
#define I2C_SCL             (21)                // I2C SCL port
#define NODE_NAME           "esp32"             // node name
struct WifiMulti_aps wifi_aps[] = {             // ssid,password table
  { "MT100_4517","cc3263f327" },
  { NULL, NULL }
};
const char *ntp_hosts[] = {                        // NTP servers
  "sgmail.jp",
  "sgmail.jp",
  "sgmail.jp",
  NULL
};
#endif

#if defined(NODE_OUTER3)
#define LTE_POWER           (23)                // GPIO23 lte_power on digital port
#define LTE_POWERLED        (LED_BUILTIN)       // Use Bultin LED
#define TIME_TO_ON          (1)                 // MT100 startup wait time
#define WAKE_OFFSET         (29)                // wakeup time offset use other overhead
#define TIME_TO_SLEEP       (10 * 60)           // Time ESP32 will go to sleep (in seconds)
#define ENABLE_BME280
#define BME280_ADD          (0x76)
#define I2C_SDA             (21)                // I2C SDA port
#define I2C_SCL             (22)                // I2C SCL port
#define NODE_NAME           "esp32-outer3"      // node name
struct WifiMulti_aps wifi_aps[] = {             // ssid,password table
//  { "aterm-20cd3f-g",  "65676ae9e9f9b" },
  { "106F3FDBCFA9_EXT","amhh8khbexssm" },
  { NULL, NULL }
};
const char *ntp_hosts[] = {                        // NTP servers
  "192.168.254.7",
  "192.168.254.7",
  "192.168.254.7",
  NULL
};
#endif

#if defined(NODE_OUTER4)
#define LTE_POWER           (23)                // GPIO23 lte_power on digital port
#define LTE_POWERLED        (LED_BUILTIN)       // Use Bultin LED
#define TIME_TO_ON          (1)                 // MT100 startup wait time
#define WAKE_OFFSET         (29)                // wakeup time offset use other overhead
#define TIME_TO_SLEEP       (10 * 60)           // Time ESP32 will go to sleep (in seconds)
#define ENABLE_BME280
#define BME280_ADD          (0x76)
#define I2C_SDA             (21)                // I2C SDA port
#define I2C_SCL             (22)                // I2C SCL port
#define NODE_NAME           "esp32-outer4"      // node name
struct WifiMulti_aps wifi_aps[] = {             // ssid,password table
  { "aterm-20cd3f-g",  "65676ae9e9f9b" },
//  { "106F3FDBCFA9_EXT","amhh8khbexssm" },
  { NULL, NULL }
};
const char *ntp_hosts[] = {                        // NTP servers
  "192.168.254.7",
  "192.168.254.7",
  "192.168.254.7",
  NULL
};
#endif

#if defined(NODE_BLANK)
#define LTE_POWER           (23)                // GPIO23 lte_power on digital port
#define LTE_POWERLED        (LED_BUILTIN)       // Use Bultin LED
#define TIME_TO_ON          (1)                 // MT100 startup wait time
#define WAKE_OFFSET         (29)                // wakeup time offset use other overhead
#define TIME_TO_SLEEP       (5 * 60)            // Time ESP32 will go to sleep (in seconds)
// #define ENABLE_BME280
// #define ENABLE_SHT3X
#define NODE_NAME           "esp32-blank"        // node name
struct WifiMulti_aps wifi_aps[] = {             // ssid,password table
  { "106F3FDBCFA9_EXT","amhh8khbexssm" },
  { NULL, NULL }
};
const char *ntp_hosts[] = {                        // NTP servers
  "192.168.254.7",
  "192.168.254.7",
  "192.168.254.7",
  NULL
};
#endif

#if defined(NODE_DHT22)
#define LTE_POWER           (23)                // GPIO23 lte_power on digital port
#define LTE_POWERLED        (LED_BUILTIN)       // Use Bultin LED
#define TIME_TO_ON          (1)                 // MT100 startup wait time
#define WAKE_OFFSET         (29)                // wakeup time offset use other overhead
#define TIME_TO_SLEEP       (10 * 60)           // Time ESP32 will go to sleep (in seconds)
// #define ENABLE_BME280
// #define ENABLE_SHT3X
#define ENABLE_DHT22
#define GPIO_PORT           (23)                // Use GPIO PORT 4
#define NODE_NAME           "esp32-dht22"       // node name
struct WifiMulti_aps wifi_aps[] = {             // ssid,password table
  { "aterm-20cd3f-g",  "65676ae9e9f9b" },
//  { "106F3FDBCFA9_EXT","amhh8khbexssm" },
  { NULL, NULL }
};
const char *ntp_hosts[] = {                        // NTP servers
  "192.168.254.7",
  "192.168.254.7",
  "192.168.254.7",
  NULL
};
#endif

#if defined(NODE_STORE)
#define LTE_POWER           (23)                // GPIO23 lte_power on digital port
#define LTE_POWERLED        (LED_BUILTIN)       // Use Bultin LED
#define TIME_TO_ON          (1)                 // MT100 startup wait time
#define WAKE_OFFSET         (29)                // wakeup time offset use other overhead
#define TIME_TO_SLEEP       (10 * 60)           // Time ESP32 will go to sleep (in seconds)
#define ENABLE_BME280
#define I2C_SDA             (21)                // I2C SDA port
#define I2C_SCL             (22)                // I2C SCL port
#define NODE_NAME           "esp32-store"       // node name
struct WifiMulti_aps wifi_aps[] = {             // ssid,password table
  { "106F3FDBCFA9_EXT","amhh8khbexssm" },
  { NULL, NULL }
};
const char *ntp_hosts[] = {                        // NTP servers
  "192.168.254.7",
  "192.168.254.7",
  "192.168.254.7",
  NULL
};
#endif

#if defined(NODE_SHT3X)
#define LTE_POWER           (23)                // GPIO23 lte_power on digital port
#define LTE_POWERLED        (-1)                // Use Bultin LED
#define TIME_TO_ON          (1)                 // MT100 startup wait time
#define WAKE_OFFSET         (29)                // wakeup time offset use other overhead
#define TIME_TO_SLEEP       (10 * 60)           // Time ESP32 will go to sleep (in seconds)
// #define ENABLE_BME280
#define ENABLE_SHT3X
#define SDAPIN (22)
#define SCLPIN (21)
// #define ENABLE_DHT22
#define NODE_NAME           "esp32-sht3x"       // node name
struct WifiMulti_aps wifi_aps[] = {             // ssid,password table
  { "aterm-20cd3f-g",  "65676ae9e9f9b" },
//  { "106F3FDBCFA9_EXT","amhh8khbexssm" },
  { NULL, NULL }
};
const char *ntp_hosts[] = {                        // NTP servers
  "192.168.254.7",
  "192.168.254.7",
  "192.168.254.7",
  NULL
};
#endif
