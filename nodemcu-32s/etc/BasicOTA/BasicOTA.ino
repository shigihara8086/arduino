#include <WiFi.h>
#include <WifiMulti.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

// #define HOST_NAME   "ambient-bme280"

// #define HOST_NAME   "esp32-sim800l"
// #define HOST_NAME   "esp32-oled-lolin32"
// #define HOST_NAME   "esp32-oled-lolin32-2"
// #define HOST_NAME   "esp32-st7789v"
// #define HOST_NAME   "esp32-NodeMCU-32S"
// #define HOST_NAME   "esp32-wrover-battery"
#define HOST_NAME   "esp32-TTGO-T-Display"

void setup() {
  wifi_init();
}
void loop() {
  ArduinoOTA.handle();
}
