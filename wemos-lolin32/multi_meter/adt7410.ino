/*
 ADT7410
*/

#include <Wire.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include "multi_meter.h"

#define ADT7410_ADD 0x48
#define DS18X20_OFFSET (0.0)

int adt7410I2CAddress = 0x48;
OneWire  onewire(13);  // on pin 13 (a 4.7K resistor is necessary)
DallasTemperature sensors(&onewire);

int adt7410_init() {
  Wire.begin(I2C_SDA, I2C_SCL);
  sensors.begin();
  return 0;
}

unsigned int adt7410_readRegister(void) {
  unsigned int val;

  Wire.requestFrom(ADT7410_ADD, 2);
  val  = Wire.read() << 8;
  val |= Wire.read();
  return val;
}

void adt7410_display(int times, int wait) {
  char strbuf[100];
  int head_pos    =  0;
  int data_pos    = 94 + 10;
  int unit_pos    = 128;
  int adt7410_pos = (16 - LINE_OFFSET);
  int ds18x20_pos = (32 - LINE_OFFSET);
  int core_pos    = (48 - LINE_OFFSET);
  uint16_t uiVal; //2バイト(16ビット)の領域
  int iVal;
  float adt7410;
  float ds18x20;
  float core;
  bool done = false;

  for (int i = 0; i < times; i ++) {
    // adt7410
    uiVal = adt7410_readRegister();
    uiVal >>= 3;                          // シフトで13bit化
    if (uiVal & 0x1000) {                 // 13ビットで符号判定
      iVal = uiVal - 0x2000;              // マイナスの時 (10進数で8192)
    } else {
      iVal = uiVal;                       //プラスの時
    }
    adt7410 = (float)iVal / 16.0;            // 温度換算(摂氏)
    // ds18x20
    sensors.requestTemperatures(); // Send the command to get temperatures
    delay(100);
    ds18x20 = sensors.getTempCByIndex(0) + DS18X20_OFFSET;
    // core
    core = temperatureRead();
    //
    display.clear();
    ntp_ticker();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(0, 0, "TEMPS");

    display.setFont(ArialMT_Plain_16);
    // display.setFont(Monospaced_plain_16);

    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(head_pos, adt7410_pos, "ADT7410");
    display.drawString(head_pos, ds18x20_pos, "DS18X20");
    display.drawString(head_pos, core_pos,    "Core");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(unit_pos, adt7410_pos, "ºC");
    display.drawString(unit_pos, ds18x20_pos, "ºC");
    display.drawString(unit_pos, core_pos,    "ºC");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    sprintf(strbuf, "%5.1f", adt7410); display.drawString(data_pos, adt7410_pos, strbuf);
    sprintf(strbuf, "%5.1f", ds18x20); display.drawString(data_pos, ds18x20_pos, strbuf);
    sprintf(strbuf, "%5.1f", core);    display.drawString(data_pos, core_pos,    strbuf);

    if (not done) {
      Serial.printf("A:%5.1fºC,D:%5.1fºC,C:%5.1fºC ", adt7410,ds18x20,core);
      done = true;
    }
    display.display();
    delay(wait);
  }
}

