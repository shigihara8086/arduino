
#define NODE_LOCAL                                 // 机上設置(Wi-Fi)

#include "TimerSampler.h"

void setup() {
  Serial.begin(115200);
  wifimulti_init();
  ntp_init();
}

void loop() {
float core_temp;
  core_temp = temperatureRead();
  Serial.printf("core temp = %f\n",core_temp);
  sleep(10);
  ntp_init();
}
