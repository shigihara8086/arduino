# main.py (サーバ建てるのと制御)

import socket
import machine
import network

def connect():
    ssid     = "aterm-20cd3f-g"
    password = "65676ae9e9f9b"
    station = network.WLAN(network.STA_IF)
    if station.isconnected() == True:
        print("接続済みです")
        return
    station.active(True)
    station.connect(ssid, password)
    while station.isconnected() == False:
        pass
    print("接続しました")
    print(station.ifconfig())

# ブラウザに表示させるためのhtml
# ポートは自身の環境に合わせてください

html = """
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>LED</title>
 </head>
<center>
<h2>MicroPythonは多分いいぞ...!!</h2>
<form>
LED 4番ポート
<button name="LED" value="ON_4" type="submit">ON</button>
<button name="LED" value="OFF_4" type="submit">OFF</button><br><br>
</form>
</center>
</html>
"""

LED_4 = machine.Pin(4, machine.Pin.OUT)

#サーバー関係と制御
connect()
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 80))
s.listen(5)

while True:
    conn, addr = s.accept()
    request = conn.recv(1024)
    request = str(request)
    LEDON_4 = request.find('/?LED=ON_4')
    LEDOFF_4 = request.find('/?LED=OFF_4')    

    if LEDON_4 == 6:
        LED_4.value(1)
    if LEDOFF_4 == 6:
        LED_4.value(0)
    response = html
    conn.send(response)
    conn.close()
 