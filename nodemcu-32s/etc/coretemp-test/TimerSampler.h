/*
 board base definition 
*/

//
// SSID and Password structure
//

struct WifiMulti_aps {                          // ssid,password pare
  char *ssid;
  char *passwd;
};

//
// difinitions for each boards
//

#if defined(NODE_LTE)
#define I2C_SDA             (25)                // I2C SDA port
#define I2C_SCL             (21)                // I2C SCL port
#define LTE_POWER           (23)                // GPIO23 lte_power on digital port
#define LTE_POWERLED        (LED_BUILTIN)       // Use Bultin LED
#define TIME_TO_ON          (50)                // MT100 startup wait time
#define WAKE_OFFSET         (10)                // wakeup time offset use other overhead
#define TIME_TO_SLEEP       (60 * 60)           // Time ESP32 will go to sleep (in seconds)
#define ENABLE_BME280
#define BME280_ADD          (0x76)
#define NODE_NAME           "esp32"             // node name
struct WifiMulti_aps wifi_aps[] = {             // ssid,password table
  { "MT100_4517","cc3263f327" },
  { NULL, NULL }
};
#endif

#if defined(NODE_LOCAL)
#define I2C_SDA             (25)                // I2C SDA port
#define I2C_SCL             (21)                // I2C SCL port
#define LTE_POWER           (23)                // GPIO23 lte_power on digital port
#define LTE_POWERLED        (LED_BUILTIN)       // Use Bultin LED
#define TIME_TO_ON          (50)                // MT100 startup wait time
#define WAKE_OFFSET         (10)                // wakeup time offset use other overhead
#define TIME_TO_SLEEP       (60 * 60)           // Time ESP32 will go to sleep (in seconds)
#define ENABLE_BME280
#define BME280_ADD          (0x76)
#define NODE_NAME           "esp32-local"       // node name
struct WifiMulti_aps wifi_aps[] = {             // ssid,password table
  { "aterm-20cd3f-g","65676ae9e9f9b" },
  { NULL, NULL }
};
#endif

#if defined(NODE_LOCAL2)
#define LTE_POWER           (23)                // GPIO23 lte_power on digital port
#define LTE_POWERLED        (LED_BUILTIN)       // Use Bultin LED
#define TIME_TO_ON          (10)                // MT100 startup wait time
#define WAKE_OFFSET         (10)                // wakeup time offset use other overhead
#define TIME_TO_SLEEP       (5 * 60)            // Time ESP32 will go to sleep (in seconds)
#define ENABLE_SHT3X
#define NODE_NAME           "esp32-sht3x"      // node name
struct WifiMulti_aps wifi_aps[] = {             // ssid,password table
  { "106F3FDBCFA9_EXT","amhh8khbexssm" },
  { NULL, NULL }
};
#endif

#if defined(NODE_LOCAL3)
#define I2C_SDA             (22)                // I2C SDA port
#define I2C_SCL             (21)                // I2C SCL port
#define LTE_POWER           (23)                // GPIO23 lte_power on digital port
#define LTE_POWERLED        (LED_BUILTIN)       // Use Bultin LED
#define TIME_TO_ON          (10)                // MT100 startup wait time
#define WAKE_OFFSET         (10)                // wakeup time offset use other overhead
#define TIME_TO_SLEEP       (5 * 60)            // Time ESP32 will go to sleep (in seconds)
#define ENABLE_BME280
#define NODE_NAME           "esp32-local3"      // node name
struct WifiMulti_aps wifi_aps[] = {             // ssid,password table
  { "aterm-20cd3f-g","65676ae9e9f9b" },
  { NULL, NULL }
};
#endif

#if defined(NODE_OUTER)
#define I2C_SDA             (22)                // I2C SDA port
#define I2C_SCL             (21)                // I2C SCL port
#define LTE_POWER           (23)                // GPIO23 lte_power on digital port
#define LTE_POWERLED        (LED_BUILTIN)       // Use Bultin LED
#define TIME_TO_ON          (10)                // MT100 startup wait time
#define WAKE_OFFSET         (10)                // wakeup time offset use other overhead
#define TIME_TO_SLEEP       (5 * 60)            // Time ESP32 will go to sleep (in seconds)
#define ENABLE_BME280
#define NODE_NAME           "esp32-outer2"      // node name
struct WifiMulti_aps wifi_aps[] = {             // ssid,password table
  { "aterm-20cd3f-g","65676ae9e9f9b" },
  { NULL, NULL }
};
#endif

#if defined(NODE_TEST)
#define I2C_SDA             (22)                // I2C SDA port
#define I2C_SCL             (21)                // I2C SCL port
#define LTE_POWER           (23)                // GPIO23 lte_power on digital port
#define LTE_POWERLED        (LED_BUILTIN)       // Use Bultin LED
#define TIME_TO_ON          (10)                // MT100 startup wait time
#define WAKE_OFFSET         (10)                // wakeup time offset use other overhead
#define TIME_TO_SLEEP       (5 * 60)            // Time ESP32 will go to sleep (in seconds)
#define NODE_NAME           "esp32-test"        // node name
struct WifiMulti_aps wifi_aps[] = {             // ssid,password table
  { "aterm-20cd3f-g","65676ae9e9f9b" },
  { NULL, NULL }
};
#endif

#if defined(NODE_TEST2)
#define I2C_SDA             (22)                // I2C SDA port
#define I2C_SCL             (21)                // I2C SCL port
#define LTE_POWER           (23)                // GPIO23 lte_power on digital port
#define LTE_POWERLED        (LED_BUILTIN)       // Use Bultin LED
#define TIME_TO_ON          (10)                // MT100 startup wait time
#define WAKE_OFFSET         (10)                // wakeup time offset use other overhead
#define TIME_TO_SLEEP       (5 * 60)            // Time ESP32 will go to sleep (in seconds)
#define NODE_NAME           "esp32-test2"       // node name
struct WifiMulti_aps wifi_aps[] = {             // ssid,password table
  { "aterm-20cd3f-a",   "65676ae9e9f9b" },
  { "aterm-20cd3f-g",   "65676ae9e9f9b" },
  { "106F3FDBCFA9",     "amhh8khbexssm" },
  { "106F3FDBCFA9_EXT", "amhh8khbexssm" },
  { NULL, NULL }
};
#endif
