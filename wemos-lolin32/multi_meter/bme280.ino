/*
  BME280
*/

#include <Wire.h>
#include "Adafruit_BME280.h"
#include "Adafruit_Sensor.h"
#include "multi_meter.h"

#define BME280_ADD 0x76

Adafruit_BME280 bme(I2C_SDA, I2C_SCL);

int bme280_init() {
  bme.begin(BME280_ADD);
  return 0;
}

void bme280_display(int times, int wait) {
  char strbuf[100];
  int head_pos =  0;
  int data_pos = 94 + 2;
  int unit_pos = 128;
  int temp_pos = (16 - LINE_OFFSET);
  int humi_pos = (32 - LINE_OFFSET);
  int pres_pos = (48 - LINE_OFFSET);
  bool done = false;

  // BME280
  for (int i = 0; i < times; i ++) {
    display.clear();
    ntp_ticker();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(0, 0, "BME280");

    display.setFont(ArialMT_Plain_16);
    // display.setFont(Monospaced_plain_10);

    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(head_pos, temp_pos, "Temp");
    display.drawString(head_pos, humi_pos, "Humi");
    display.drawString(head_pos, pres_pos, "P:slvl");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(unit_pos, temp_pos, "ºC");
    display.drawString(unit_pos, humi_pos, "%");
    display.drawString(unit_pos, pres_pos, "hPa");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    sprintf(strbuf, "%5.1f", bme.readTemperature());
    display.drawString(data_pos, temp_pos, strbuf);
    sprintf(strbuf, "%5.1f", bme.readHumidity());
    display.drawString(data_pos, humi_pos, strbuf);
    sprintf(strbuf, "%5.1f", bme.readPressure() / 100.0F + SEALEVEL_ADJUSTMENT);
    display.drawString(data_pos, pres_pos, strbuf);

    if (not done) {
      Serial.printf("T:%5.1fºC,H:%5.1f%%,P:%5.1fhPa ", bme.readTemperature(), bme.readHumidity(), bme.readPressure() / 100.0F + SEALEVEL_ADJUSTMENT);
      done = true;
    }
    display.display();
    delay(wait);
  }
}

