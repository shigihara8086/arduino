#include "esp32-hal-log.h"
#include "freertos/task.h"

void task0(void* arg) {
  while (1) {
    log_i("This is Core 0");
    delay(1000);
  }
}

void task1(void* arg) {
  while (1) {
    log_i("This is Core 1");
    delay(1500);
  }
}

void setup() {
  log_i("Hello, this is ESP32:)");
  xTaskCreatePinnedToCore(task0, "Task0", 4096, NULL, 1, NULL, 0);
  xTaskCreatePinnedToCore(task1, "Task1", 4096, NULL, 1, NULL, 1);
}

void loop() {
}

