/*
 DS18B20
*/

#if defined(ENABLE_DS18B20)

char *node_config_ds18b20_temp =
  "graph_title 温度(%s)\n"
  "graph_args --base 1000 --alt-autoscale\n"
  "graph_vlabel ℃\n"
  "graph_scale no\n"
  "graph_order temp inter trend\n"
  "graph_category measure_temperature\n"
  "graph_info DS18B20センサーで測定した温度\n"
  "temp.label 温度　　\n"
  "temp.info DS18B20センサーの温度\n"
  "inter.cdef temp,UN,PREV,temp,IF\n"
  "inter.graph no\n"
  "inter.update no\n"
  "trend.label 移動平均\n"
  "trend.line 25.0:880000\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info 温度の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

float meas_temp = 0.0;

#include <OneWire.h>
#include <DallasTemperature.h>

// GPIO where the DS18B20 is connected to
const int oneWireBus = 4;

// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(oneWireBus);

// Pass our oneWire reference to Dallas Temperature sensor 
DallasTemperature sensors(&oneWire);

//
// initialize
//

int ds18b20_init() {
  sensors.begin();
  return 0;
}

void do_config_ds18b20_temp(WiFiClient &client) {
  while(true) {
    sensors.requestTemperatures(); 
    meas_temp = sensors.getTempCByIndex(0);
    if(meas_temp > -50 && meas_temp < 85) break;
    delay(1000);
  }
  client.printf(node_config_ds18b20_temp,node_name);
  Serial.printf(node_config_ds18b20_temp,node_name);
}

//
// fetch
//

void do_fetch_ds18b20_temp(WiFiClient &client) {
  client.printf("temp.value %f\n",meas_temp);
  Serial.printf("meas_temp: %5.1f\n", meas_temp);
}

//
// loop
//

void ds18b20_loop() {
}

#endif
