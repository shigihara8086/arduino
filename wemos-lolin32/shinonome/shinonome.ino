#include "ESP32_I2C_SSD1306.h"
#include "ESP32_SPIFFS_ShinonomeFNT.h"

const uint8_t ADDRES_OLED =  0x3C;
const int sda = 5;
const int scl = 4;
const uint8_t Horizontal_pixel = 128;
const uint8_t Vertical_pixel = 64;

const char* UTF8SJIS_file = "/Utf8Sjis.tbl"; //UTF8 Shift_JIS 変換テーブルファイル名を記載しておく
const char* Shino_Zen_Font_file = "/shnmk16.bdf"; //全角フォントファイル名を定義
const char* Shino_Half_Font_file = "/shnm8x16.bdf"; //半角フォントファイル名を定義

ESP32_I2C_SSD1306 ssd1306(ADDRES_OLED, sda, scl, Horizontal_pixel, Vertical_pixel);
ESP32_SPIFFS_ShinonomeFNT SFR;

enum { Display_Max_num = 4, MaxTxtByte = 100 , cash = 2};

String utf8_str[Display_Max_num];
uint8_t sj_txt[Display_Max_num][MaxTxtByte] = {0}; //Shift_JISコード格納
uint16_t sj_length[Display_Max_num] = {0}; //Shift_JISコードの長さ
uint16_t sj_cnt[Display_Max_num] = {0}; //Shift_JISコード半角文字数カウント

uint8_t Size = 1;

uint8_t cash_font_cnt[Display_Max_num] = {0};
uint8_t cash_font_read_cnt[Display_Max_num] = {0};
uint8_t disp_buf[Display_Max_num][16][16] = {0};

String test_str;
uint8_t test_buf[16][16] = {0};
uint8_t test_sj_txt[MaxTxtByte] = {0};
uint16_t test_sj_length;

boolean fnt_read_ok[Display_Max_num] = {true, true, true, true};
boolean FontReakOK[Display_Max_num] = {false, false, false, false};

uint32_t SclTime1 = 0;
uint32_t SclTime2 = 0;

uint8_t CashFont[cash][Display_Max_num][2][16] = {0};
uint8_t Zen_or_Han[cash][Display_Max_num] = {0};

void setup() {
  Serial.begin(115200);
  ssd1306.SSD1306_Init(400000); //I2C Max=400kHz
  ssd1306.Display_Clear_All();
  //３つのSPIFFSファイルを同時オープン
  SFR.SPIFFS_Shinonome_Init3F(UTF8SJIS_file, Shino_Half_Font_file, Shino_Zen_Font_file); //ライブラリ初期化。２ファイル同時に開く
  Serial.println();

  test_str = "大晦日！";
  Size = 1;
  test_sj_length = SFR.StrDirect_ShinoFNT_readALL(-90, "大晦日！", test_buf);
  ssd1306.Font8x16_1line_Page_DisplayOut(test_sj_length, 0, 0, test_buf);
  delay(1500);
  ssd1306.Display_Clear_All();

  Size = 2;
  ssd1306.SizeUp_8x16_Font_DisplayOut(Size, test_sj_length, 0, 0, test_buf);
  delay(1500);
  ssd1306.Display_Clear_All();

  test_str = "大晦";
  Size = 4;
  test_sj_length = SFR.StrDirect_ShinoFNT_readALL(-90, test_str, test_buf);
  ssd1306.SizeUp_8x16_Font_DisplayOut(Size, test_sj_length, 0, 0, test_buf);
  delay(1500);
  ssd1306.Display_Clear_All();

  test_str = "♪";
  Size = 4;
  test_sj_length = SFR.StrDirect_ShinoFNT_readALL(-90, test_str, test_buf);
  ssd1306.SizeUp_8x16_Font_DisplayOut(Size, test_sj_length, 32, 0, test_buf);
  delay(2000);
  ssd1306.Display_Clear_All();

  //キャッシュバイトを使う場合、文字の最後に全角スペース適当に入れておけば、文字スクロール終了を検出可能
  //４倍角ならスペース５つ入力
  sj_length[0] = SFR.UTF8toSJIS_convert("← ← ESP32 いいのができました！!東雲フォント４倍角！　　　　　", sj_txt[0]);
  sj_length[1] = SFR.UTF8toSJIS_convert("●１．ESP32 とOLED SSD1306はマルチタスクにすると意外と使える！　", sj_txt[1]);
  sj_length[2] = SFR.UTF8toSJIS_convert("●２．SSD1306 は￥６９０と安いですよ～～　", sj_txt[2]);
  sj_length[3] = SFR.UTF8toSJIS_convert("★本年は当ブログをご覧いただきありがとうございます。　", sj_txt[3]);

  //マルチタスクでスムースにスクロールするための、キャッシュバイト２バイト事前読み込み
  for (int j = 0; j < cash; j++) {
    for (int str_number = 0; str_number < Display_Max_num; str_number++) {
      Zen_or_Han[j][str_number] = SFR.Sjis_inc_FntRead_Rot(&sj_cnt[str_number], -90, 0, str_number, sj_txt[str_number], sj_length[str_number], CashFont[j][str_number]);
    }
  }

  TaskHandle_t th; //ESP32 マルチタスク　ハンドル定義
  xTaskCreatePinnedToCore(Task1, "Task1", 4096, NULL, 5, &th, 0); //マルチタスク core 0 実行

  SclTime1 = millis();
  SclTime2 = millis();
}
//************************************************
void loop() {
  int str_number;
  //１文字スクロールしたらフォントを読み込む
  for (str_number = 0; str_number < Display_Max_num; str_number++) {
    if (FontReakOK[str_number] == true) {
      Zen_or_Han[ cash_font_read_cnt[str_number] ][str_number] = SFR.Sjis_inc_FntRead_Rot(&sj_cnt[str_number], -90, 0, str_number, sj_txt[str_number], sj_length[str_number], CashFont[ cash_font_read_cnt[str_number] ][str_number]);
      cash_font_read_cnt[str_number]++;

      if (cash_font_read_cnt[str_number] >= cash) cash_font_read_cnt[str_number] = 0;
      FontReakOK[str_number] = false;
    }
  }
}
//************* マルチタスク ****************************************
void Task1(void *pvParameters) {
  int str_number = 0;
  while (1) {
    if (millis() - SclTime1 > 0) {
      if ( sj_cnt[0] < (sj_length[0] - 2) ) { //スクロール終了検出はsj_lengthから全角2バイト分引く
        str_number = 0;
        if (ssd1306.Scroller_Font8x16_PageReplace(4, str_number, Zen_or_Han[cash_font_cnt[str_number]][str_number], CashFont[cash_font_cnt[str_number]][str_number], disp_buf[str_number])) {
          cash_font_cnt[str_number]++;
          if (cash_font_cnt[str_number] >= cash) cash_font_cnt[str_number] = 0;
          FontReakOK[str_number] = true;
        }
        ssd1306.SizeUp_8x16_Font_DisplayOut(4, 4, 0, 0, disp_buf[str_number]);
        SclTime2 = millis();
      } else if ((millis() - SclTime2 < 20000)) {
        str_number = 1;
        if (ssd1306.Scroller_Font8x16_PageReplace(16, str_number, Zen_or_Han[cash_font_cnt[str_number]][str_number], CashFont[cash_font_cnt[str_number]][str_number], disp_buf[str_number])) {
          cash_font_cnt[str_number]++;
          if (cash_font_cnt[str_number] >= cash) cash_font_cnt[str_number] = 0;
          FontReakOK[str_number] = true;
        }
        ssd1306.Font8x16_1line_Page_DisplayOut(16, 0, 0, disp_buf[str_number]);

        str_number = 2;
        if (ssd1306.Scroller_Font8x16_PageReplace(16, str_number, Zen_or_Han[cash_font_cnt[str_number]][str_number], CashFont[cash_font_cnt[str_number]][str_number], disp_buf[str_number])) {
          cash_font_cnt[str_number]++;
          if (cash_font_cnt[str_number] >= cash) cash_font_cnt[str_number] = 0;
          FontReakOK[str_number] = true;
        }
        ssd1306.Font8x16_1line_Page_DisplayOut(16, 0, 2, disp_buf[str_number]);

        str_number = 3;
        if (ssd1306.Scroller_Font8x16_PageReplace(8, str_number, Zen_or_Han[cash_font_cnt[str_number]][str_number], CashFont[cash_font_cnt[str_number]][str_number], disp_buf[str_number])) {
          cash_font_cnt[str_number]++;
          if (cash_font_cnt[str_number] >= cash) cash_font_cnt[str_number] = 0;
          FontReakOK[str_number] = true;
        }
        ssd1306.SizeUp_8x16_Font_DisplayOut(2, 8, 0, 4, disp_buf[str_number]);
      } else if (millis() - SclTime2 >= 10000) {
        SclTime2 = millis();
        sj_cnt[0] = 0;
      }

      SclTime1 = millis();
    }
    delay(1);//マルチタスクのwhileループでは必ず必要
  }
}

