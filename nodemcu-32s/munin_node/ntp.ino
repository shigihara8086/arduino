/*
 get ntp date or rtc date
*/

#define JST                 (9 * 3600L)            // JST offset

//
// NTP servers
//

const char *ntp_hosts[] = {                        // NTP servers
  "192.168.254.7",
  "192.168.254.7",
  "192.168.254.7",
  NULL
};

struct tm timeInfo;                                // time structure

void ntp_init() {
int i;
  i = 0;
  while(true) {
    configTime(JST, 0, ntp_hosts[0], ntp_hosts[1], ntp_hosts[2]);
    getLocalTime(&timeInfo);
    Serial.printf("NTP date : %04d-%02d-%02d %02d:%02d:%02d\n",
                  timeInfo.tm_year + 1900, timeInfo.tm_mon + 1, timeInfo.tm_mday,
                  timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec);
    if((timeInfo.tm_year + 1900) >= 1971) break;
    if(i > 5) {    
      Serial.print("Can not NTP sync, force restart\n");
      ESP.restart();
    }
    i ++;
    delay(1000);
  }
}
